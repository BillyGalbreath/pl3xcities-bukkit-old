package net.pl3x.pl3xcities.events.plot;

import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import net.pl3x.pl3xcities.cities.City;
import net.pl3x.pl3xcities.cities.Plot;
import net.pl3x.pl3xcities.events.PlotEvent;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;


public class PlotRedefineEvent extends PlotEvent implements Cancellable {
	private ProtectedRegion oldRegion;
	private ProtectedRegion newRegion;
	private boolean cancelled;
	private static final HandlerList handlers = new HandlerList();

	public PlotRedefineEvent(City city, Plot plot, Player player, ProtectedRegion oldRegion, ProtectedRegion newRegion) {
		super(city, plot, player);
	    this.oldRegion = oldRegion;
	    this.newRegion = newRegion;
	}

	public ProtectedRegion getOldRegion() {
		return oldRegion;
	}

	public ProtectedRegion getNewRegion() {
		return newRegion;
	}

	public void setNewRegion(ProtectedRegion newRegion) {
		this.newRegion = newRegion;
	}

	@Override
	public boolean isCancelled() {
		return cancelled;
	}

	@Override
	public void setCancelled(boolean b) {
		this.cancelled = b;
	}

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}
}
