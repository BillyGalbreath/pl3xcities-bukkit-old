package net.pl3x.pl3xcities.events.plot;

import net.pl3x.pl3xcities.cities.City;
import net.pl3x.pl3xcities.cities.Plot;
import net.pl3x.pl3xcities.events.PlotEvent;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;

/**
 * Called when a plot rent price is set/changed.
 * 
 * Cancelling this event will prevent rent price change of the plot, plugins which cancel this event are responsible for notifying the player.
 */
public class PlotSetRentPriceEvent extends PlotEvent implements Cancellable {
	private double price;
	private boolean cancelled;
	private static final HandlerList handlers = new HandlerList();

	public PlotSetRentPriceEvent(City city, Plot plot, Player player, double price) {
		super(city, plot, player);
		this.price = price;
	}

	/**
	 * Get the rent price being set
	 * 
	 * @return The price
	 */
	public double getPrice() {
		return price;
	}

	/**
	 * Set the new rent price to be set
	 * 
	 * @param price - The price
	 */
	public void setPrice(double price) {
		this.price = price;
	}

	/**
	 * Gets the cancellation state of this event. A cancelled event will not be executed in the server, but will still pass to other plugins
	 * 
	 * @return True if this event is cancelled
	 */
	public boolean isCancelled() {
		return cancelled;
	}

	/**
	 * Sets the cancellation state of this event. A cancelled event will not be executed in the server, but will still pass to other plugins.
	 * 
	 * @param cancel true if you wish to cancel this event
	 */
	public void setCancelled(boolean cancelled) {
		this.cancelled = cancelled;
	}

	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}
}
