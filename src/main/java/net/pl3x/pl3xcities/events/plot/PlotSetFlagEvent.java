package net.pl3x.pl3xcities.events.plot;

import net.pl3x.pl3xcities.cities.City;
import net.pl3x.pl3xcities.cities.Plot;
import net.pl3x.pl3xcities.events.PlotEvent;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;

import com.sk89q.worldguard.protection.flags.Flag;

/**
 * Called when a player changes a flag setting on a plot.
 * 
 * Cancelling this event will prevent flag change of the plot, plugins which cancel this event are responsible for notifying the player.
 */
public class PlotSetFlagEvent extends PlotEvent implements Cancellable {
	private Flag<?> flag;
	private Object oldValue;
	private String newValue;
	private boolean cancelled;
	private static final HandlerList handlers = new HandlerList();

	public PlotSetFlagEvent(City city, Plot plot, Player player, Flag<?> flag, Object oldValue, String newValue) {
		super(city, plot, player);
		this.flag = flag;
		this.oldValue = oldValue;
		this.newValue = newValue;
	}

	/**
	 * Get the WorldGuard flag
	 * 
	 * @return WorldGuard Flag
	 */
	public Flag<?> getFlag() {
		return flag;
	}

	/**
	 * Get the old value of the WorldGuard flag
	 * 
	 * @return Object of old value
	 */
	public Object getOldValue() {
		return oldValue;
	}

	/**
	 * Get the new value of the WorldGuard flag
	 * 
	 * @return Object of new value
	 */
	public String getNewValue() {
		return newValue;
	}

	/**
	 * Set the new value of the WorldGuard flag
	 * 
	 * @param val - The Object of the new value
	 */
	public void setNewValue(String val) {
		newValue = val;
	}

	/**
	 * Gets the cancellation state of this event. A cancelled event will not be executed in the server, but will still pass to other plugins
	 * 
	 * @return True if this event is cancelled
	 */
	public boolean isCancelled() {
		return cancelled;
	}

	/**
	 * Sets the cancellation state of this event. A cancelled event will not be executed in the server, but will still pass to other plugins.
	 * 
	 * @param cancel true if you wish to cancel this event
	 */
	public void setCancelled(boolean cancelled) {
		this.cancelled = cancelled;
	}

	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}
}
