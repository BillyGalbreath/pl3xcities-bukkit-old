package net.pl3x.pl3xcities.events.plot;

import net.pl3x.pl3xcities.cities.City;
import net.pl3x.pl3xcities.cities.Plot;
import net.pl3x.pl3xcities.events.PlotEvent;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;

/**
 * Called when a Plot is bought
 * 
 * Cancelling this event will prevent purchase of the plot, plugins which cancel this event are responsible for notifying the player.
 */
public class PlotBuyEvent extends PlotEvent implements Cancellable {
	private boolean cancelled;
	private static final HandlerList handlers = new HandlerList();

	public PlotBuyEvent(City city, Plot plot, Player player) {
		super(city, plot, player);
	}

	/**
	 * Gets the cancellation state of this event. A cancelled event will not be executed in the server, but will still pass to other plugins
	 * 
	 * @return True if this event is cancelled
	 */
	public boolean isCancelled() {
		return cancelled;
	}

	/**
	 * Sets the cancellation state of this event. A cancelled event will not be executed in the server, but will still pass to other plugins.
	 * 
	 * @param cancel true if you wish to cancel this event
	 */
	public void setCancelled(boolean cancelled) {
		this.cancelled = cancelled;
	}

	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}
}
