package net.pl3x.pl3xcities.events.plot;

import net.pl3x.pl3xcities.cities.City;
import net.pl3x.pl3xcities.cities.Plot;
import net.pl3x.pl3xcities.events.PlotEvent;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;

/**
 * Called when a plot's buyable state is changed
 * 
 * Cancelling this event will prevent changing the buyable state of the plot, plugins which cancel this event are responsible for notifying the player.
 */
public class PlotForSaleEvent extends PlotEvent implements Cancellable {
	private boolean oldState;
	private boolean newState;
	private boolean cancelled;
	private static final HandlerList handlers = new HandlerList();

	public PlotForSaleEvent(City city, Plot plot, Player player, boolean oldState, boolean newState) {
		super(city, plot, player);
		this.oldState = oldState;
		this.newState = newState;
	}

	/**
	 * Get the old state
	 * 
	 * @return The old state
	 */
	public boolean getOldState() {
		return oldState;
	}

	/**
	 * Get the new state to set
	 * 
	 * @return The new state
	 */
	public boolean getNewState() {
		return newState;
	}

	/**
	 * Set the new state to set
	 * 
	 * @param newState - The new state to set
	 */
	public void setNewState(boolean newState) {
		this.newState = newState;
	}

	/**
	 * Gets the cancellation state of this event. A cancelled event will not be executed in the server, but will still pass to other plugins
	 * 
	 * @return True if this event is cancelled
	 */
	public boolean isCancelled() {
		return cancelled;
	}

	/**
	 * Sets the cancellation state of this event. A cancelled event will not be executed in the server, but will still pass to other plugins.
	 * 
	 * @param cancel true if you wish to cancel this event
	 */
	public void setCancelled(boolean cancelled) {
		this.cancelled = cancelled;
	}

	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}
}
