package net.pl3x.pl3xcities.events.city;

import net.pl3x.pl3xcities.cities.City;
import net.pl3x.pl3xcities.events.CityEvent;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;

/**
 * Called when a player sets a new spawn location for the city.
 * 
 * Cancelling this event will prevent changing of the spawn location, plugins which cancel this event are responsible for notifying the player.
 */
public class CitySetSpawnEvent extends CityEvent implements Cancellable {
	private Location oldLocation;
	private Location newLocation;
	private boolean cancelled;
	private static final HandlerList handlers = new HandlerList();

	public CitySetSpawnEvent(City city, Player player, Location oldLocation) {
		super(city, player);
		this.oldLocation = oldLocation.clone();
		this.newLocation = player.getLocation().clone();
	}

	/**
	 * Get the old spawn location for the city
	 * 
	 * @return Bukkit Location
	 */
	public Location getOldLocation() {
		return oldLocation;
	}

	/**
	 * Get the new spawn location being set
	 * 
	 * @return Bukkit Location
	 */
	public Location getNewLocation() {
		return newLocation;
	}

	/**
	 * Set the new spawn location being set
	 * 
	 * @param location - The Bukkit Location
	 */
	public void setNewLocation(Location location) {
		newLocation = location.clone();
	}

	/**
	 * Gets the cancellation state of this event. A cancelled event will not be executed in the server, but will still pass to other plugins
	 * 
	 * @return True if this event is cancelled
	 */
	public boolean isCancelled() {
		return cancelled;
	}

	/**
	 * Sets the cancellation state of this event. A cancelled event will not be executed in the server, but will still pass to other plugins.
	 * 
	 * @param cancel true if you wish to cancel this event
	 */
	public void setCancelled(boolean cancelled) {
		this.cancelled = cancelled;
	}

	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}
}
