package net.pl3x.pl3xcities.events.city;

import net.pl3x.pl3xcities.cities.City;
import net.pl3x.pl3xcities.events.CityEvent;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;

/**
 * Called when a city's disabled flags list is modified
 */
public class CitySetDisabledFlagsEvent extends CityEvent implements Cancellable {
	private String disabledFlags;
	private boolean cancelled;
	private static final HandlerList handlers = new HandlerList();

	public CitySetDisabledFlagsEvent(City city, Player player, String disabledFlags) {
		super(city, player);
		this.disabledFlags = disabledFlags;
	}

	/**
	 * Get the string of disabled flags
	 * 
	 * @return The disabled flags
	 */
	public String getDisabledFlags() {
		return disabledFlags;
	}

	/**
	 * Set the disabled flags
	 * 
	 * @param disabledFlags - The disabled flags
	 */
	public void setDisabledFlags(String disabledFlags) {
		this.disabledFlags = disabledFlags;
	}

	/**
	 * Gets the cancellation state of this event. A cancelled event will not be executed in the server, but will still pass to other plugins
	 * 
	 * @return True if this event is cancelled
	 */
	public boolean isCancelled() {
		return cancelled;
	}

	/**
	 * Sets the cancellation state of this event. A cancelled event will not be executed in the server, but will still pass to other plugins.
	 * 
	 * @param cancel true if you wish to cancel this event
	 */
	public void setCancelled(boolean cancelled) {
		this.cancelled = cancelled;
	}

	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}
}
