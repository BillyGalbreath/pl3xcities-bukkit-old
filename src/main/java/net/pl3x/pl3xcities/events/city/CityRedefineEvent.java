package net.pl3x.pl3xcities.events.city;

import net.pl3x.pl3xcities.cities.City;
import net.pl3x.pl3xcities.events.CityEvent;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;

import com.sk89q.worldguard.protection.regions.ProtectedRegion;

/**
 * Called when a player redefines the borders of a City
 * 
 * Cancelling this event will prevent redefining of the city, plugins which cancel this event are responsible for notifying the player.
 */
public class CityRedefineEvent extends CityEvent implements Cancellable {
	private ProtectedRegion oldRegion;
	private ProtectedRegion newRegion;
	private boolean cancelled;
	private static final HandlerList handlers = new HandlerList();

	public CityRedefineEvent(City city, Player player, ProtectedRegion oldRegion, ProtectedRegion newRegion) {
		super(city, player);
		this.oldRegion = oldRegion;
		this.newRegion = newRegion;
	}

	/**
	 * Get the old WorldGuard ProtectedRegion
	 * 
	 * @return The old ProtectedRegion
	 */
	public ProtectedRegion getOldRegion() {
		return oldRegion;
	}

	/**
	 * Get the new WorldGuard ProtectedRegion
	 * 
	 * @return The new ProtectedRegion
	 */
	public ProtectedRegion getNewRegion() {
		return newRegion;
	}

	/**
	 * Set the new WorldGuard ProtectedRegion
	 * 
	 * @param newRegion - The new ProtectedRegion
	 */
	public void setNewRegion(ProtectedRegion newRegion) {
		this.newRegion = newRegion;
	}

	/**
	 * Gets the cancellation state of this event. A cancelled event will not be executed in the server, but will still pass to other plugins
	 * 
	 * @return True if this event is cancelled
	 */
	public boolean isCancelled() {
		return cancelled;
	}

	/**
	 * Sets the cancellation state of this event. A cancelled event will not be executed in the server, but will still pass to other plugins.
	 * 
	 * @param cancel true if you wish to cancel this event
	 */
	public void setCancelled(boolean cancelled) {
		this.cancelled = cancelled;
	}

	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}
}
