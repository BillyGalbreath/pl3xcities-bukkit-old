package net.pl3x.pl3xcities.events.city;

import net.pl3x.pl3xcities.cities.City;
import net.pl3x.pl3xcities.events.CityEvent;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;

/**
 * Called when a City officer is removed
 * 
 * Cancelling this event will prevent removal of the city officer, plugins which cancel this event are responsible for notifying the player.
 */
public class CityOfficerRemoveEvent extends CityEvent implements Cancellable {
	private String name;
	private boolean cancelled;
	private static final HandlerList handlers = new HandlerList();

	public CityOfficerRemoveEvent(City city, Player player, String name) {
		super(city, player);
		this.name = name;
	}

	/**
	 * Get the name of the officer to remove
	 * 
	 * @return Name of the officer
	 */
	public String getOfficerName() {
		return name;
	}

	/**
	 * Set the officer to remove
	 * 
	 * @param name - The name of the officer
	 */
	public void setOfficerName(String name) {
		this.name = name;
	}

	/**
	 * Gets the cancellation state of this event. A cancelled event will not be executed in the server, but will still pass to other plugins
	 * 
	 * @return True if this event is cancelled
	 */
	public boolean isCancelled() {
		return cancelled;
	}

	/**
	 * Sets the cancellation state of this event. A cancelled event will not be executed in the server, but will still pass to other plugins.
	 * 
	 * @param cancel true if you wish to cancel this event
	 */
	public void setCancelled(boolean cancelled) {
		this.cancelled = cancelled;
	}

	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}
}
