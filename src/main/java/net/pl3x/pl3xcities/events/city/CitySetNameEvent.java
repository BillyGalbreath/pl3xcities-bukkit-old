package net.pl3x.pl3xcities.events.city;

import net.pl3x.pl3xcities.cities.City;
import net.pl3x.pl3xcities.events.CityEvent;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;

/**
 * Called when a city is renamed
 * 
 * Cancelling this event will prevent price name of the plot, plugins which cancel this event are responsible for notifying the player.
 */
public class CitySetNameEvent extends CityEvent implements Cancellable {
	private String oldName;
	private String newName;
	private boolean cancelled;
	private static final HandlerList handlers = new HandlerList();

	public CitySetNameEvent(City city, Player player, String oldName, String newName) {
		super(city, player);
		this.oldName = oldName;
		this.newName = newName;
	}

	/**
	 * Get the old name of the city
	 * 
	 * @return The old name
	 */
	public String getOldName() {
		return oldName;
	}

	/**
	 * Get the new name of the city being set
	 * 
	 * @return The new name
	 */
	public String getNewName() {
		return newName;
	}

	/**
	 * Set the new name of the city being set
	 * 
	 * @param newName - The new name
	 */
	public void setNewName(String newName) {
		this.newName = newName;
	}

	/**
	 * Gets the cancellation state of this event. A cancelled event will not be executed in the server, but will still pass to other plugins
	 * 
	 * @return True if this event is cancelled
	 */
	public boolean isCancelled() {
		return cancelled;
	}

	/**
	 * Sets the cancellation state of this event. A cancelled event will not be executed in the server, but will still pass to other plugins.
	 * 
	 * @param cancel true if you wish to cancel this event
	 */
	public void setCancelled(boolean cancelled) {
		this.cancelled = cancelled;
	}

	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}
}
