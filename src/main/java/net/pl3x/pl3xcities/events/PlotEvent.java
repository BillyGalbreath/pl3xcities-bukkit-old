package net.pl3x.pl3xcities.events;

import net.pl3x.pl3xcities.cities.City;
import net.pl3x.pl3xcities.cities.Plot;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;

/**
 * a base class for all events involving a plot
 */
public abstract class PlotEvent extends Event {
	private City city;
	private Plot plot;
	private Player player;

	public PlotEvent(City city, Plot plot, Player player) {
		this.city = city;
		this.plot = plot;
		this.player = player;
	}

	/**
	 * get the City involved in this event.
	 * 
	 * @return the city
	 */
	public City getCity() {
		return city;
	}

	/**
	 * get the Plot involved in this event.
	 * 
	 * @return the plot
	 */
	public Plot getPlot() {
		return plot;
	}

	/**
	 * Get the player that triggered this event
	 * 
	 * @return Bukkit Player
	 */
	public Player getPlayer() {
		return player;
	}
}
