package net.pl3x.pl3xcities.events;

import net.pl3x.pl3xcities.cities.City;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;

/**
 * a base class for all events involving a city
 */
public abstract class CityEvent extends Event {
	private City city;
	private Player player;

	public CityEvent(City city, Player player) {
		this.city = city;
		this.player = player;
	}

	/**
	 * get the City involved in this event.
	 * 
	 * @return the city
	 */
	public City getCity() {
		return city;
	}

	/**
	 * Get the player that triggered this event
	 * 
	 * @return Bukkit Player
	 */
	public Player getPlayer() {
		return player;
	}
}
