package net.pl3x.pl3xcities.listeners;

import java.util.ArrayList;
import java.util.List;

import net.pl3x.pl3xcities.flag.PistonProtectionFlag;
import net.pl3x.pl3xcities.util.RegionUtils;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPistonExtendEvent;
import org.bukkit.event.block.BlockPistonRetractEvent;

import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public class PistonListener implements Listener {
	/**
	 * Listen for pistons pushing blocks into or out of a protected region
	 * 
	 * @param event - BlockPistonExtendEvent
	 */
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void protectRegionFromPistonPush(BlockPistonExtendEvent event) {
		ApplicableRegionSet rSetFrom = null;
		ApplicableRegionSet rSetTo = null;
		rSetFrom = RegionUtils.getApplicableRegionSet(event.getBlock().getLocation());
		for (Block block : event.getBlocks()) {
			rSetTo = RegionUtils.getApplicableRegionSet(block.getRelative(event.getDirection()).getLocation());
		}
		if (rSetTo == null)
			rSetTo = RegionUtils.getApplicableRegionSet(event.getBlock().getRelative(event.getDirection()).getLocation());
		if (!PistonProtectionFlag.getValue(rSetFrom) && !PistonProtectionFlag.getValue(rSetTo))
			return; // not from/to a protected region
		List<ProtectedRegion> regionsFrom = new ArrayList<ProtectedRegion>();
		for (ProtectedRegion region : rSetFrom) {
			regionsFrom.add(region);
		}
		List<ProtectedRegion> regionsTo = new ArrayList<ProtectedRegion>();
		for (ProtectedRegion region : rSetTo) {
			regionsTo.add(region);
		}
		boolean cancel = false;
		for (ProtectedRegion region : rSetTo) {
			if (!regionsFrom.contains(region))
				cancel = true;
		}
		for (ProtectedRegion region : rSetFrom) {
			if (!regionsTo.contains(region))
				cancel = true;
		}
		if (cancel) // dont set to false! just in case another plugin has cancelled the event.
			event.setCancelled(true);
	}

	/**
	 * Listen for sticky pistons pulling block into or out of protected regions
	 * 
	 * @param event - BlockPistonRetractEvent
	 */
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void protectRegionFromPistonPull(BlockPistonRetractEvent event) {
		Block piston = event.getBlock();
		if (!piston.getType().equals(Material.PISTON_STICKY_BASE))
			return;
		ApplicableRegionSet rSetFrom = RegionUtils.getApplicableRegionSet(piston.getLocation());
		Block block = piston.getRelative(event.getDirection()).getRelative(event.getDirection());
		if (block.getType().equals(Material.AIR))
			return; // nothing to pull (air)
		ApplicableRegionSet rSetTo = RegionUtils.getApplicableRegionSet(block.getLocation());
		if (!PistonProtectionFlag.getValue(rSetFrom) && !PistonProtectionFlag.getValue(rSetTo))
			return; // not from/to a protected region
		List<ProtectedRegion> regionsFrom = new ArrayList<ProtectedRegion>();
		for (ProtectedRegion region : rSetFrom) {
			regionsFrom.add(region);
		}
		List<ProtectedRegion> regionsTo = new ArrayList<ProtectedRegion>();
		for (ProtectedRegion region : rSetTo) {
			regionsTo.add(region);
		}
		boolean cancel = false;
		for (ProtectedRegion region : rSetTo) {
			if (!regionsFrom.contains(region))
				cancel = true;
		}
		for (ProtectedRegion region : rSetFrom) {
			if (!regionsTo.contains(region))
				cancel = true;
		}
		if (cancel) // dont set to false! just in case another plugin has cancelled the event.
			event.setCancelled(true);
	}
}
