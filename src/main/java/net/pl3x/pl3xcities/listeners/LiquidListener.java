package net.pl3x.pl3xcities.listeners;

import java.util.ArrayList;
import java.util.List;

import net.pl3x.pl3xcities.flag.LiquidProtectionFlag;
import net.pl3x.pl3xcities.util.RegionUtils;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockFromToEvent;

import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public class LiquidListener implements Listener {
	/**
	 * Listen for any liquids moving and block them if leaving/entering a protected region
	 * 
	 * @param event - BlockFromToEvent
	 */
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void protectRegionFromLiquidSpills(BlockFromToEvent event) {
		if (!event.getBlock().isLiquid())
			return;
		ApplicableRegionSet rSetFrom = RegionUtils.getApplicableRegionSet(event.getBlock().getLocation());
		ApplicableRegionSet rSetTo = RegionUtils.getApplicableRegionSet(event.getToBlock().getLocation());
		if (!LiquidProtectionFlag.getValue(rSetFrom) && !LiquidProtectionFlag.getValue(rSetTo))
			return; // not from/to a protected region
		List<ProtectedRegion> regionsFrom = new ArrayList<ProtectedRegion>();
		for (ProtectedRegion region : rSetFrom) {
			regionsFrom.add(region);
		}
		List<ProtectedRegion> regionsTo = new ArrayList<ProtectedRegion>();
		for (ProtectedRegion region : rSetTo) {
			regionsTo.add(region);
		}
		boolean cancel = false;
		for (ProtectedRegion region : rSetTo) {
			if (!regionsFrom.contains(region))
				cancel = true;
		}
		for (ProtectedRegion region : rSetFrom) {
			if (!regionsTo.contains(region))
				cancel = true;
		}
		if (cancel) // dont set to false! just in case another plugin has cancelled the event.
			event.setCancelled(true);
	}
}
