package net.pl3x.pl3xcities.listeners;

import net.pl3x.pl3xcities.Pl3xCities;
import net.pl3x.pl3xcities.cities.City;
import net.pl3x.pl3xcities.cities.Plot;
import net.pl3x.pl3xcities.util.RegionUtils;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Sign;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerJoinEvent;

import com.sk89q.worldedit.bukkit.selections.Selection;

public class PlayerListener implements Listener {
	private Pl3xCities plugin;

	public PlayerListener(Pl3xCities plugin) {
		this.plugin = plugin;
	}

	/**
	 * Check for an update every time a user logs in or every hour (whichever comes first).
	 * 
	 * @param event - PlayerJoinEvent
	 */
	@EventHandler(priority = EventPriority.MONITOR)
	public void updateCheckOnJoin(PlayerJoinEvent event) {
		if (!plugin.getConfig().getBoolean("check-for-updates", true))
			return;
		if (plugin.hasUpdate())
			return;
		long timeNow = System.currentTimeMillis();
		if (timeNow - 3600000 < plugin.getLastUpdateCheck())
			return;
		plugin.checkForUpdate();
	}

	/**
	 * Fill in and colorize [ForSale] sign if the plot it was placed in is up for sale
	 * 
	 * @param event - SignChangeEvent
	 */
	@EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
	public void forSaleSign(SignChangeEvent event) {
		Location location = event.getBlock().getLocation();
		Selection selection = null;
		double price = 0;
		try {
			City city = RegionUtils.getCity(location);
			Plot plot = RegionUtils.getPlot(city, location);
			if (!plot.isBuyable())
				return;
			selection = RegionUtils.getSelection(plot.getRegion(), city.getWorld());
			price = plot.getPrice();
		} catch (Exception e) {
			return;
		}
		if (selection == null)
			return;
		final Sign sign = (Sign) event.getBlock().getState();
		if (!event.getLine(0).equalsIgnoreCase("[For Sale]") && !event.getLine(0).equalsIgnoreCase("[ForSale]") && !event.getLine(0).equalsIgnoreCase("[For-Sale]"))
			return;
		int low = selection.getMinimumPoint().getBlockY();
		int high = selection.getMaximumPoint().getBlockY();
		int area = selection.getArea() / selection.getHeight();
		sign.setLine(0, plugin.colorize("&4[&1ForSale&4]"));
		sign.setLine(1, plugin.colorize("&a" + area + "m\u00B2"));
		sign.setLine(2, plugin.colorize("&b" + low + " - " + high));
		sign.setLine(3, plugin.colorize("&5" + plugin.getEconomy().format(price)));
		Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
			@Override
			public void run() {
				sign.update();
			}
		}, 1);
	}

	/**
	 * Fill in and colorize [ForRent] sign if the plot it was placed in is up for rent
	 * 
	 * @param event - SignChangeEvent
	 */
	@EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
	public void forRentSign(SignChangeEvent event) {
		Location location = event.getBlock().getLocation();
		Selection selection = null;
		double price = 0;
		try {
			City city = RegionUtils.getCity(location);
			Plot plot = RegionUtils.getPlot(city, location);
			if (!plot.isRentable())
				return;
			selection = RegionUtils.getSelection(plot.getRegion(), city.getWorld());
			price = plot.getPrice();
		} catch (Exception e) {
			return;
		}
		if (selection == null)
			return;
		final Sign sign = (Sign) event.getBlock().getState();
		if (!event.getLine(0).equalsIgnoreCase("[For Rent]") && !event.getLine(0).equalsIgnoreCase("[ForRent]") && !event.getLine(0).equalsIgnoreCase("[For-Rent]"))
			return;
		int low = selection.getMinimumPoint().getBlockY();
		int high = selection.getMaximumPoint().getBlockY();
		int area = selection.getArea() / selection.getHeight();
		sign.setLine(0, plugin.colorize("&4[&1ForRent&4]"));
		sign.setLine(1, plugin.colorize("&a" + area + "m\u00B2"));
		sign.setLine(2, plugin.colorize("&b" + low + " - " + high));
		sign.setLine(3, plugin.colorize("&5" + plugin.getEconomy().format(price)));
		Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
			@Override
			public void run() {
				sign.update();
			}
		}, 1);
	}
}
