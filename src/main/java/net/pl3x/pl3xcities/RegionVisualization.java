package net.pl3x.pl3xcities;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import com.sk89q.worldedit.BlockVector2D;
import com.sk89q.worldguard.protection.regions.ProtectedPolygonalRegion;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public class RegionVisualization {
	private Pl3xCities plugin;
	private Player player;
	private ProtectedRegion region;
	private long duration;
	private Material block;
	private byte data;

	public RegionVisualization(Pl3xCities plugin, Player player, ProtectedRegion region, long duration, Material block, byte data) {
		this.plugin = plugin;
		this.player = player;
		this.region = region;
		this.duration = duration;
		this.block = block;
		this.data = data;
	}

	private static BlockVector2D midpoint(BlockVector2D min, BlockVector2D max) {
		return min.add(max.subtract(min).divide(2)).toBlockVector2D();
	}

	private static BlockVector2D[] subdivideDiagonal(BlockVector2D[] points) {
		BlockVector2D[] newPoints = new BlockVector2D[(points.length * 2) - 1];
		int idx = 0;
		for (int i = 0; i < points.length - 1; i++) {
			newPoints[idx++] = points[i];
			newPoints[idx++] = midpoint(points[i], points[i + 1]);
		}
		newPoints[idx++] = points[points.length - 1];
		if (newPoints[0].distance(newPoints[1]) > 8)
			return subdivideDiagonal(newPoints);
		return newPoints;
	}

	private static List<Integer> subdivideLine(int min, int max) {
		ArrayList<Integer> points = new ArrayList<Integer>();
		int delta = max - min;
		int mid = min + (delta / 2);
		if (delta >= 4) {
			points.add(min + 1);
			points.add(max - 1);
			if (delta >= 5) {
				points.add(mid);
				int step = 8;
				if (delta / 2 >= 32) {
					step = 16;
				}
				if (delta / 2 >= step * 2) {
					for (int i = step; i + mid < max - 1; i += step) {
						points.add(mid + i);
						points.add(mid - i);
					}
				}
			}
		}
		return points;
	}

	private List<Location> getCuboidPoints() {
		World world = player.getWorld();
		int maxX = this.region.getMaximumPoint().getBlockX();
		int minX = this.region.getMinimumPoint().getBlockX();
		int maxY = this.region.getMaximumPoint().getBlockY();
		int minY = this.region.getMinimumPoint().getBlockY();
		int maxZ = this.region.getMaximumPoint().getBlockZ();
		int minZ = this.region.getMinimumPoint().getBlockZ();
		ArrayList<Location> points = new ArrayList<Location>(64);

		// 8 main corners
		points.add(new Location(world, minX, minY, minZ));
		points.add(new Location(world, minX, minY, maxZ));
		points.add(new Location(world, minX, maxY, maxZ));
		points.add(new Location(world, minX, maxY, minZ));
		points.add(new Location(world, maxX, maxY, maxZ));
		points.add(new Location(world, maxX, minY, minZ));
		points.add(new Location(world, maxX, minY, maxZ));
		points.add(new Location(world, maxX, maxY, minZ));

		// Fancier X - edge
		for (int i : subdivideLine(minX, maxX)) {
			points.add(new Location(world, i, minY, minZ));
			points.add(new Location(world, i, minY, maxZ));
			points.add(new Location(world, i, maxY, minZ));
			points.add(new Location(world, i, maxY, maxZ));
		}

		// Fancier Y - edge.
		for (int i : subdivideLine(minY, maxY)) {
			points.add(new Location(world, minX, i, minZ));
			points.add(new Location(world, minX, i, maxZ));
			points.add(new Location(world, maxX, i, minZ));
			points.add(new Location(world, maxX, i, maxZ));
		}

		// ground-level markers
		int y = world.getHighestBlockYAt(minX, minZ);
		if (y > minY && y < maxY)
			points.add(new Location(world, minX, y, minZ));
		y = world.getHighestBlockYAt(minX, maxZ);
		if (y > minY && y < maxY)
			points.add(new Location(world, minX, y, maxZ));
		y = world.getHighestBlockYAt(maxY, minZ);
		if (y > minY && y < maxY)
			points.add(new Location(world, maxX, y, minZ));
		y = world.getHighestBlockYAt(maxX, maxZ);
		if (y > minY && y < maxY)
			points.add(new Location(world, maxX, y, maxZ));

		// Fancier Z - edge
		for (int i : subdivideLine(minZ, maxZ)) {
			points.add(new Location(world, minX, minY, i));
			points.add(new Location(world, minX, maxY, i));
			points.add(new Location(world, maxX, minY, i));
			points.add(new Location(world, maxX, maxY, i));
		}
		return points;
	}

	private List<Location> getPolyPoints() {
		ProtectedPolygonalRegion poly = (ProtectedPolygonalRegion) region;
		ArrayList<Location> points = new ArrayList<Location>(5 * poly.getPoints().size());
		World world = player.getWorld();
		int minY = poly.getMinimumPoint().getBlockY();
		int maxY = poly.getMaximumPoint().getBlockY();
		List<Integer> yPoints = subdivideLine(minY, maxY);

		BlockVector2D point;
		BlockVector2D[] dots;
		for (int i = 0; i < poly.getPoints().size() - 1; i++) {
			point = poly.getPoints().get(i);
			dots = subdivideDiagonal(new BlockVector2D[] { point, poly.getPoints().get(i + 1) });
			for (int di = 1; di < dots.length - 1; di++) {
				points.add(new Location(world, dots[i].getBlockX(), minY, dots[i].getBlockZ()));
				points.add(new Location(world, dots[i].getBlockX(), maxY, dots[i].getBlockZ()));
			}
			points.add(new Location(world, point.getBlockX(), minY, point.getBlockZ()));
			points.add(new Location(world, point.getBlockX(), maxY, point.getBlockZ()));
			for (int y : yPoints) {
				points.add(new Location(world, point.getBlockX(), y, point.getBlockZ()));
			}
			int gy = world.getHighestBlockYAt(point.getBlockX(), point.getBlockZ());
			if (gy > minY && gy < maxY) {
				points.add(new Location(world, point.getBlockX(), gy, point.getBlockZ()));
			}
		}
		return points;
	}

	@SuppressWarnings("deprecation")
	private void show(List<Location> points) {
		if (player == null || !player.isOnline())
			return;
		for (Location l : points) {
			player.sendBlockChange(l, this.block, this.data);
		}
	}

	@SuppressWarnings("deprecation")
	private void hide(List<Location> points) {
		if (player == null || !player.isOnline())
			return;
		for (Location l : points) {
			player.sendBlockChange(l, l.getBlock().getType(), l.getBlock().getData());
		}
	}

	public void start() {
		final List<Location> points;

		if (region instanceof ProtectedPolygonalRegion)
			points = this.getPolyPoints();
		else
			points = this.getCuboidPoints();

		show(points);
		new BukkitRunnable() {
			@Override
			public void run() {
				hide(points);
			}
		}.runTaskLater(plugin, duration);
	}

	public static class Start implements Runnable {
		private RegionVisualization visualization;

		public Start(RegionVisualization visualization) {
			this.visualization = visualization;
		}

		@Override
		public void run() {
			visualization.start();
		}
	}
}
