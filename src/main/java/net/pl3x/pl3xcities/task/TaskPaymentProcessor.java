package net.pl3x.pl3xcities.task;

import net.pl3x.pl3xcities.Pl3xCities;

public class TaskPaymentProcessor implements Runnable {
	private Pl3xCities plugin;

	public TaskPaymentProcessor(Pl3xCities plugin) {
		this.plugin = plugin;
	}

	@Override
	public void run() {
		if (plugin.getConfig().getBoolean("city-taxes") || plugin.getConfig().getBoolean("plot-taxes"))
			plugin.getCityManager().processTaxes();
		if (plugin.getConfig().getBoolean("plot-rentals"))
			plugin.getPlotManager().processRent();
	}
}
