package net.pl3x.pl3xcities.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import net.pl3x.pl3xcities.Pl3xCities;
import net.pl3x.pl3xcities.cities.City;
import net.pl3x.pl3xcities.cities.Plot;
import net.pl3x.pl3xcities.flag.DisabledFlag;
import net.pl3x.pl3xcities.flag.TypeFlag;
import net.pl3x.pl3xcities.flag.TypeFlag.RegionType;
import net.pl3x.pl3xcities.flag.WorldFlag;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;

import com.sk89q.worldedit.Vector;
import com.sk89q.worldedit.bukkit.BukkitUtil;
import com.sk89q.worldedit.bukkit.selections.CuboidSelection;
import com.sk89q.worldedit.bukkit.selections.Polygonal2DSelection;
import com.sk89q.worldedit.bukkit.selections.Selection;
import com.sk89q.worldguard.domains.DefaultDomain;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.flags.BooleanFlag;
import com.sk89q.worldguard.protection.flags.CommandStringFlag;
import com.sk89q.worldguard.protection.flags.DefaultFlag;
import com.sk89q.worldguard.protection.flags.DoubleFlag;
import com.sk89q.worldguard.protection.flags.EntityTypeFlag;
import com.sk89q.worldguard.protection.flags.EnumFlag;
import com.sk89q.worldguard.protection.flags.Flag;
import com.sk89q.worldguard.protection.flags.IntegerFlag;
import com.sk89q.worldguard.protection.flags.InvalidFlagFormat;
import com.sk89q.worldguard.protection.flags.LocationFlag;
import com.sk89q.worldguard.protection.flags.RegionGroupFlag;
import com.sk89q.worldguard.protection.flags.SetFlag;
import com.sk89q.worldguard.protection.flags.StateFlag;
import com.sk89q.worldguard.protection.flags.StringFlag;
import com.sk89q.worldguard.protection.flags.VectorFlag;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedCuboidRegion;
import com.sk89q.worldguard.protection.regions.ProtectedPolygonalRegion;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import com.sk89q.worldguard.protection.regions.ProtectedRegion.CircularInheritanceException;

public class RegionUtils {
	private static Pl3xCities plugin = (Pl3xCities) Bukkit.getPluginManager().getPlugin("Pl3xCities");

	/**
	 * Create a new region and save to WorldGuard (City or Plot)
	 * 
	 * @param region - The ProtectedRegion
	 * @param owner - The owner's name (mayor)
	 * @param parent - The parent city region name (null if this is creating a city region)
	 * @throws Exception
	 */
	public static void createNewRegion(ProtectedRegion region, Player owner, String parent) throws Exception {
		RegionManager rManager = plugin.getWorldGuard().getRegionManager(owner.getWorld());
		DefaultDomain dd = new DefaultDomain();
		dd.addPlayer(owner.getName());
		region.setOwners(dd);
		region.setPriority(0);
		if (parent != null) {
			ProtectedRegion parentRegion = rManager.getRegion(parent);
			if (parentRegion == null)
				throw new Exception("&4Cannot find parent city region!");
			try {
				region.setParent(parentRegion);
			} catch (CircularInheritanceException e) {
				throw new Exception("&4Something went wrong! Tried to set parent to self!");
			}
		} else {
			// only cities get these flags.
			setCityDefaultFlags(region);
		}
		rManager.addRegion(region);
		saveRegion(region);
	}

	/**
	 * Redefines an existing region.
	 * 
	 * @param newRegion - The new ProtectedRegion
	 * @param oldRegion - The old ProtectedRegion
	 * @return The new ProtectedRegion
	 * @throws Exception
	 */
	public static ProtectedRegion redefineRegion(ProtectedRegion newRegion, ProtectedRegion oldRegion) throws Exception {
		RegionManager rManager = plugin.getWorldGuard().getRegionManager(WorldFlag.getWorld(oldRegion));
		newRegion.setMembers(oldRegion.getMembers());
		newRegion.setOwners(oldRegion.getOwners());
		newRegion.setFlags(oldRegion.getFlags());
		newRegion.setPriority(oldRegion.getPriority());
		try {
			newRegion.setParent(oldRegion.getParent());
		} catch (CircularInheritanceException e) {
			throw new Exception("&4ERROR: Oops, This error should never happen.");
		}
		rManager.addRegion(newRegion); // Replaces.
		saveRegion(newRegion);
		return newRegion;
	}

	/**
	 * Renames an existing region by creating a copy under a new name. All child regions get moved over to the new region.
	 * 
	 * @param oldRegion - The Old ProtectedRegion that will be replaced
	 * @param newName - the name for the new region
	 * @param childRegions - the childRegions of the region being redefined.
	 * @throws Exception
	 */
	public static void renameRegion(ProtectedRegion oldRegion, String newName, List<ProtectedRegion> childRegions) throws Exception {
		World world = WorldFlag.getWorld(oldRegion);
		RegionManager rManager = plugin.getWorldGuard().getRegionManager(world);

		// Create new region
		Selection selection = getSelection(oldRegion, world);
		if (selection == null)
			throw new Exception("&4Error: Could not get a selection from existing region!");
		ProtectedRegion newRegion = createRegion(selection, newName);
		if (newRegion == null)
			throw new Exception("&4Error: Could not create a new region from old region!");
		newRegion.setMembers(oldRegion.getMembers());
		newRegion.setOwners(oldRegion.getOwners());
		newRegion.setFlags(oldRegion.getFlags());
		newRegion.setPriority(oldRegion.getPriority());
		try {
			newRegion.setParent(oldRegion.getParent());
		} catch (CircularInheritanceException e) {
			throw new Exception("&Error: Oops, This error should never happen.");
		}
		rManager.addRegion(newRegion);

		// Set parent of childRegions to newRegion
		for (ProtectedRegion childRegion : childRegions) {
			String plotId = childRegion.getId().split("_plot")[1];
			selection = getSelection(childRegion, world);
			if (selection == null)
				continue;
			ProtectedRegion newChildRegion = createRegion(selection, newName + "_plot" + plotId);
			if (newChildRegion == null)
				continue;
			newChildRegion.setMembers(childRegion.getMembers());
			newChildRegion.setOwners(childRegion.getOwners());
			newChildRegion.setFlags(childRegion.getFlags());
			newChildRegion.setPriority(childRegion.getPriority());
			try {
				newChildRegion.setParent(newRegion);
			} catch (CircularInheritanceException e) {
				continue; // This should never happen...
			}
			rManager.addRegion(newChildRegion);
		}

		// Delete old region (removes child regions too)
		rManager.removeRegion(oldRegion.getId());

		// Save these changes to WorldGuard
		saveRegion(newRegion);
	}

	/**
	 * Deletes a region. All child regions are automagically deleted as well.
	 * 
	 * @param region - The ProtectedRegion to delete
	 * @throws Exception
	 */
	public static void deleteRegion(ProtectedRegion region) throws Exception {
		if (region == null)
			throw new Exception("&4Could not find region from WorldGuard!");
		RegionManager rManager = plugin.getWorldGuard().getRegionManager(WorldFlag.getWorld(region));
		rManager.removeRegion(region.getId());
		saveRegion(region);
	}

	/**
	 * Save region to WorldGuard
	 * 
	 * @param region - The ProtectedRegion to save
	 * @throws Exception
	 */
	public static void saveRegion(ProtectedRegion region) throws Exception {
		RegionManager rManager = plugin.getWorldGuard().getRegionManager(WorldFlag.getWorld(region));
		try {
			rManager.save();
		} catch (Exception e) {
			throw new Exception("&4Something went wrong! Region was not saved!");
		}
	}

	/**
	 * Checks if an ApplicableRegionSet contains a region that is part of a City
	 * 
	 * @param rSet - The ApplicableRegionSet
	 * @return boolean - True if the set contains a city
	 */
	public static boolean containsCity(ApplicableRegionSet rSet) {
		for (ProtectedRegion region : rSet) {
			if (plugin.getCityManager().isCityCreated(region.getId().split("_")[0]))
				return true;
		}
		return false;
	}

	/**
	 * Checks if ApplicableRegionSet contains a region that is part of a Plot
	 * 
	 * @param rSet - The ApplicableRegionSet
	 * @param except - Ignore this plot. (for resizing)
	 * @return boolean - True if the set contains a plot
	 */
	public static boolean containsPlot(ApplicableRegionSet rSet, Plot except) {
		for (ProtectedRegion region : rSet) {
			String regionId = region.getId();
			if (!regionId.contains("_plot"))
				continue;
			int id = -1;
			String[] values = regionId.split("_plot");
			if (!plugin.getCityManager().isCityCreated(values[0]))
				continue;
			City city = plugin.getCityManager().getCity(values[0]);
			try {
				id = Integer.valueOf(values[1]);
			} catch (Exception e) {
				continue;
			}
			Plot plot = city.getPlot(id);
			if (plot != null && plot != except) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Get a list of cities from an ApplicableRegionSet
	 * 
	 * @param rSet - The ApplicableRegionSet
	 * @return The List of cities
	 */
	public static List<City> getCities(ApplicableRegionSet rSet) {
		List<City> overlap = new ArrayList<City>();
		for (ProtectedRegion region : rSet) {
			if (!TypeFlag.getType(region).equals(RegionType.CITY))
				continue;
			City city = plugin.getCityManager().getCity(region.getId());
			if (overlap.contains(city))
				continue;
			overlap.add(city);
		}
		return overlap;
	}

	/**
	 * Get City by Location
	 * 
	 * @param location - A Bukkit Location
	 * @return The City object at this Location
	 * @throws Exception
	 */
	public static City getCity(Location location) throws Exception {
		List<City> cities = RegionUtils.getCities(getApplicableRegionSet(location));
		if (cities.size() < 1)
			throw new Exception("&4No city is defined here.");
		if (cities.size() > 1)
			throw new Exception("&4More than one city seems to be defined here.");
		return cities.get(0);
	}

	/**
	 * Get Plot by Location
	 * 
	 * @param location - A Bukkit Location
	 * @return The Plot object at this Location
	 * @throws Exception
	 */
	public static Plot getPlot(City city, Location location) throws Exception {
		ApplicableRegionSet set = getApplicableRegionSet(location);
		if (set == null || set.size() == 0)
			throw new Exception("&4No plot is defined here.");
		for (ProtectedRegion region : set) {
			if (!region.getId().contains("_plot"))
				continue;
			int id = -1;
			try {
				id = Integer.valueOf(region.getId().split("_plot")[1]);
			} catch (Exception e) {
				continue;
			}
			for (Plot plot : city.getPlots()) {
				if (plot.getId() == id)
					return plot;
			}
		}
		throw new Exception("&4No plot is defined here.");
	}

	/**
	 * Get the population of a city
	 * 
	 * @param city - The city
	 * @return The population
	 */
	public static int getCityPopulation(City city) {
		List<String> names = new ArrayList<String>();
		for (String name : city.getRegion().getOwners().getPlayers()) {
			if (!names.contains(name))
				names.add(name);
		}
		for (String name : city.getRegion().getMembers().getPlayers()) {
			if (!names.contains(name))
				names.add(name);
		}
		for (Plot plot : city.getPlots()) {
			for (String name : plot.getRegion().getOwners().getPlayers()) {
				if (!names.contains(name))
					names.add(name);
			}
			for (String name : plot.getRegion().getMembers().getPlayers()) {
				if (!names.contains(name))
					names.add(name);
			}
		}
		return names.size();
	}

	/**
	 * Get a resized Selection from a Selection after converting its min and max points to the world limits.
	 * 
	 * @param selection - A Selection
	 * @return A Selection or null
	 */
	public static Selection resizeSelection(Selection selection) {
		int maxHeight = Bukkit.getServer().getWorld(selection.getWorld().getName()).getMaxHeight() - 1;
		if (selection instanceof CuboidSelection)
			return new CuboidSelection(selection.getWorld(), selection.getNativeMinimumPoint().setY(0), selection.getNativeMaximumPoint().setY(maxHeight));
		if (selection instanceof Polygonal2DSelection)
			return new Polygonal2DSelection(selection.getWorld(), ((Polygonal2DSelection) selection).getNativePoints(), 0, maxHeight);
		return null;
	}

	/**
	 * Get a Selection from an existing ProtectedRegion
	 * 
	 * @param region - The ProtectedRegion
	 * @return The Selection or null
	 */
	public static Selection getSelection(ProtectedRegion region, World world) {
		if (region instanceof ProtectedCuboidRegion) {
			ProtectedCuboidRegion cuboid = (ProtectedCuboidRegion) region;
			Vector pt1 = cuboid.getMinimumPoint();
			Vector pt2 = cuboid.getMaximumPoint();
			return new CuboidSelection(world, pt1, pt2);
		}
		if (region instanceof ProtectedPolygonalRegion) {
			ProtectedPolygonalRegion poly2d = (ProtectedPolygonalRegion) region;
			return new Polygonal2DSelection(world, poly2d.getPoints(), poly2d.getMinimumPoint().getBlockY(), poly2d.getMaximumPoint().getBlockY());
		}
		return null;
	}

	/**
	 * Get an ApplicableRegionSet from a Selection
	 * 
	 * @param selection - A Selection
	 * @return The ApplicableRegionSet or null
	 */
	public static ApplicableRegionSet getApplicableRegionSet(Selection selection) {
		ProtectedRegion selRegion = createRegion(selection, "tmpname");
		if (selRegion == null)
			return null;
		RegionManager rManager = plugin.getWorldGuard().getRegionManager(selection.getWorld());
		if (rManager == null)
			return null;
		return rManager.getApplicableRegions(selRegion);
	}

	/**
	 * Get an ApplicableRegionSet from a Location
	 * 
	 * @param location - A Bukkit Location
	 * @return The ApplicableRegionSet or null
	 */
	public static ApplicableRegionSet getApplicableRegionSet(Location location) {
		RegionManager rManager = plugin.getWorldGuard().getRegionManager(location.getWorld());
		if (rManager == null)
			return null;
		return rManager.getApplicableRegions(location);
	}

	/**
	 * Get an exiting region from WorldGuard.
	 * 
	 * @param regionId - The string ID of the region (region name)
	 * @param world - The Bukkit World the region resides in
	 * @return The ProtectedRegion or null
	 */
	public static ProtectedRegion getRegion(String regionId, World world) {
		RegionManager rManager = plugin.getWorldGuard().getRegionManager(world);
		if (rManager == null)
			return null;
		return rManager.getRegionExact(regionId);
	}

	/**
	 * Create a new named region (extended bedrock to sky) from the current selection (not saved to WG)
	 * 
	 * @param selection - A Selection
	 * @param name - Name of the region
	 * @return The ProtectedRegion or null
	 */
	public static ProtectedRegion createRegion(Selection selection, String name) {
		if (selection instanceof CuboidSelection)
			return new ProtectedCuboidRegion(name, selection.getNativeMinimumPoint().toBlockVector(), selection.getNativeMaximumPoint().toBlockVector());
		if (selection instanceof Polygonal2DSelection)
			return new ProtectedPolygonalRegion(name, ((Polygonal2DSelection) selection).getNativePoints(), selection.getNativeMinimumPoint().getBlockY(), selection.getNativeMaximumPoint().getBlockY());
		return null;
	}

	/**
	 * Check if a plot's region is fully inside its parent city region.
	 * 
	 * @param selection - The plot Selection
	 * @param city - The parent City
	 * @return True if no parts of the plot region are outside the parent city region.
	 */
	public static boolean isPlotInsideCity(Selection selection, City city) {
		int minX = selection.getNativeMinimumPoint().getBlockX();
		int maxX = selection.getNativeMaximumPoint().getBlockX();
		int minZ = selection.getNativeMinimumPoint().getBlockZ();
		int maxZ = selection.getNativeMaximumPoint().getBlockZ();
		int y = selection.getNativeMinimumPoint().getBlockY();
		for (int x = minX; x <= maxX; x++) {
			for (int z = minZ; z <= maxZ; z++) {
				try {
					getCity(new Location(selection.getWorld(), x, y, z));
				} catch (Exception e) {
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * Find any forsale signs in a plot and change them to SOLD.
	 * 
	 * @param selection - The plot
	 * @param city - The parent City
	 */
	public static void changeForSaleSignToSold(Plot plot, City city) {
		int minX = plot.getRegion().getMinimumPoint().getBlockX();
		int minY = plot.getRegion().getMinimumPoint().getBlockY();
		int minZ = plot.getRegion().getMinimumPoint().getBlockZ();
		int maxX = plot.getRegion().getMaximumPoint().getBlockX();
		int maxY = plot.getRegion().getMaximumPoint().getBlockY();
		int maxZ = plot.getRegion().getMaximumPoint().getBlockZ();
		for (int x = minX; x <= maxX; x++) {
			for (int y = minY; y <= maxY; y++) {
				for (int z = minZ; z <= maxZ; z++) {
					try {
						getPlot(city, new Location(plot.getWorld(), x, y, z));
					} catch (Exception e) {
						continue;
					}
					Block block = plot.getWorld().getBlockAt(x, y, z);
					if (!(block.getState() instanceof Sign))
						continue;
					final Sign sign = (Sign) block.getState();
					if (!sign.getLine(0).equals(plugin.colorize("&4[&1ForSale&4]")) && !sign.getLine(0).equals(plugin.colorize("&4[&1ForRent&4]")))
						continue;
					sign.setLine(0, plugin.colorize("&1[&4 SOLD! &1]"));
					Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
						@Override
						public void run() {
							sign.update();
						}
					}, 1);
				}
			}
		}
	}

	/**
	 * Get a formatted list of the region's members
	 * 
	 * @param region - The ProtectedRegion
	 * @return Formatted string of member names
	 */
	public static String listMembers(ProtectedRegion region) {
		DefaultDomain dd = region.getMembers();
		if (dd.getPlayers().isEmpty())
			return "&onone";
		String list = "";
		for (String name : dd.getPlayers()) {
			if (list != "")
				list += "&7, ";
			list += "&e" + name;
		}
		return list;
	}

	/**
	 * Get the WorldEdit Location from a Bukkit Location for use with WorldGuard
	 * 
	 * @param location - The Bukkit Location
	 * @return A String representation of the Location that WorldGuard understands
	 */
	public static com.sk89q.worldedit.Location getLocation(Location location) {
		return new com.sk89q.worldedit.Location(BukkitUtil.getLocalWorld(location.getWorld()), new Vector(location.getBlockX(), location.getBlockY(), location.getBlockZ()), location.getYaw(), location.getPitch());
	}

	/**
	 * Set a flag for a region
	 * 
	 * @param region - The ProtectedRegion
	 * @param flag - The Flag
	 * @param value - The String value
	 * @throws Exception
	 */
	public static <V> void setFlag(ProtectedRegion region, Flag<V> flag, Object value) throws Exception {
		if (value != null)
			region.setFlag(flag, flag.parseInput(plugin.getWorldGuard(), Bukkit.getConsoleSender(), String.valueOf(value)));
		else
			region.setFlag(flag, null);
		saveRegion(region);
	}

	/**
	 * Compile a String with a region's flags
	 * 
	 * @param region - The ProtectedRegion
	 * @return String of flags and values (WorldGuard formatted)
	 */
	public static String getFlagsString(ProtectedRegion region) {
		if (region == null)
			return "&onone";
		String flagList = "";
		for (Flag<?> flag : DefaultFlag.getFlags()) {
			Object val = region.getFlag(flag);

			// If flag set?
			if (val == null)
				continue;

			// Is this flag an illegal internal flag?
			if (Utils.getIllegalFlags().contains(flag.getName()))
				continue;

			// Does server admin allow this flag to be set?
			if (!plugin.getConfig().getStringList("allowed-city-flags").contains(flag.getName()))
				continue;

			// Is this a plot?
			if (TypeFlag.getType(region).equals(RegionType.PLOT)) {
				// Does server admin allow this flag to be set?
				if (!plugin.getConfig().getStringList("allowed-plot-flags").contains(flag.getName()))
					continue;

				// Does city owner allow this flag to be set?
				if (DisabledFlag.isFlagDisabled(region.getParent(), flag.getName()))
					continue;
			}

			// Add to the list the list
			if (flagList != "")
				flagList += "&7, ";
			flagList += "&e" + flag.getName() + "&3: &6" + String.valueOf(val);
		}
		return (flagList == "") ? "&onone" : flagList;
	}

	/**
	 * Set the city's default flags set in config.yml
	 * 
	 * @param region - The ProtectedRegion getting the flags
	 */
	private static void setCityDefaultFlags(ProtectedRegion region) {
		List<Map<?, ?>> list = plugin.getConfig().getMapList("default-city-flags");
		if (list == null || list.isEmpty())
			return;
		Map<Flag<?>, Object> flags = region.getFlags();
		for (Map<?, ?> map : list) {
			for (Object obj : map.keySet()) {
				String key = (String) obj;
				Flag<?> flag = DefaultFlag.fuzzyMatchFlag(key);
				if (flag == null) {
					plugin.debug("&4Default flag not found: " + key);
					continue;
				}
				try {
					if (flag instanceof BooleanFlag)
						flags.put(flag, ((BooleanFlag) flag).parseInput(plugin.getWorldGuard(), Bukkit.getConsoleSender(), (String) map.get(key)));
					else if (flag instanceof CommandStringFlag)
						flags.put(flag, ((CommandStringFlag) flag).parseInput(plugin.getWorldGuard(), Bukkit.getConsoleSender(), (String) map.get(key)));
					else if (flag instanceof DoubleFlag)
						flags.put(flag, ((DoubleFlag) flag).parseInput(plugin.getWorldGuard(), Bukkit.getConsoleSender(), (String) map.get(key)));
					else if (flag instanceof EntityTypeFlag)
						flags.put(flag, ((EntityTypeFlag) flag).parseInput(plugin.getWorldGuard(), Bukkit.getConsoleSender(), (String) map.get(key)));
					else if (flag instanceof EnumFlag)
						flags.put(flag, ((EnumFlag<?>) flag).parseInput(plugin.getWorldGuard(), Bukkit.getConsoleSender(), (String) map.get(key)));
					else if (flag instanceof IntegerFlag)
						flags.put(flag, ((IntegerFlag) flag).parseInput(plugin.getWorldGuard(), Bukkit.getConsoleSender(), (String) map.get(key)));
					else if (flag instanceof LocationFlag)
						flags.put(flag, ((LocationFlag) flag).parseInput(plugin.getWorldGuard(), Bukkit.getConsoleSender(), (String) map.get(key)));
					else if (flag instanceof RegionGroupFlag)
						flags.put(flag, ((RegionGroupFlag) flag).parseInput(plugin.getWorldGuard(), Bukkit.getConsoleSender(), (String) map.get(key)));
					else if (flag instanceof SetFlag)
						flags.put(flag, ((SetFlag<?>) flag).parseInput(plugin.getWorldGuard(), Bukkit.getConsoleSender(), (String) map.get(key)));
					else if (flag instanceof StateFlag)
						flags.put(flag, ((StateFlag) flag).parseInput(plugin.getWorldGuard(), Bukkit.getConsoleSender(), (String) map.get(key)));
					else if (flag instanceof StringFlag)
						flags.put(flag, ((StringFlag) flag).parseInput(plugin.getWorldGuard(), Bukkit.getConsoleSender(), (String) map.get(key)));
					else if (flag instanceof VectorFlag)
						flags.put(flag, ((VectorFlag) flag).parseInput(plugin.getWorldGuard(), Bukkit.getConsoleSender(), (String) map.get(key)));
					else
						plugin.debug("&4Value of flag not found: " + map.get(key));
				} catch (InvalidFlagFormat e) {
					plugin.debug("&4Value of flag not found: " + map.get(key));
					continue;
				}
			}
		}
		region.setFlags(flags);
	}
}
