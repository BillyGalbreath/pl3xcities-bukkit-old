package net.pl3x.pl3xcities.util;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import net.pl3x.pl3xcities.cities.City;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import com.sk89q.worldedit.Vector;
import com.sk89q.worldguard.protection.flags.DefaultFlag;

public class Utils {
	public static List<String> illegalFlags = new ArrayList<String>() {
		private static final long serialVersionUID = 5227113826695720576L;
		{
			// Add WorldGuard default flags (Custom flags are added in their constructors)
			add("buyable");
			add("price");
			add("spawn");
		}
	};

	/**
	 * Check if city name is invalid name for WorldGuard. I purposefully have excluded "-" and "_" characters for internal use.
	 * 
	 * @param name - Name to check
	 * @return True if name is NOT valid
	 */
	public static boolean isInvalidName(String name) {
		return Pattern.compile("[^a-zA-Z0-9]").matcher(name).find();
	}

	/**
	 * Gets the permission node limit of the user. Maximum limit allowed is 1024. (Example: "pl3xcities.city.limit")
	 * 
	 * @param player - The user
	 * @param node - The permission node
	 * @return Integer limit of node
	 */
	public static Integer getLimit(Player player, String node) {
		Integer limit = 0;
		Integer maxlimit = 1024;
		if (player.hasPermission(node + ".*"))
			return -1;
		for (int i = 0; i < maxlimit; i++) {
			if (player.hasPermission(node + "." + i)) {
				if (i > limit)
					limit = i;
			}
		}
		return limit;
	}

	/**
	 * Get the spawn location of a city
	 * 
	 * @param city - The City object
	 * @return The Bukkit Location
	 */
	public static Location getSpawn(City city) {
		com.sk89q.worldedit.Location loc = (com.sk89q.worldedit.Location) city.getRegion().getFlag(DefaultFlag.SPAWN_LOC);
		Vector vec = loc.getPosition();
		return new Location(Bukkit.getWorld(loc.getWorld().getName()), vec.getBlockX(), vec.getBlockY(), vec.getBlockZ(), loc.getYaw(), loc.getPitch());
	}

	public static void addIllegalFlag(String flag) {
		illegalFlags.add(flag);
	}

	/**
	 * Get a list of illegal region flags. These flags are used internally and direct use of them can break functionality of the plugin.
	 * 
	 * @return List of illegal flags
	 */
	public static List<String> getIllegalFlags() {
		return illegalFlags;
	}

	/**
	 * Convert a duration in seconds to a string format
	 * 
	 * @param seconds A duration to convert to a string form
	 * @return A string of the form "X Days Y Hours Z Minutes A Seconds".
	 */
	public static String formatSeconds(long seconds) {
		if (seconds < 0) {
			throw new IllegalArgumentException("Duration must be greater than zero!");
		}

		long days = TimeUnit.SECONDS.toDays(seconds);
		seconds -= TimeUnit.DAYS.toSeconds(days);
		long hours = TimeUnit.SECONDS.toHours(seconds);
		seconds -= TimeUnit.HOURS.toSeconds(hours);
		long minutes = TimeUnit.SECONDS.toMinutes(seconds);
		seconds -= TimeUnit.MINUTES.toSeconds(minutes);

		StringBuilder sb = new StringBuilder(64);
		if (days > 0) {
			sb.append(days);
			sb.append(" Days ");
		}
		if (hours > 0) {
			sb.append(hours);
			sb.append(" Hr ");
		}
		if (minutes > 0) {
			sb.append(minutes);
			sb.append(" Min ");
		}
		if (seconds > 0) {
			sb.append(seconds);
			sb.append(" Sec ");
		}
		return (sb.toString());
	}
}
