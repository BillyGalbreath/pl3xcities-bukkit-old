package net.pl3x.pl3xcities.commands;

import net.pl3x.pl3xcities.Pl3xCities;
import net.pl3x.pl3xcities.commands.plot.BuyCommand;
import net.pl3x.pl3xcities.commands.plot.CreateCommand;
import net.pl3x.pl3xcities.commands.plot.DeleteCommand;
import net.pl3x.pl3xcities.commands.plot.ForRentCommand;
import net.pl3x.pl3xcities.commands.plot.ForSaleCommand;
import net.pl3x.pl3xcities.commands.plot.InfoCommand;
import net.pl3x.pl3xcities.commands.plot.RedefineCommand;
import net.pl3x.pl3xcities.commands.plot.ResetCommand;
import net.pl3x.pl3xcities.commands.plot.ResidentCommand;
import net.pl3x.pl3xcities.commands.plot.SelectCommand;
import net.pl3x.pl3xcities.commands.plot.SetCommand;

public class CmdPl3xPlot extends BaseCommand {
	public CmdPl3xPlot(Pl3xCities plugin) {
		super(plugin, "plot", "Manage a plot", "pl3xcities.command.plot", null);
		registerSubcommand(new BuyCommand(plugin));
		registerSubcommand(new CreateCommand(plugin));
		registerSubcommand(new DeleteCommand(plugin));
		registerSubcommand(new ForSaleCommand(plugin));
		registerSubcommand(new ForRentCommand(plugin));
		registerSubcommand(new InfoCommand(plugin));
		registerSubcommand(new ResetCommand(plugin));
		registerSubcommand(new ResidentCommand(plugin));
		registerSubcommand(new SelectCommand(plugin));
		registerSubcommand(new SetCommand(plugin));
		registerSubcommand(new RedefineCommand(plugin));
	}
}
