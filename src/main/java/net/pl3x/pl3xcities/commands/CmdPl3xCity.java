package net.pl3x.pl3xcities.commands;

import net.pl3x.pl3xcities.Pl3xCities;
import net.pl3x.pl3xcities.commands.city.BankCommand;
import net.pl3x.pl3xcities.commands.city.BuyCommand;
import net.pl3x.pl3xcities.commands.city.CreateCommand;
import net.pl3x.pl3xcities.commands.city.DeleteCommand;
import net.pl3x.pl3xcities.commands.city.ForSaleCommand;
import net.pl3x.pl3xcities.commands.city.InfoCommand;
import net.pl3x.pl3xcities.commands.city.OfficerCommand;
import net.pl3x.pl3xcities.commands.city.RedefineCommand;
import net.pl3x.pl3xcities.commands.city.SelectCommand;
import net.pl3x.pl3xcities.commands.city.SetCommand;
import net.pl3x.pl3xcities.commands.city.SpawnCommand;
import net.pl3x.pl3xcities.commands.city.VisualizeCommmand;

public class CmdPl3xCity extends BaseCommand {
	public CmdPl3xCity(Pl3xCities plugin) {
		super(plugin, "city", "Manage a city", "pl3xcities.command.city", null);
		registerSubcommand(new BankCommand(plugin));
		registerSubcommand(new BuyCommand(plugin));
		registerSubcommand(new CreateCommand(plugin));
		registerSubcommand(new DeleteCommand(plugin));
		registerSubcommand(new ForSaleCommand(plugin));
		registerSubcommand(new InfoCommand(plugin));
		registerSubcommand(new OfficerCommand(plugin));
		registerSubcommand(new RedefineCommand(plugin));
		registerSubcommand(new SelectCommand(plugin));
		registerSubcommand(new SetCommand(plugin));
		registerSubcommand(new SpawnCommand(plugin));
		registerSubcommand(new VisualizeCommmand(plugin));
	}
}
