package net.pl3x.pl3xcities.commands;

import net.pl3x.pl3xcities.Pl3xCities;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class CmdPl3xCities implements CommandExecutor {
	private Pl3xCities plugin;

	public CmdPl3xCities(Pl3xCities plugin) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		if (!cmd.getName().equalsIgnoreCase("pl3xcities"))
			return false;
		if (!cs.hasPermission("pl3xcities.command.pl3xcities")) {
			cs.sendMessage(plugin.colorize("&4You do not have permission for that command!"));
			plugin.log(cs.getName() + " was denied access to that command!");
			return true;
		}
		if (args.length > 0) {
			if (args[0].equalsIgnoreCase("reload")) {
				plugin.reloadConfig();
				cs.sendMessage(plugin.colorize("&d" + plugin.getName() + " config.yml has been reloaded."));
				plugin.debug(plugin.colorize("&aReloaded config.yml"));
				plugin.loadCities();
				cs.sendMessage(plugin.colorize("&dAll city/plot data has been reloaded."));
				plugin.loadTasks();
				cs.sendMessage(plugin.colorize("&dAll repeating tasks have been restarted."));
			}
		}
		cs.sendMessage(plugin.colorize("&d" + plugin.getName() + " v" + plugin.getDescription().getVersion()));
		return true;
	}
}
