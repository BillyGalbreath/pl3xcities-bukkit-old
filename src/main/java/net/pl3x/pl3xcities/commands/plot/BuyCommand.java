package net.pl3x.pl3xcities.commands.plot;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import net.milkbowl.vault.economy.EconomyResponse;
import net.pl3x.pl3xcities.Pl3xCities;
import net.pl3x.pl3xcities.cities.City;
import net.pl3x.pl3xcities.cities.Plot;
import net.pl3x.pl3xcities.commands.PlayerCommand;
import net.pl3x.pl3xcities.events.plot.PlotBuyEvent;
import net.pl3x.pl3xcities.util.RegionUtils;
import net.pl3x.pl3xcities.util.Utils;

import org.bukkit.command.Command;
import org.bukkit.entity.Player;

public class BuyCommand extends PlayerCommand {
	public BuyCommand(Pl3xCities plugin) {
		super(plugin, "buy", "Buy a plot", "pl3xcities.command.plot.buy", new String[] { "&dPurchase the plot you are standing in:", "   &e/&7plot buy" });
	}

	@Override
	public List<String> onTabComplete(Player player, Command command, String label, LinkedList<String> args) {
		return new ArrayList<String>();
	}

	@Override
	public boolean onCommand(Player player, Command command, String label, LinkedList<String> args) throws Exception {
		// Get city
		City city = RegionUtils.getCity(player.getLocation());
		if (city == null)
			throw new Exception("&4You are not standing in a city!");

		// Get plot
		Plot plot = RegionUtils.getPlot(city, player.getLocation());
		if (plot == null)
			throw new Exception("&4You are not standing in a plot!");

		// Check if plot is for sale
		boolean isForSale = plot.isBuyable();
		if (!isForSale)
			throw new Exception("&4This plot is not for sale!");

		// Check is buyer already owns the plot
		if (player.getName().equalsIgnoreCase(plot.getOwner()))
			throw new Exception("&4You already own this plot!");

		// Check the players owned-plot limit
		int count = 0;
		int limit = Utils.getLimit(player, "pl3xcities.plot.owned.limit");
		for (City c : plugin.getCityManager().getAllCities()) {
			for (Plot p : c.getPlots()) {
				if (p.getOwner().equals(player.getName()))
					count++;
			}
		}
		if (count >= limit && limit >= 0)
			throw new Exception("&4You have reached your limit of owned plots!");

		// Get the price of the plot
		double price = plot.getPrice();

		// Check for sufficient funds.
		double balance = plugin.getEconomy().getBalance(player.getName());
		if (price > balance)
			throw new Exception("&4You cannot afford to buy this plot! (cost: " + plugin.getEconomy().format(price) + ")");

		// Call the event and check for cancellation
		PlotBuyEvent event = new PlotBuyEvent(city, plot, player);
		plugin.getServer().getPluginManager().callEvent(event);
		if (event.isCancelled())
			return true;

		// Check funds
		EconomyResponse response = plugin.getEconomy().withdrawPlayer(player.getName(), price);
		if (!response.transactionSuccess())
			throw new Exception("&4An error has occurred trying to charge your account! " + response.errorMessage);

		String owner = plot.getOwner();
		EconomyResponse depositResponse = plugin.getEconomy().depositPlayer(owner, price);
		if (!depositResponse.transactionSuccess()) {
			EconomyResponse refundResponse = plugin.getEconomy().depositPlayer(player.getName(), price);
			if (!refundResponse.transactionSuccess())
				throw new Exception("&4Something really bad went wrong trying to complete your purchase! " + response.errorMessage);
			throw new Exception("&4An error has occurred trying to complete your purchase! " + response.errorMessage);
		}
		// Update buyable flag and new owner
		plot.setBuyable(false);
		plot.setOwner(player.getName());
		plot.setPaymentDue(Integer.MAX_VALUE);
		plugin.getPlotManager().refreshPlot(plot);
		// Scan for forsale signs and change them to sold
		RegionUtils.changeForSaleSignToSold(plot, city);

		// Finished.
		player.sendMessage(plugin.colorize("&dYou have purchased the plot for " + plugin.getEconomy().format(price)));
		return true;
	}
}
