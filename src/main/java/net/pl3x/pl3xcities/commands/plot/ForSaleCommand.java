package net.pl3x.pl3xcities.commands.plot;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import net.pl3x.pl3xcities.Pl3xCities;
import net.pl3x.pl3xcities.cities.City;
import net.pl3x.pl3xcities.cities.Plot;
import net.pl3x.pl3xcities.commands.PlayerCommand;
import net.pl3x.pl3xcities.events.plot.PlotForSaleEvent;
import net.pl3x.pl3xcities.util.RegionUtils;

import org.bukkit.command.Command;
import org.bukkit.entity.Player;

public class ForSaleCommand extends PlayerCommand {
	public ForSaleCommand(Pl3xCities plugin) {
		super(plugin, "forsale", "Toggle plot for-sale state", "pl3xcities.command.plot.forsale", new String[] { "&dView or set the plot's for-sale state:", "   &e/&7plot forsale [value]" });
	}

	@Override
	public List<String> onTabComplete(Player player, Command command, String label, LinkedList<String> args) {
		List<String> results = new ArrayList<String>();
		if (args.size() == 1) {
			if ("true".startsWith(args.peek().toLowerCase()))
				results.add("true");
			if ("false".startsWith(args.peek().toLowerCase()))
				results.add("false");
			if ("yes".startsWith(args.peek().toLowerCase()))
				results.add("yes");
			if ("no".startsWith(args.peek().toLowerCase()))
				results.add("no");
		}
		return results;
	}

	@Override
	public boolean onCommand(Player player, Command command, String label, LinkedList<String> args) throws Exception {
		// Check if server admin has disabled this feature.
		if (!plugin.getConfig().getBoolean("plot-resale", true))
			throw new Exception("&4Plot re-selling has been disabled!");

		// Get city
		City city = RegionUtils.getCity(player.getLocation());
		if (city == null)
			throw new Exception("&4You are not standing in a city!");

		// Get plot
		Plot plot = RegionUtils.getPlot(city, player.getLocation());
		if (plot == null)
			throw new Exception("You are not standing in a plot.");

		// Get the current state
		boolean state = plot.isBuyable();

		// Is the user actually setting the state?
		if (args.size() > 0) {
			// Check ownership
			if (!plot.getOwner().equalsIgnoreCase(player.getName()) && !city.getOwner().equalsIgnoreCase(player.getName()))
				throw new Exception("&4You cannot sell a plot you do not own!");

			// Check if plot is already up for rent
			if (plot.isRentable())
				throw new Exception("&4You cannot sell a plot that is up for rent!");

			// Get the new state value
			String value = args.peek();

			// Check the state value and convert to boolean
			boolean newState = false;
			if (value.equalsIgnoreCase("true") || value.equalsIgnoreCase("yes") || value.equalsIgnoreCase("1")) {
				newState = true;
			} else if (value.equalsIgnoreCase("false") || value.equalsIgnoreCase("no") || value.equalsIgnoreCase("0")) {
				newState = false;
			} else {
				throw new Exception("&4Not a yes/no value: " + value);
			}

			// Call the event and check for cancellation
			PlotForSaleEvent event = new PlotForSaleEvent(city, plot, player, state, newState);
			plugin.getServer().getPluginManager().callEvent(event);
			if (event.isCancelled())
				return true;
			state = event.getNewState();

			// Set the state
			plot.setBuyable(state);
		}

		// Finished.
		player.sendMessage(plugin.colorize("&dThis plot is " + (state ? "&a" : "&cNOT ") + "FOR SALE"));
		return true;
	}
}
