package net.pl3x.pl3xcities.commands.plot;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import net.pl3x.pl3xcities.Pl3xCities;
import net.pl3x.pl3xcities.cities.City;
import net.pl3x.pl3xcities.cities.Plot;
import net.pl3x.pl3xcities.commands.BaseCommand;
import net.pl3x.pl3xcities.commands.PlayerCommand;
import net.pl3x.pl3xcities.events.plot.PlotSetFlagEvent;
import net.pl3x.pl3xcities.events.plot.PlotSetPriceEvent;
import net.pl3x.pl3xcities.events.plot.PlotSetRentPriceEvent;
import net.pl3x.pl3xcities.flag.DisabledFlag;
import net.pl3x.pl3xcities.util.RegionUtils;
import net.pl3x.pl3xcities.util.Utils;

import org.bukkit.command.Command;
import org.bukkit.entity.Player;

import com.sk89q.worldguard.protection.flags.DefaultFlag;
import com.sk89q.worldguard.protection.flags.Flag;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public class SetCommand extends BaseCommand {
	public SetCommand(Pl3xCities plugin) {
		super(plugin, "set", "Set a plot setting", null, null);
		registerSubcommand(new SetPriceCommand(plugin));
		registerSubcommand(new SetRentPriceCommand(plugin));
		registerSubcommand(new SetFlagCommand(plugin));
	}

	public static class SetPriceCommand extends PlayerCommand {
		public SetPriceCommand(Pl3xCities plugin) {
			super(plugin, "price", "Set the plot price", "pl3xcities.command.plot.set.price", new String[] { "&dSet the price of the plot:", "   &e/&7plot set price <value>" });
		}

		@Override
		public List<String> onTabComplete(Player player, Command command, String label, LinkedList<String> args) {
			return new ArrayList<String>();
		}

		@Override
		public boolean onCommand(Player player, Command command, String label, LinkedList<String> args) throws Exception {
			// Check if server admin has disabled this feature.
			if (!plugin.getConfig().getBoolean("plot-resale", true))
				throw new Exception("&4Plot selling has been disabled!");
			if (!plugin.getConfig().getBoolean("plot-custom-pricing", true))
				throw new Exception("&4Custom plot pricing has been disabled!");

			// Check for price
			if (args.isEmpty())
				throw new Exception("&4You must supply a price value!");

			// Get city
			City city = RegionUtils.getCity(player.getLocation());
			if (city == null)
				throw new Exception("&4You are not standing in a city!");

			// Get plot
			Plot plot = RegionUtils.getPlot(city, player.getLocation());
			if (plot == null)
				throw new Exception("&4You are not standing in a plot!");

			// Check ownership
			if (!plot.getOwner().equalsIgnoreCase(player.getName()) && !city.getOwner().equalsIgnoreCase(player.getName()))
				throw new Exception("&4You cannot manage a plot you do not own!");

			// Check if plot is up for sale
			if (plot.isBuyable())
				throw new Exception("&4This plot is marked for sale! Cannot change the price!");

			// Convert string to double
			double price;
			try {
				price = Double.parseDouble(args.peek().trim());
			} catch (IllegalArgumentException ex) {
				throw new Exception("&4'" + args.peek().trim() + "' is not a valid price!");
			}

			// No negative prices
			if (price < 0)
				throw new Exception("&4Price cannot be negative!");

			// Call the event and check for cancellation
			PlotSetPriceEvent event = new PlotSetPriceEvent(city, plot, player, price);
			plugin.getServer().getPluginManager().callEvent(event);
			if (event.isCancelled())
				return true;
			price = event.getPrice();

			// Set the price
			plot.setPrice(price);

			// Finished.
			player.sendMessage(plugin.colorize("&dPlot price set to " + plugin.getEconomy().format(price)));
			return true;
		}
	}

	public static class SetRentPriceCommand extends PlayerCommand {
		public SetRentPriceCommand(Pl3xCities plugin) {
			super(plugin, "rent-price", "Set the plot rent price", "pl3xcities.command.plot.set.rentprice", new String[] { "&dSet the rent price of the plot:", "   &e/&7plot set rent-price <value>" });
		}

		@Override
		public List<String> onTabComplete(Player player, Command command, String label, LinkedList<String> args) {
			return new ArrayList<String>();
		}

		@Override
		public boolean onCommand(Player player, Command command, String label, LinkedList<String> args) throws Exception {
			// Check if server admin has disabled this feature.
			if (!plugin.getConfig().getBoolean("plot-custom-pricing", true))
				throw new Exception("&4Custom plot pricing has been disabled!");

			// Check for price
			if (args.isEmpty())
				throw new Exception("&4You must supply a price value!");

			// Get city
			City city = RegionUtils.getCity(player.getLocation());
			if (city == null)
				throw new Exception("&4You are not standing in a city!");

			// Get plot
			Plot plot = RegionUtils.getPlot(city, player.getLocation());
			if (plot == null)
				throw new Exception("&4You are not standing in a plot!");

			// Check ownership
			if (!plot.getOwner().equalsIgnoreCase(player.getName()))
				throw new Exception("&4You cannot manage a plot you do not own!");

			// Check if plot is up for sale
			if (plot.isRentable())
				throw new Exception("&4This plot is marked for rent! Cannot change the rent price!");

			// Convert string to double
			double price;
			try {
				price = Double.parseDouble(args.peek().trim());
			} catch (IllegalArgumentException ex) {
				throw new Exception("&4'" + args.peek().trim() + "' is not a valid price!");
			}

			// No negative prices
			if (price < 0)
				throw new Exception("&4Price cannot be negative!");

			// Call the event and check for cancellation
			PlotSetRentPriceEvent event = new PlotSetRentPriceEvent(city, plot, player, price);
			plugin.getServer().getPluginManager().callEvent(event);
			if (event.isCancelled())
				return true;
			price = event.getPrice();

			// Set the price
			plot.setRentPrice(price);

			// Finished.
			player.sendMessage(plugin.colorize("&dPlot rent price set to " + plugin.getEconomy().format(price)));
			return true;
		}
	}

	public static class SetFlagCommand extends PlayerCommand {
		public SetFlagCommand(Pl3xCities plugin) {
			super(plugin, "flag", "Set a plot flag", "pl3xcities.command.plot.set.flag", new String[] { "&dSets a WorldGuard flag for the plot region:", "   &e/&7plot set flag <flag> [value]" });
		}

		@Override
		public List<String> onTabComplete(Player player, Command command, String label, LinkedList<String> args) {
			List<String> results = new ArrayList<String>();
			if (args.size() == 1) {
				for (Flag<?> flag : DefaultFlag.getFlags()) {
					if (flag.getName().toLowerCase().startsWith(args.peek()))
						results.add(flag.getName());
				}
			}
			return results;
		}

		@Override
		public boolean onCommand(Player player, Command command, String label, LinkedList<String> args) throws Exception {
			// Show help
			if ("?".equals(args.peek())) {
				showHelp(player);
				return true;
			}

			// Check for flag
			if (args.isEmpty())
				throw new Exception("&4You must specify a flag to set!");

			// Get the name of the flag
			String flagName = args.pop();

			// Get city
			City city = RegionUtils.getCity(player.getLocation());
			if (city == null)
				throw new Exception("&4You are not standing in a city.");

			// Get plot
			Plot plot = RegionUtils.getPlot(city, player.getLocation());
			if (plot == null)
				throw new Exception("&4You are not standing in a plot.");

			// Check ownership
			if (!player.getName().equalsIgnoreCase(city.getOwner()))
				throw new Exception("&4You cannot manage a plot you do not own!");

			// Is this flag an illegal internal flag?
			if (Utils.getIllegalFlags().contains(flagName))
				throw new Exception("&4This flag is reserved for internal use only!");

			// Does server admin allow this flag to be set?
			if (!plugin.getConfig().getStringList("allowed-city-flags").contains(flagName))
				throw new Exception("&4This flag has been disabled!");

			// Does server admin allow this flag to be set?
			if (!plugin.getConfig().getStringList("allowed-plot-flags").contains(flagName))
				throw new Exception("&4This flag has been disabled!");

			// Does city owner allow this flag to be set?
			if (DisabledFlag.isFlagDisabled(city.getRegion(), flagName))
				throw new Exception("&4This flag has been disabled!");

			// Get the region
			ProtectedRegion region = plot.getRegion();
			if (region == null)
				throw new Exception("&4Something went wrong! Cannot find plot region!");

			// Get the flag
			Flag<?> flag = DefaultFlag.fuzzyMatchFlag(flagName);
			if (flag == null)
				throw new Exception("&4That flag doesnt exist!");

			// Get the flag value to be set
			String value = null;
			if (!args.isEmpty())
				value = args.pop();

			// Call event and check for cancellation
			PlotSetFlagEvent event = new PlotSetFlagEvent(city, plot, player, flag, region.getFlag(flag), value);
			plugin.getServer().getPluginManager().callEvent(event);
			if (event.isCancelled())
				return true;
			value = event.getNewValue();

			// Set the flag
			try {
				RegionUtils.setFlag(region, flag, value);
			} catch (Exception e) {
				throw new Exception("&4" + e.getMessage()); // WorldGuard has good error messages here
			}

			// Finished.
			String message = (value != null) ? "&dFlag was set." : "&dFlag was removed.";
			player.sendMessage(plugin.colorize(message));
			return true;
		}
	}
}
