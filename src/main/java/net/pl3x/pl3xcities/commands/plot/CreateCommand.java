package net.pl3x.pl3xcities.commands.plot;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import net.pl3x.pl3xcities.Pl3xCities;
import net.pl3x.pl3xcities.cities.City;
import net.pl3x.pl3xcities.cities.Plot;
import net.pl3x.pl3xcities.commands.PlayerCommand;
import net.pl3x.pl3xcities.events.plot.PlotCreateEvent;
import net.pl3x.pl3xcities.flag.TypeFlag;
import net.pl3x.pl3xcities.flag.WorldFlag;
import net.pl3x.pl3xcities.util.RegionUtils;
import net.pl3x.pl3xcities.util.Utils;

import org.bukkit.command.Command;
import org.bukkit.entity.Player;

import com.sk89q.worldedit.bukkit.selections.CuboidSelection;
import com.sk89q.worldedit.bukkit.selections.Selection;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public class CreateCommand extends PlayerCommand {
	public CreateCommand(Pl3xCities plugin) {
		super(plugin, "create", "Create a new plot", "pl3xcities.command.plot.create", new String[] { "&dCreate a new plot from your current WorldEdit wand selection:", "   &e/&7plot create" });
	}

	@Override
	public List<String> onTabComplete(Player player, Command command, String label, LinkedList<String> args) {
		return new ArrayList<String>();
	}

	@Override
	public boolean onCommand(Player player, Command command, String label, LinkedList<String> args) throws Exception {
		// Get WorldEdit Selection
		Selection selection = plugin.getWorldEdit().getSelection(player);
		// Expand region vert min/max?
		if ("-vert".equalsIgnoreCase(args.peek()))
			selection = RegionUtils.resizeSelection(selection);
		if (selection == null)
			throw new Exception("&4You have not selected a region.");
		if (!(selection instanceof CuboidSelection))
			throw new Exception("&4Only cuboid selections are allowed for plots!");

		// Check the size requirements of the selection
		int regionArea = selection.getArea() / selection.getHeight();
		int minPlot = plugin.getConfig().getInt("minimum-plot-area", 100);
		if (regionArea < minPlot)
			throw new Exception("&4Minimum area for a plot is " + minPlot + ". (" + regionArea + ")");
		int maxPlot = plugin.getConfig().getInt("maximum-plot-area", 10000);
		if (regionArea > maxPlot)
			throw new Exception("&4Maximum area for a plot is " + maxPlot + ". (" + regionArea + ")");

		// Check for overlapping regions
		ApplicableRegionSet rSet = RegionUtils.getApplicableRegionSet(selection);
		if (rSet.size() <= 0)
			throw new Exception("&4Your current selection is not in a city!");

		// Check for cities
		List<City> cities = RegionUtils.getCities(rSet);
		if (cities.isEmpty())
			throw new Exception("&4Your current selection is not in a city!");
		if (cities.size() > 1)
			throw new Exception("&4Your current selection is overlapping multiple cities!");

		// Get the city
		City city = cities.get(0);

		// Check ownership
		if (!city.getOwner().equalsIgnoreCase(player.getName()))
			throw new Exception("&4You cannot create a plot in a city you do not own!");

		// Check for existing plots
		if (RegionUtils.containsPlot(rSet, null))
			throw new Exception("&4Your current selection overlaps an existing plot!");

		// Check if plot is all the way inside of the city.
		if (!RegionUtils.isPlotInsideCity(selection, city))
			throw new Exception("&4Your plot cannot spread outside of your city!");

		// Check how many plots the player already has in _this_ city.
		int limit = Utils.getLimit(player, "pl3xcities.plot.create.limit");
		if (city.getPlots().size() >= limit && limit >= 0)
			throw new Exception("&4You have reached your plot limit for this city!");

		// Check funds
		double costPerBlock = plugin.getConfig().getDouble("cost-per-plot-block-buy", 1.0);
		double costTotal = (regionArea * costPerBlock);
		double balance = city.getBankBalance();
		if (costTotal > balance)
			throw new Exception("&4Not enough bank funds to create this plot! (cost: " + plugin.getEconomy().format(costTotal) + ")");

		// Create the new region
		int next = city.getNextPlotId();
		ProtectedRegion region = RegionUtils.createRegion(selection, city.getName().toLowerCase() + "_plot" + next);
		region.setFlag(WorldFlag.flag, player.getWorld().getName());
		region.setFlag(TypeFlag.flag, TypeFlag.RegionType.PLOT.toString());

		// Create the new plot and save region to WorldGuard
		RegionUtils.createNewRegion(region, player, city.getName());
		Plot plot = new Plot(next, region);
		plot.setOwner(player.getName());
		plot.setPrice(costTotal);
		plot.setRentPrice(plugin.getConfig().getDouble("cost-per-plot-block-rent", 0.1));
		plot.setPaymentDue(Integer.MAX_VALUE);
		plot.setRentalPeriod(plugin.getConfig().getInt("plot-rental-period", 1200));
		city.addPlot(plot);
		plugin.getPlotManager().addPlot(plot);

		// Call the event and check for cancellation
		PlotCreateEvent event = new PlotCreateEvent(city, plot, player);
		plugin.getServer().getPluginManager().callEvent(event);
		if (event.isCancelled()) {
			city.removePlot(plot);
			plugin.getPlotManager().removePlot(plot);
			return true;
		}

		// Charge the account
		city.addToBank(-costTotal);

		// Update player's selection now that the command has succeeded.
		if (plugin.getConfig().getBoolean("auto-update-player-selection", false))
			plugin.getWorldEdit().setSelection(player, selection);

		// Finished.
		player.sendMessage(plugin.colorize("&dYou have successfully created an empty plot!"));
		player.sendMessage(plugin.colorize("&d" + plugin.getEconomy().format(costTotal) + " has been deducted from your account."));
		return true;
	}
}
