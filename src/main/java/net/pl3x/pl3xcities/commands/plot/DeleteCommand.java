package net.pl3x.pl3xcities.commands.plot;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import net.pl3x.pl3xcities.Pl3xCities;
import net.pl3x.pl3xcities.cities.City;
import net.pl3x.pl3xcities.cities.Plot;
import net.pl3x.pl3xcities.commands.PlayerCommand;
import net.pl3x.pl3xcities.events.plot.PlotDeleteEvent;
import net.pl3x.pl3xcities.util.RegionUtils;

import org.bukkit.command.Command;
import org.bukkit.entity.Player;

public class DeleteCommand extends PlayerCommand {
	public DeleteCommand(Pl3xCities plugin) {
		super(plugin, "delete", "Delete a plot", "pl3xcities.command.plot.delete", new String[] { "&dDelete a plot from existance:", "   &e/&7plot delete [id]" });
	}

	@Override
	public List<String> onTabComplete(Player player, Command command, String label, LinkedList<String> args) {
		return new ArrayList<String>();
	}

	@Override
	public boolean onCommand(Player player, Command command, String label, LinkedList<String> args) throws Exception {
		// Id must be specified
		if (args.isEmpty())
			throw new Exception("&4You must specify a plot id!");

		// Get city
		City city = RegionUtils.getCity(player.getLocation());
		if (city == null)
			throw new Exception("&4You are not standing in a city, thus no plots here!");

		// Check ownership.
		// Only city owners can delete the plot. Not the plot owner.
		if (!player.getName().equalsIgnoreCase(city.getOwner()))
			throw new Exception("&4You do not own this city!");

		// Get plot
		int id = -1;
		try {
			id = Integer.valueOf(args.peek().trim());
		} catch (Exception e) {
			throw new Exception("&4Plot IDs are numbers only!");
		}
		Plot plot = city.getPlot(id);
		if (plot == null)
			throw new Exception("&4Cannot find plot with id of " + id);
		if (plot != RegionUtils.getPlot(city, player.getLocation()))
			throw new Exception("&4You must be standing in the plot to delete it!");

		// Call the event and check for cancellation
		PlotDeleteEvent event = new PlotDeleteEvent(city, plot, player);
		plugin.getServer().getPluginManager().callEvent(event);
		if (event.isCancelled())
			return true;

		// Delete plot region from WorldGuard
		RegionUtils.deleteRegion(plot.getRegion());

		// Delete the plot from the city object and the plot manager
		city.removePlot(plot);
		plugin.getPlotManager().removePlot(plot);

		// Finished.
		player.sendMessage(plugin.colorize("&dYou have successfully deleted the plot."));
		return true;
	}
}
