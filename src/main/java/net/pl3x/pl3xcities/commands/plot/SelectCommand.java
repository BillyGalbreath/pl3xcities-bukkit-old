package net.pl3x.pl3xcities.commands.plot;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import net.pl3x.pl3xcities.Pl3xCities;
import net.pl3x.pl3xcities.cities.City;
import net.pl3x.pl3xcities.cities.Plot;
import net.pl3x.pl3xcities.commands.PlayerCommand;
import net.pl3x.pl3xcities.events.plot.PlotSelectEvent;
import net.pl3x.pl3xcities.util.RegionUtils;

import org.bukkit.command.Command;
import org.bukkit.entity.Player;

public class SelectCommand extends PlayerCommand {

	public SelectCommand(Pl3xCities plugin) {
		super(plugin, "select", "Create a WorldEdit selection matching the plot's boundaries", "pl3xcities.command.plot.select", new String[] { "&dSelect the plot's region with the WorldEdit wand:", "   &e/&7plot select" });
	}

	@Override
	public List<String> onTabComplete(Player player, Command command, String label, LinkedList<String> args) {
		return new ArrayList<String>();
	}

	public boolean onCommand(Player player, Command command, String label, LinkedList<String> args) throws Exception {
		// Get the city
		City city = RegionUtils.getCity(player.getLocation());
		if (city == null)
			throw new Exception("&4You are not standing in a city!");

		// Get the plot
		Plot plot = RegionUtils.getPlot(city, player.getLocation());
		if (plot == null)
			throw new Exception("&4You are not standing in a plot!");

		// Call the event and check for cancellation
		PlotSelectEvent event = new PlotSelectEvent(city, plot, player);
		plugin.getServer().getPluginManager().callEvent(event);
		if (event.isCancelled())
			return true;

		// Update user's selection
		plugin.getWorldEdit().setSelection(player, RegionUtils.getSelection(plot.getRegion(), city.getWorld()));

		// Finished.
		player.sendMessage(plugin.colorize("&dYou now have the plot selected."));
		return true;
	}
}
