package net.pl3x.pl3xcities.commands.plot;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import net.pl3x.pl3xcities.Pl3xCities;
import net.pl3x.pl3xcities.cities.City;
import net.pl3x.pl3xcities.cities.Plot;
import net.pl3x.pl3xcities.commands.PlayerCommand;
import net.pl3x.pl3xcities.events.plot.PlotResetEvent;
import net.pl3x.pl3xcities.util.RegionUtils;

import org.bukkit.command.Command;
import org.bukkit.entity.Player;

import com.sk89q.worldedit.EditSession;
import com.sk89q.worldedit.LocalPlayer;
import com.sk89q.worldedit.LocalSession;
import com.sk89q.worldedit.masks.Mask;
import com.sk89q.worldedit.regions.Region;

public class ResetCommand extends PlayerCommand {
	public ResetCommand(Pl3xCities plugin) {
		super(plugin, "reset", "Reset a plot", "pl3xcities.command.plot.reset", new String[] { "&dReset the plot to the world seed:", "   &e/&7plot reset" });
	}

	@Override
	public List<String> onTabComplete(Player player, Command command, String label, LinkedList<String> args) {
		return new ArrayList<String>();
	}

	@Override
	public boolean onCommand(Player player, Command command, String label, LinkedList<String> args) throws Exception {
		// Get city
		City city = RegionUtils.getCity(player.getLocation());
		if (city == null)
			throw new Exception("&4You are not standing in a city, thus no plots here!");

		// Get plot
		Plot plot = RegionUtils.getPlot(city, player.getLocation());
		if (plot == null)
			throw new Exception("&4You are not standing in a plot!");

		// Check ownership
		if (!plot.getOwner().equalsIgnoreCase(player.getName()) && !city.getOwner().equalsIgnoreCase(player.getName()))
			throw new Exception("&4You cannot manage a plot you do not own!");

		// Call the event and check for cancellation
		PlotResetEvent event = new PlotResetEvent(city, plot, player);
		plugin.getServer().getPluginManager().callEvent(event);
		if (event.isCancelled())
			return true;

		try {
			plugin.getWorldEdit().setSelection(player, RegionUtils.getSelection(plot.getRegion(), city.getWorld()));
			LocalPlayer localPlayer = plugin.getWorldEdit().wrapPlayer(player);
			LocalSession localSession = plugin.getWorldEdit().getSession(player);
			EditSession editSession = localSession.createEditSession(localPlayer);
			Region region = localSession.getSelection(localPlayer.getWorld());
			Mask mask = localSession.getMask();
			localSession.setMask(null);
			localPlayer.getWorld().regenerate(region, editSession);
			localSession.setMask(mask);
		} catch (Exception e) {
			throw new Exception("&4Something went wrong!");
		}

		// Finished.
		player.sendMessage(plugin.colorize("&dYou have successfully reset the plot."));
		return true;
	}
}
