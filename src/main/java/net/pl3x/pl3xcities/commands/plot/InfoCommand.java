package net.pl3x.pl3xcities.commands.plot;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import net.pl3x.pl3xcities.Pl3xCities;
import net.pl3x.pl3xcities.cities.City;
import net.pl3x.pl3xcities.cities.Plot;
import net.pl3x.pl3xcities.commands.PlayerCommand;
import net.pl3x.pl3xcities.util.RegionUtils;
import net.pl3x.pl3xcities.util.Utils;

import org.bukkit.command.Command;
import org.bukkit.entity.Player;

import com.sk89q.worldedit.bukkit.selections.Selection;

/**
 * Get information about a plot and display it to the CommandSender.
 * 
 */
public class InfoCommand extends PlayerCommand {
	public InfoCommand(Pl3xCities plugin) {
		super(plugin, "info", "Display info about a plot", "pl3xcities.command.plot.info", new String[] { "&dGet information about the plot:", "   &e/&7plot info" });
	}

	@Override
	public List<String> onTabComplete(Player player, Command command, String label, LinkedList<String> args) {
		return new ArrayList<String>();

	}

	@Override
	public boolean onCommand(Player player, Command command, String label, LinkedList<String> args) throws Exception {
		// Get city
		City city = RegionUtils.getCity(player.getLocation());
		if (city == null)
			throw new Exception("&4You are not standing in a city, thus no plots here!");

		// Get plot
		Plot plot = null;
		if (!args.isEmpty()) {
			String idStr = args.peek();
			int id = -1;
			try {
				id = Integer.valueOf(idStr);
			} catch (Exception e) {
				throw new Exception("&4Plot IDs are numbers only!");
			}
			plot = city.getPlot(id);
			if (plot == null)
				throw new Exception("&4Cannot find plot with id of " + id + " in this city.");
		} else
			plot = RegionUtils.getPlot(city, player.getLocation());

		// Do some calculations
		Selection selection = RegionUtils.getSelection(plot.getRegion(), plot.getWorld());
		int area = selection.getArea() / selection.getHeight();
		double tax = (plugin.getConfig().getDouble("cost-per-plot-block-buy", 1.0) * (plugin.getConfig().getDouble("plot-tax-percent", 1.0) / 100)) * area;
		long secondsDue = city.getPaymentDue() - (System.currentTimeMillis() / 1000);

		// Show the info
		player.sendMessage(plugin.colorize("&dPlot info for: " + plot.getRegion().getId()));
		player.sendMessage(plugin.colorize("   &dId: &7" + plot.getId()));
		player.sendMessage(plugin.colorize("   &dOwner: &7" + plot.getOwner()));
		player.sendMessage(plugin.colorize("   &dResidents: &7" + RegionUtils.listMembers(plot.getRegion())));
		player.sendMessage(plugin.colorize("   &dForSale: &7" + plot.isBuyable()));
		player.sendMessage(plugin.colorize("   &dForRent: &7" + plot.isRentable()));
		player.sendMessage(plugin.colorize("   &dPrice: &7" + plugin.getEconomy().format(plot.getPrice())));
		player.sendMessage(plugin.colorize("   &dArea: &7" + area + " m\u00B2"));
		player.sendMessage(plugin.colorize("   &dTaxes: &7 " + plugin.getEconomy().format(tax) + " due in " + Utils.formatSeconds(secondsDue)));
		player.sendMessage(plugin.colorize("   &dFlags: &7 " + RegionUtils.getFlagsString(plot.getRegion())));
		return true;
	}
}
