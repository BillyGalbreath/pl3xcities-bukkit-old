package net.pl3x.pl3xcities.commands.plot;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import net.milkbowl.vault.economy.EconomyResponse;
import net.pl3x.pl3xcities.Pl3xCities;
import net.pl3x.pl3xcities.cities.City;
import net.pl3x.pl3xcities.cities.Plot;
import net.pl3x.pl3xcities.commands.PlayerCommand;
import net.pl3x.pl3xcities.events.plot.PlotRentEvent;
import net.pl3x.pl3xcities.util.RegionUtils;
import net.pl3x.pl3xcities.util.Utils;

import org.bukkit.command.Command;
import org.bukkit.entity.Player;

public class RentCommand extends PlayerCommand {
	public RentCommand(Pl3xCities plugin) {
		super(plugin, "rent", "Rent a plot", "pl3xcities.command.plot.rent", new String[] { "&dRent the plot you are standing in:", "   &e/&7plot rent" });
	}

	@Override
	public List<String> onTabComplete(Player player, Command command, String label, LinkedList<String> args) {
		return new ArrayList<String>();
	}

	@Override
	public boolean onCommand(Player player, Command command, String label, LinkedList<String> args) throws Exception {
		// Get city
		City city = RegionUtils.getCity(player.getLocation());
		if (city == null)
			throw new Exception("&4You are not standing in a city!");

		// Get plot
		Plot plot = RegionUtils.getPlot(city, player.getLocation());
		if (plot == null)
			throw new Exception("&4You are not standing in a plot!");

		// Check if plot is for sale
		boolean isForRent = plot.isRentable();
		if (!isForRent)
			throw new Exception("&4This plot is not for rent!");

		// Check is buyer already owns the plot
		if (player.getName().equalsIgnoreCase(plot.getOwner()))
			throw new Exception("&4You already own this plot!");

		// Check the players owned-plot limit
		int count = 0;
		int limit = Utils.getLimit(player, "pl3xcities.plot.owned.limit");
		for (City c : plugin.getCityManager().getAllCities()) {
			for (Plot p : c.getPlots()) {
				if (p.getOwner().equals(player.getName()))
					count++;
			}
		}
		if (count >= limit && limit >= 0)
			throw new Exception("&4You have reached your limit of owned plots!");

		// Get the price of the plot
		double price = plot.getRentPrice();

		// Check for sufficient funds.
		double balance = plugin.getEconomy().getBalance(player.getName());
		if (price > balance)
			throw new Exception("&4You cannot afford to rent this plot! (cost: " + plugin.getEconomy().format(price) + ")");

		// Call the event and check for cancellation
		PlotRentEvent event = new PlotRentEvent(city, plot, player);
		plugin.getServer().getPluginManager().callEvent(event);
		if (event.isCancelled())
			return true;

		// Check funds
		EconomyResponse response = plugin.getEconomy().withdrawPlayer(player.getName(), price);
		if (!response.transactionSuccess())
			throw new Exception("&4An error has occurred trying to charge your account! " + response.errorMessage);

		// Update rentable flag and new owner
		plot.setRentable(false);
		plot.setRenter(player.getName());
		int due = (int) ((System.currentTimeMillis() / 1000) + plugin.getConfig().getInt("rent-check-interval", 1200));
		plot.setPaymentDue(due);
		plugin.getPlotManager().refreshPlot(plot);

		// Scan for forsale signs and change them to sold
		RegionUtils.changeForSaleSignToSold(plot, city);

		// Finished.
		player.sendMessage(plugin.colorize("&dYou have rented the plot for " + plugin.getEconomy().format(price)));
		player.sendMessage(plugin.colorize("&dThis is a recurring payment and will automatically be deducted from your account whether you are online or not when rent is due."));
		player.sendMessage(plugin.colorize("&dIf you are unable to pay for rent due to non-sufficient funds you will have access removed from the plot and all belongings become property of the city owner."));
		return true;
	}
}
