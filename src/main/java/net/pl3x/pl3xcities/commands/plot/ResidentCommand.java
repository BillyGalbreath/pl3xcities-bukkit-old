package net.pl3x.pl3xcities.commands.plot;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import net.pl3x.pl3xcities.Pl3xCities;
import net.pl3x.pl3xcities.cities.City;
import net.pl3x.pl3xcities.cities.Plot;
import net.pl3x.pl3xcities.commands.BaseCommand;
import net.pl3x.pl3xcities.commands.PlayerCommand;
import net.pl3x.pl3xcities.events.plot.PlotResidentAddEvent;
import net.pl3x.pl3xcities.events.plot.PlotResidentRemoveEvent;
import net.pl3x.pl3xcities.util.RegionUtils;

import org.bukkit.command.Command;
import org.bukkit.entity.Player;

import com.sk89q.worldguard.domains.DefaultDomain;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

/**
 * Control the list of residents for the plot.
 * 
 */
public class ResidentCommand extends BaseCommand {
	public ResidentCommand(Pl3xCities plugin) {
		super(plugin, "resident", "Manage residents of a plot", null, null);
		registerSubcommand(new ListCommand(plugin));
		registerSubcommand(new AddCommand(plugin));
		registerSubcommand(new RemoveCommand(plugin));
	}

	public static class ListCommand extends PlayerCommand {
		public ListCommand(Pl3xCities plugin) {
			super(plugin, "list", "List the plot residents", "pl3xcities.command.plot.resident.list", new String[] { "&dView the residents of the plot:", "   &e/&7plot resident list" });
		}

		@Override
		public List<String> onTabComplete(Player player, Command command, String label, LinkedList<String> args) {
			return new ArrayList<String>();
		}

		@Override
		public boolean onCommand(Player player, Command command, String label, LinkedList<String> args) throws Exception {
			// Get city
			City city = RegionUtils.getCity(player.getLocation());

			// Get plot
			Plot plot = RegionUtils.getPlot(city, player.getLocation());

			// Finished.
			player.sendMessage(plugin.colorize("&dPlot Residents:"));
			player.sendMessage(plugin.colorize(RegionUtils.listMembers(plot.getRegion())));
			return true;
		}
	}

	public static class AddCommand extends PlayerCommand {
		public AddCommand(Pl3xCities plugin) {
			super(plugin, "add", "Add a plot resident", "pl3xcities.command.plot.resident.add", new String[] { "&dAdd a resident to the plot: &e/&7plot resident add <name>" });
		}

		@Override
		public List<String> onTabComplete(Player player, Command command, String label, LinkedList<String> args) {
			return new ArrayList<String>();
		}

		@Override
		public boolean onCommand(Player player, Command command, String label, LinkedList<String> args) throws Exception {
			// Check for resident name
			if (args.isEmpty())
				throw new Exception("&4You must specify a player's name to add!");

			// Get city
			City city = RegionUtils.getCity(player.getLocation());

			// Get the region
			Plot plot = RegionUtils.getPlot(city, player.getLocation());

			// Check ownership
			if (!player.getName().equalsIgnoreCase(city.getOwner()) && !player.getName().equalsIgnoreCase(plot.getOwner()))
				throw new Exception("&4You do not own this city.");

			// Get resident name
			String name = args.pop().trim();

			// Get the residents list
			ProtectedRegion region = plot.getRegion();
			DefaultDomain dd = region.getMembers();

			// Check if resident is already on list
			if (dd.contains(name))
				throw new Exception("That name is already on the residents list!");

			// Call the event and check for cancellation
			PlotResidentAddEvent event = new PlotResidentAddEvent(city, plot, player, name);
			plugin.getServer().getPluginManager().callEvent(event);
			if (event.isCancelled())
				return true;
			name = event.getResidentName();

			// Add name to the list
			dd.addPlayer(name);
			region.setMembers(dd);

			// Save to WorldGuard
			RegionUtils.saveRegion(region);

			// Finished.
			player.sendMessage(plugin.colorize("&dAdded plot resident."));
			return true;
		}
	}

	public static class RemoveCommand extends PlayerCommand {
		public RemoveCommand(Pl3xCities plugin) {
			super(plugin, "remove", "Remove a plot resident", "pl3xcities.command.plot.resident.remove", new String[] { "&dRemove a resident from the plot:", "   &e/&7plot resident remove <name>" });
		}

		@Override
		public List<String> onTabComplete(Player player, Command command, String label, LinkedList<String> args) {
			List<String> results = new ArrayList<String>();
			if (args.size() == 1) {
				try {
					City city = RegionUtils.getCity(player.getLocation());
					Plot plot = RegionUtils.getPlot(city, player.getLocation());
					for (String name : plot.getRegion().getMembers().getPlayers()) {
						if (name.startsWith(args.peek()))
							results.add(name);
					}
				} catch (Exception ignored) {
				}
			}
			return results;
		}

		@Override
		public boolean onCommand(Player player, Command command, String label, LinkedList<String> args) throws Exception {
			// Check for resident name
			if (args.isEmpty())
				throw new Exception("&4You must specify a player's name to remove!");

			// Get city
			City city = RegionUtils.getCity(player.getLocation());

			// Get plot
			Plot plot = RegionUtils.getPlot(city, player.getLocation());

			// Check ownership
			if (!player.getName().equalsIgnoreCase(city.getOwner()) && !player.getName().equalsIgnoreCase(plot.getOwner()))
				throw new Exception("&4You do not own this city.");

			// Get resident name
			String name = args.pop().trim();

			// Get the residents list
			ProtectedRegion region = plot.getRegion();
			DefaultDomain dd = region.getMembers();

			// Check if resident is already on list
			if (!dd.contains(name))
				throw new Exception("That name is not on the residents list!");

			// Call the event and check for cancellation
			PlotResidentRemoveEvent event = new PlotResidentRemoveEvent(city, plot, player, name);
			plugin.getServer().getPluginManager().callEvent(event);
			if (event.isCancelled())
				return true;
			name = event.getResidentName();

			// Remove from residents list
			dd.removePlayer(name);

			// Save to WorldGuard
			RegionUtils.saveRegion(region);

			// Finished.
			player.sendMessage(plugin.colorize("&dRemoved plot resident."));
			return true;
		}

	}
}
