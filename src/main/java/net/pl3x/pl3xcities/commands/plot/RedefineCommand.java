package net.pl3x.pl3xcities.commands.plot;

import com.sk89q.worldedit.bukkit.selections.Selection;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import net.pl3x.pl3xcities.Pl3xCities;
import net.pl3x.pl3xcities.cities.City;
import net.pl3x.pl3xcities.cities.Plot;
import net.pl3x.pl3xcities.commands.PlayerCommand;
import net.pl3x.pl3xcities.events.city.CityRedefineEvent;
import net.pl3x.pl3xcities.events.plot.PlotCreateEvent;
import net.pl3x.pl3xcities.events.plot.PlotRedefineEvent;import net.pl3x.pl3xcities.flag.TypeFlag;
import net.pl3x.pl3xcities.flag.WorldFlag;
import net.pl3x.pl3xcities.util.RegionUtils;
import net.pl3x.pl3xcities.util.Utils;
import org.bukkit.command.Command;
import org.bukkit.entity.Player;

import java.util.LinkedList;
import java.util.List;

public class RedefineCommand extends PlayerCommand {

	public RedefineCommand(Pl3xCities plugin) {
		super(plugin, "redefine", "Redefine the borders of a plot", "pl3xcities.command.plot.redefine", new String[] { "&dRedefine the plot's region to the current WorldEdit selection:", "   &e/&7plot redefine" });
	}
	@Override
	public List<String> onTabComplete(Player player, Command command, String label, LinkedList<String> args) {
		return null;
	}

	@Override
	public boolean onCommand(Player player, Command command, String label, LinkedList<String> args) throws Exception {
		// Get WorldEdit Selection
		Selection selection = RegionUtils.resizeSelection(plugin.getWorldEdit().getSelection(player));
		if (selection == null)
			throw new Exception("&4You have not selected a valid region.");

		// Check for overlapping regions
		ApplicableRegionSet rSet = RegionUtils.getApplicableRegionSet(selection);
		if (rSet.size() <= 0)
			throw new Exception("&4Your current selection is not in a city!");

		// Check for cities
		List<City> cities = RegionUtils.getCities(rSet);
		if (cities.isEmpty())
			throw new Exception("&4Your current selection is not in a city!");
		if (cities.size() > 1)
			throw new Exception("&4Your current selection is overlapping multiple cities!");

		// Get the city
		City city = cities.get(0);

		// Check ownership
		if (!city.getOwner().equalsIgnoreCase(player.getName()))
			throw new Exception("&4You cannot redefine a plot in a city you do not own!");

		// Get the plot
		Plot plot = RegionUtils.getPlot(city, player.getLocation());
		if (plot == null) {
			throw new Exception("&4You are not standing in a plot!");
		}

		// Check the size requirements of the selection
		int regionArea = selection.getArea() / selection.getHeight();
		int minPlot = plugin.getConfig().getInt("minimum-plot-area", 100);
		if (regionArea < minPlot)
			throw new Exception("&4Minimum area for a plot is " + minPlot + ". (" + regionArea + ")");
		int maxPlot = plugin.getConfig().getInt("maximum-plot-area", 10000);
		if (regionArea > maxPlot)
			throw new Exception("&4Maximum area for a plot is " + maxPlot + ". (" + regionArea + ")");

		// Check for existing plots
		if (RegionUtils.containsPlot(rSet, plot))
			throw new Exception("&4Your current selection overlaps an existing plot!");

		// Check if plot is all the way inside of the city.
		if (!RegionUtils.isPlotInsideCity(selection, city))
			throw new Exception("&4Your plot cannot spread outside of your city!");

		Selection plotSelection = RegionUtils.getSelection(plot.getRegion(), plot.getWorld());
		int oldArea = plotSelection.getArea() / plotSelection.getHeight();

		// Check funds
		double costPerBlock = plugin.getConfig().getDouble("cost-per-plot-block-buy", 1.0);
		double costTotal = ((regionArea-oldArea) * costPerBlock);
		double balance = city.getBankBalance();
		if (costTotal > balance)
			throw new Exception("&4Not enough bank funds to create this plot! (cost: " + plugin.getEconomy().format(costTotal) + ")");


		// Create new region from selection
		ProtectedRegion newRegion = RegionUtils.createRegion(selection, city.getName().toLowerCase() + "_plot" + plot.getId());

		// Call the event and check for cancellation

		PlotRedefineEvent event = new PlotRedefineEvent(city, plot, player, plot.getRegion(), newRegion);
		plugin.getServer().getPluginManager().callEvent(event);
		if (event.isCancelled())
			return true;
		newRegion = event.getNewRegion();

		// Charge the account
		city.addToBank(-costTotal);

		// Redefine the region
		newRegion = RegionUtils.redefineRegion(newRegion, plot.getRegion());
		plot.setRegion(newRegion);
		plot.setPrice(regionArea * costPerBlock);

		// Update player's selection now that the command has succeeded.
		if (plugin.getConfig().getBoolean("auto-update-player-selection", false))
			plugin.getWorldEdit().setSelection(player, selection);

		// Finished.
		player.sendMessage(plugin.colorize("&dYou have successfully redefined the plot!"));
		player.sendMessage(plugin.colorize("&d" + plugin.getEconomy().format(costTotal) + " has been deducted from your city's account."));
		return true;
	}
}
