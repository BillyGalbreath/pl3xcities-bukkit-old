package net.pl3x.pl3xcities.commands;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import net.pl3x.pl3xcities.Pl3xCities;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * A BaseCommand is a CommandHandler that excusively dispatches to sub-commands
 * 
 */
public class BaseCommand extends CommandHandler {
	private final TreeMap<String, CommandHandler> subCommands = new TreeMap<String, CommandHandler>();

	public BaseCommand(Pl3xCities plugin, String name, String description, String requiredPermission, String[] help) {
		super(plugin, name, description, requiredPermission, help);
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, org.bukkit.command.Command command, String label, LinkedList<String> args) {
		if (args.size() > 1) {
			CommandHandler subCmd = subCommands.get(args.peek().toLowerCase());
			if (subCmd != null) {
				if (subCmd.getRequiredPermission() == null || sender.hasPermission(subCmd.getRequiredPermission()))
					return subCmd.onTabComplete(sender, command, args.pop(), args);
			}
			return null;
		} else {
			List<String> results = new ArrayList<String>();
			for (Map.Entry<String, CommandHandler> cmdPair : subCommands.entrySet()) {
				if (cmdPair.getKey().startsWith(args.peek().toLowerCase())) {
					if (cmdPair.getValue().getRequiredPermission() == null || sender.hasPermission(cmdPair.getValue().getRequiredPermission()))
						results.add(cmdPair.getKey());
				}
			}
			return results;
		}
	}

	@Override
	public boolean onCommand(CommandSender sender, org.bukkit.command.Command command, String label, LinkedList<String> args) throws Exception {
		if (sender instanceof Player) {
			if (!plugin.getConfig().getStringList("enabled-worlds").contains(((Player) sender).getWorld().getName())) {
				sender.sendMessage(plugin.colorize("&4This command is not allowed in this world!"));
				return true;
			}
		}
		if (args.size() > 0) {
			CommandHandler subCmd = subCommands.get(args.peek().toLowerCase());
			if (subCmd != null)
				return subCmd.onCommand(sender, command, args.pop(), args);
			if (!args.getFirst().equals("?"))
				sender.sendMessage(plugin.colorize("&4Unknown Subcommand!"));
		}
		showHelp(sender);
		return true;
	}

	@Override
	public void showHelp(CommandSender sender) {
		sender.sendMessage(plugin.colorize("&dAvailable Subcommands:"));
		for (CommandHandler handler : subCommands.values()) {
			if (handler.getRequiredPermission() == null || sender.hasPermission(handler.getRequiredPermission()))
				sender.sendMessage(plugin.colorize("  &3" + handler.getName() + " &e- &7" + handler.getDescription()));
		}
	}

	public void registerSubcommand(CommandHandler commandHandler) {
		subCommands.put(commandHandler.getName().toLowerCase(), commandHandler);
	}
}
