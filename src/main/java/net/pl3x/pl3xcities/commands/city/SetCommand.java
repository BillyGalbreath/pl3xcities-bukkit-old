package net.pl3x.pl3xcities.commands.city;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import net.pl3x.pl3xcities.Pl3xCities;
import net.pl3x.pl3xcities.cities.City;
import net.pl3x.pl3xcities.cities.Plot;
import net.pl3x.pl3xcities.commands.BaseCommand;
import net.pl3x.pl3xcities.commands.PlayerCommand;
import net.pl3x.pl3xcities.events.city.CitySetDisabledFlagsEvent;
import net.pl3x.pl3xcities.events.city.CitySetFlagEvent;
import net.pl3x.pl3xcities.events.city.CitySetNameEvent;
import net.pl3x.pl3xcities.events.city.CitySetSpawnEvent;
import net.pl3x.pl3xcities.util.RegionUtils;
import net.pl3x.pl3xcities.util.Utils;

import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.entity.Player;

import com.sk89q.worldguard.protection.flags.DefaultFlag;
import com.sk89q.worldguard.protection.flags.Flag;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

/**
 * Set a setting for a city.
 * 
 */
public class SetCommand extends BaseCommand {
	public SetCommand(Pl3xCities plugin) {
		super(plugin, "set", "Set a city setting", null, null);
		registerSubcommand(new SetPriceCommand(plugin));
		registerSubcommand(new SetNameCommand(plugin));
		registerSubcommand(new SetFlagCommand(plugin));
		registerSubcommand(new SetSpawnCommand(plugin));
		registerSubcommand(new SetDisabledFlagsCommand(plugin));
	}

	public static class SetPriceCommand extends PlayerCommand {
		public SetPriceCommand(Pl3xCities plugin) {
			super(plugin, "price", "Set the city price", "pl3xcities.command.city.set.price", new String[] { "&dSets the price of the city:", "   &e/&7city set price <value>" });
		}

		@Override
		public List<String> onTabComplete(Player player, Command command, String label, LinkedList<String> args) {
			return new ArrayList<String>();
		}

		@Override
		public boolean onCommand(Player player, Command command, String label, LinkedList<String> args) throws Exception {
			// Check if server admin has disabled this feature.
			if (!plugin.getConfig().getBoolean("city-resale", false))
				throw new Exception("&4City selling has been disabled!");
			if (!plugin.getConfig().getBoolean("city-custom-pricing", false))
				throw new Exception("&4Custom city pricing has been disabled!");

			// Check for price
			if (args.isEmpty())
				throw new Exception("&4You must supply a price value!");

			// Get city
			City city = RegionUtils.getCity(player.getLocation());
			if (city == null)
				throw new Exception("&4You are not standing in a city!");

			// Check ownership
			if (!city.getOwner().equalsIgnoreCase(player.getName()))
				throw new Exception("&4You do not own this city!");

			// Check if city is up for sale
			if (city.isBuyable())
				throw new Exception("&4This city is marked for sale! Cannot change the price!");

			// Convert string to double
			double price;
			try {
				price = Double.parseDouble(args.peek().trim());
			} catch (IllegalArgumentException ex) {
				throw new Exception("&4'" + args.peek().trim() + "' is not a valid price!");
			}

			// No negative prices
			if (price < 0)
				throw new Exception("&4Price cannot be negative!");

			// Set the price
			city.setPrice(price);

			// Finished.
			player.sendMessage(plugin.colorize("&dCity price set to " + plugin.getEconomy().format(price)));
			return true;
		}
	}

	public static class SetNameCommand extends PlayerCommand {
		public SetNameCommand(Pl3xCities plugin) {
			super(plugin, "name", "Rename a city", "pl3xcities.command.city.set.name", new String[] { "&dRenames the city to a new name:", "   &e/&7city set name <newName>" });
		}

		@Override
		public List<String> onTabComplete(Player player, Command command, String label, LinkedList<String> args) {
			return new ArrayList<String>();
		}

		@Override
		public boolean onCommand(Player player, Command command, String label, LinkedList<String> args) throws Exception {
			// Check for new name
			if (args.isEmpty())
				throw new Exception("&4You must specify a new name!");

			// Get city
			City city = RegionUtils.getCity(player.getLocation());
			if (city == null)
				throw new Exception("&4You are not standing in a city.");

			// Check ownership
			if (!player.getName().equalsIgnoreCase(city.getOwner()))
				throw new Exception("&4You do not own this city.");

			// Check name
			String newName = args.peek();
			if (plugin.getCityManager().isCityCreated(newName))
				throw new Exception("&4City name already in use! Please choose another.");
			if (Utils.isInvalidName(newName))
				throw new Exception("&4City names must contain valid characters only!");

			// Call event and check for cancellation
			CitySetNameEvent event = new CitySetNameEvent(city, player, city.getName(), newName);
			plugin.getServer().getPluginManager().callEvent(event);
			if (event.isCancelled())
				return true;
			newName = event.getNewName();

			// Get the old values so we can reset them later
			List<Plot> plots = new ArrayList<Plot>(city.getPlots());

			// Get all the plot regions
			List<ProtectedRegion> childRegions = new ArrayList<ProtectedRegion>();
			for (Plot plot : plots) {
				childRegions.add(plot.getRegion());
			}

			// Rename city region (renames child regions too)
			RegionUtils.renameRegion(city.getRegion(), newName, childRegions);

			// Update plot regions
			World world = city.getWorld();
			for (Plot plot : plots) {
				plot.setRegion(RegionUtils.getRegion(newName + "_plot" + plot.getId(), world));
			}

			// Delete old city object
			plugin.getCityManager().removeCity(city);

			// Create new city object
			city = new City(newName, RegionUtils.getRegion(newName, world));

			plugin.getCityManager().addCity(city);

			// Restore old data to new city object
			for (Plot plot : plots) {
				city.addPlot(plot);
			}

			// Finished
			player.sendMessage(plugin.colorize("&dCity has been renamed."));
			return true;
		}
	}

	public static class SetFlagCommand extends PlayerCommand {
		public SetFlagCommand(Pl3xCities plugin) {
			super(plugin, "flag", "Set a city flag", "pl3xcities.command.city.set.flag", new String[] { "&dSets a WorldGuard flag for the city region:", "   &e/&7city set flag <flag> [value]" });
		}

		@Override
		public List<String> onTabComplete(Player player, Command command, String label, LinkedList<String> args) {
			List<String> results = new ArrayList<String>();
			if (args.size() == 1) {
				for (Flag<?> flag : DefaultFlag.getFlags()) {
					if (flag.getName().toLowerCase().startsWith(args.peek()))
						results.add(flag.getName());
				}
			}
			return results;
		}

		@Override
		public boolean onCommand(Player player, Command command, String label, LinkedList<String> args) throws Exception {
			// Check for flag
			if (args.isEmpty())
				throw new Exception("&4You must specify a flag to set!");

			// Get the name of the flag
			String flagName = args.pop();

			// Is this flag an illegal internal flag?
			if (Utils.getIllegalFlags().contains(flagName))
				throw new Exception("&4This flag is reserved for internal use only!");

			// Does server admin allow this flag to be set?
			if (!plugin.getConfig().getStringList("allowed-city-flags").contains(flagName))
				throw new Exception("&4This flag has been disabled!");

			// Get city
			City city = RegionUtils.getCity(player.getLocation());
			if (city == null)
				throw new Exception("&4You are not standing in a city.");

			// Check ownership
			if (!player.getName().equalsIgnoreCase(city.getOwner()))
				throw new Exception("&4You do not own this city.");

			// Get the region
			ProtectedRegion region = city.getRegion();
			if (region == null)
				throw new Exception("&4Something went wrong! Cannot find city region!");

			// Get the flag
			Flag<?> flag = DefaultFlag.fuzzyMatchFlag(flagName);
			if (flag == null)
				throw new Exception("&4That flag doesnt exist!");

			// Get the flag value to be set
			String value = null;
			if (!args.isEmpty())
				value = args.pop();

			// Call event and check for cancellation
			CitySetFlagEvent event = new CitySetFlagEvent(city, player, flag, region.getFlag(flag), value);
			plugin.getServer().getPluginManager().callEvent(event);
			if (event.isCancelled())
				return true;
			value = event.getNewValue();

			// Set the flag
			try {
				RegionUtils.setFlag(region, flag, value);
			} catch (Exception e) {
				throw new Exception("&4" + e.getMessage()); // WorldGuard has good error messages here
			}

			// Finished.
			String message = (value != null) ? "&dFlag was set." : "&dFlag was removed.";
			player.sendMessage(plugin.colorize(message));
			return true;
		}
	}

	public static class SetSpawnCommand extends PlayerCommand {
		public SetSpawnCommand(Pl3xCities plugin) {
			super(plugin, "spawn", "Set a city spawn point", "pl3xcities.command.city.set.spawn", new String[] { "&dSets the city's spawn where you are standing:", "   &e/&7city set spawn" });
		}

		@Override
		public List<String> onTabComplete(Player player, Command command, String label, LinkedList<String> args) {
			return new ArrayList<String>();
		}

		@Override
		public boolean onCommand(Player player, Command command, String label, LinkedList<String> args) throws Exception {
			// Get the city
			City city = RegionUtils.getCity(player.getLocation());
			if (city == null)
				throw new Exception("&4You are not standing in a city.");

			// Check ownership
			if (!player.getName().equalsIgnoreCase(city.getOwner()))
				throw new Exception("&4You do not own this city.");

			// Call event and check for cancellation
			CitySetSpawnEvent event = new CitySetSpawnEvent(city, player, Utils.getSpawn(city));
			plugin.getServer().getPluginManager().callEvent(event);
			if (event.isCancelled())
				return true;

			// Set the new location
			city.setSpawn(event.getNewLocation());

			// Finished
			player.sendMessage(plugin.colorize("&dNew spawn location has been set."));
			return true;
		}
	}

	public static class SetDisabledFlagsCommand extends PlayerCommand {
		public SetDisabledFlagsCommand(Pl3xCities plugin) {
			super(plugin, "disabled-flags", "Set which plot flags are disabled", "pl3xcities.command.city.set.disabledflags", new String[] { "&dSets which WorldGuard flags are disabled for the city's plots:", "   &e/&7city set disabled-flags [flags]" });
		}

		@Override
		public List<String> onTabComplete(Player player, Command command, String label, LinkedList<String> args) {
			return new ArrayList<String>();
		}

		@Override
		public boolean onCommand(Player player, Command command, String label, LinkedList<String> args) throws Exception {
			// Get the city
			City city = RegionUtils.getCity(player.getLocation());
			if (city == null)
				throw new Exception("&4You are not standing in a city.");

			// Check ownership
			if (!player.getName().equalsIgnoreCase(city.getOwner()))
				throw new Exception("&4You do not own this city.");

			// Get the list of disabled flags
			String flaglist = "";
			if (!args.isEmpty())
				flaglist = args.pop();

			// Check list for valid registered flags
			for (String flagName : flaglist.split(",")) {
				if (flagName.equals("")) {
					flaglist = null; // null will completely remove the flag value, while "" wont
					break;
				}
				if (DefaultFlag.fuzzyMatchFlag(flagName) == null)
					throw new Exception("&4That flag doesnt exist! (&7" + flagName + "&4)");
			}

			// Call event and check for cancellation
			CitySetDisabledFlagsEvent event = new CitySetDisabledFlagsEvent(city, player, flaglist);
			plugin.getServer().getPluginManager().callEvent(event);
			if (event.isCancelled())
				return true;
			flaglist = event.getDisabledFlags();

			// Set the disabled flags
			try {
				RegionUtils.setFlag(city.getRegion(), DefaultFlag.fuzzyMatchFlag("disabled"), flaglist);
			} catch (Exception e) {
				throw new Exception("&4" + e.getMessage()); // WorldGuard has good error messages here
			}

			// Finished
			player.sendMessage(plugin.colorize("&dDisabled flags list has been set."));
			return true;
		}
	}
}
