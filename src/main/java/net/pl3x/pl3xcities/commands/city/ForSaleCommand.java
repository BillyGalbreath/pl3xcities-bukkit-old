package net.pl3x.pl3xcities.commands.city;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import net.pl3x.pl3xcities.Pl3xCities;
import net.pl3x.pl3xcities.cities.City;
import net.pl3x.pl3xcities.commands.PlayerCommand;
import net.pl3x.pl3xcities.events.city.CityForSaleEvent;
import net.pl3x.pl3xcities.util.RegionUtils;

import org.bukkit.command.Command;
import org.bukkit.entity.Player;

public class ForSaleCommand extends PlayerCommand {
	public ForSaleCommand(Pl3xCities plugin) {
		super(plugin, "forsale", "Toggle city for-sale state", "pl3xcities.command.city.forsale", new String[] { "&dView or set the city's for-sale state:", "   &e/&7city forsale [value]" });
	}

	@Override
	public List<String> onTabComplete(Player player, Command command, String label, LinkedList<String> args) {
		List<String> results = new ArrayList<String>();
		if (args.size() == 1) {
			if ("true".startsWith(args.peek().toLowerCase()))
				results.add("true");
			if ("false".startsWith(args.peek().toLowerCase()))
				results.add("false");
			if ("yes".startsWith(args.peek().toLowerCase()))
				results.add("yes");
			if ("no".startsWith(args.peek().toLowerCase()))
				results.add("no");
		}
		return results;
	}

	@Override
	public boolean onCommand(Player player, Command command, String label, LinkedList<String> args) throws Exception {
		// Check if server admin has disabled this feature.
		if (!plugin.getConfig().getBoolean("city-resale", false))
			throw new Exception("&4City selling has been disabled!");

		// Get city
		City city = RegionUtils.getCity(player.getLocation());
		if (city == null)
			throw new Exception("&4You are not standing in a city!");

		// Get the current state
		boolean state = city.isBuyable();

		// Is the user actually setting the state?
		if (args.size() > 0) {
			// Check ownership
			if (!city.getOwner().equalsIgnoreCase(player.getName()))
				throw new Exception("&4You do not own this city!");

			// Get the new state value
			String value = args.peek();

			// Check the state value and convert to boolean
			boolean newState = false;
			if (value.equalsIgnoreCase("true") || value.equalsIgnoreCase("yes") || value.equalsIgnoreCase("1")) {
				newState = true;
			} else if (value.equalsIgnoreCase("false") || value.equalsIgnoreCase("no") || value.equalsIgnoreCase("0")) {
				newState = false;
			} else {
				throw new Exception("&4Not a yes/no value: " + value);
			}

			// Call the event and check for cancellation
			CityForSaleEvent event = new CityForSaleEvent(city, player, state, newState);
			plugin.getServer().getPluginManager().callEvent(event);
			if (event.isCancelled())
				return true;
			state = event.getNewState();

			// Set the state
			city.setBuyable(state);
		}

		// Finished.
		player.sendMessage(plugin.colorize("&dThis city is " + (state ? "&a" : "&cNOT ") + "FOR SALE"));
		return true;
	}
}
