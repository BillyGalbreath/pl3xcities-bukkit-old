package net.pl3x.pl3xcities.commands.city;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import net.pl3x.pl3xcities.Pl3xCities;
import net.pl3x.pl3xcities.cities.City;
import net.pl3x.pl3xcities.commands.PlayerCommand;
import net.pl3x.pl3xcities.events.city.CityRedefineEvent;
import net.pl3x.pl3xcities.util.RegionUtils;

import org.bukkit.command.Command;
import org.bukkit.entity.Player;

import com.sk89q.worldedit.bukkit.selections.Selection;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

/**
 * Redefine a city's area to the user's current WorldEdit selection.
 * 
 */
public class RedefineCommand extends PlayerCommand {
	public RedefineCommand(Pl3xCities plugin) {
		super(plugin, "redefine", "Redefine the borders of a city", "pl3xcities.command.city.redefine", new String[] { "&dRedefine the city's region to the current WorldEdit selection:", "   &e/&7city redefine" });
	}

	@Override
	public List<String> onTabComplete(Player player, Command command, String label, LinkedList<String> args) {
		return new ArrayList<String>();
	}

	@Override
	public boolean onCommand(Player player, Command command, String label, LinkedList<String> args) throws Exception {
		// Get WorldEdit Selection
		Selection selection = RegionUtils.resizeSelection(plugin.getWorldEdit().getSelection(player));
		if (selection == null)
			throw new Exception("&4You have not selected a valid region.");

		// Check selection size
		int maxHeight = selection.getWorld().getMaxHeight();
		int regionArea = selection.getArea() / maxHeight;
		if (regionArea == 0)
			throw new Exception("&4You have not selected a region.");
		int maxCity = plugin.getConfig().getInt("maximum-city-area", 1000000);
		if (regionArea > maxCity)
			throw new Exception("&4Maximum area for a city is " + maxCity + ". (" + regionArea + ")");

		// Check for overlapping regions
		ApplicableRegionSet rSet = RegionUtils.getApplicableRegionSet(selection);
		if (rSet.size() == 0)
			throw new Exception("&4Your current selection is not attached to a city.");

		// Check for overlapping cities
		List<City> overlapCities = RegionUtils.getCities(rSet);
		if (overlapCities.isEmpty())
			throw new Exception("&4Your current selection is not attached to a city.");
		if (overlapCities.size() > 1)
			throw new Exception("&4Your current selection overlaps multiple cities.");

		// Get city
		City city = overlapCities.get(0);

		// Check ownership
		if (!city.getOwner().equalsIgnoreCase(player.getName()))
			throw new Exception("&4You do not own this city.");

		// Check for any overlapping regions
		for (ProtectedRegion region : rSet) {
			if (!city.getName().equalsIgnoreCase(region.getId()))
				if (!city.getName().equalsIgnoreCase(region.getId().split("_plot")[0]))
					throw new Exception("&4Your current selection overlaps an existing region that does not belong to you!");
		}

		// Get the existing region area
		ProtectedRegion oldRegion = city.getRegion();
		if (oldRegion == null)
			throw new Exception("&4Error: Cannot find old region!");
		int oldRegionArea = oldRegion.volume() / maxHeight;

		// Check funds
		double costPerBlock = plugin.getConfig().getDouble("cost-per-city-block", 10.0);
		double costTotal = regionArea * costPerBlock;
		double costRedefine = costTotal - (oldRegionArea * costPerBlock);
		double balance = city.getBankBalance();
		if (costRedefine > balance)
			throw new Exception("&4Not enough bank funds to redefine this city! (cost: " + plugin.getEconomy().format(costRedefine) + ")");

		// Create new region from selection
		ProtectedRegion newRegion = RegionUtils.createRegion(selection, city.getName());

		// Call the event and check for cancellation
		CityRedefineEvent event = new CityRedefineEvent(city, player, oldRegion, newRegion);
		plugin.getServer().getPluginManager().callEvent(event);
		if (event.isCancelled())
			return true;
		newRegion = event.getNewRegion();

		// Charge the account
		boolean charged = false;
		if (costRedefine > 0) {
			charged = true;
			city.addToBank(-costRedefine);
		}

		// Save the new region to WorldGuard and set price flag
		newRegion = RegionUtils.redefineRegion(newRegion, oldRegion);
		city.setRegion(newRegion);
		city.setPrice(costTotal);

		// Update player's selection now that the command has succeeded.
		if (plugin.getConfig().getBoolean("auto-update-player-selection", false))
			plugin.getWorldEdit().setSelection(player, selection);

		// Finished.
		if (charged)
			player.sendMessage(plugin.colorize("&dYou have successfully redefined your city for a total cost of " + plugin.getEconomy().format(costRedefine)));
		else
			player.sendMessage(plugin.colorize("&dYou have successfully redefined your city."));
		return true;
	}
}
