package net.pl3x.pl3xcities.commands.city;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import net.pl3x.pl3xcities.Pl3xCities;
import net.pl3x.pl3xcities.cities.City;
import net.pl3x.pl3xcities.commands.PlayerCommand;
import net.pl3x.pl3xcities.util.RegionUtils;
import net.pl3x.pl3xcities.util.Utils;

import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.entity.Player;

import com.sk89q.worldedit.bukkit.selections.Selection;

/**
 * Get information about a city and display it to the CommandSender.
 * 
 */
public class InfoCommand extends PlayerCommand {
	public InfoCommand(Pl3xCities plugin) {
		super(plugin, "info", "Display info about a city", "pl3xcities.command.city.info", new String[] { "&dGet information about the city:", "   &e/&7city info [name]" });

	}

	@Override
	public List<String> onTabComplete(Player player, Command command, String label, LinkedList<String> args) {
		List<String> results = new ArrayList<String>();
		if (args.size() == 1) {
			for (City c : plugin.getCityManager().getAllCities()) {
				if (c.getName().toLowerCase().startsWith(args.peek()))
					results.add(c.getName());
			}
		}
		return results;
	}

	@Override
	public boolean onCommand(Player player, Command command, String label, LinkedList<String> args) throws Exception {
		// Get city
		City city = null;
		String cityName = args.peek();
		if (cityName != null) {
			if (!plugin.getCityManager().isCityCreated(cityName))
				throw new Exception("&4Failed to find a city by that name!");
			city = plugin.getCityManager().getCity(cityName);
		} else
			city = RegionUtils.getCity(player.getLocation());

		// Do some calculations
		Selection selection = RegionUtils.getSelection(city.getRegion(), city.getWorld());
		int area = selection.getArea() / selection.getHeight();
		double tax = (plugin.getConfig().getDouble("cost-per-city-block", 10.0) * (plugin.getConfig().getDouble("city-tax-percent", 1.0) / 100)) * area;
		long secondsDue = city.getPaymentDue() - (System.currentTimeMillis() / 1000);

		// Show the info
		Location spawn = Utils.getSpawn(city);
		player.sendMessage(plugin.colorize("&dCity info for: " + city.getName()));
		player.sendMessage(plugin.colorize("   &dName: &7" + city.getName()));
		player.sendMessage(plugin.colorize("   &dOwner: &7" + city.getOwner()));
		player.sendMessage(plugin.colorize("   &dOfficers: &7" + RegionUtils.listMembers(city.getRegion())));
		player.sendMessage(plugin.colorize("   &dPopulation: &7" + RegionUtils.getCityPopulation(city)));
		player.sendMessage(plugin.colorize("   &dSpawn: &7" + spawn.getBlockX() + "&d, &7" + spawn.getBlockY() + "&d, &7" + spawn.getBlockZ()));
		player.sendMessage(plugin.colorize("   &dForSale: &7" + city.isBuyable()));
		player.sendMessage(plugin.colorize("   &dPrice: &7" + plugin.getEconomy().format(city.getPrice())));
		player.sendMessage(plugin.colorize("   &dArea: &7" + area + " m\u00B2"));
		player.sendMessage(plugin.colorize("   &dPlots: &7" + city.getPlots().size()));
		player.sendMessage(plugin.colorize("   &dTaxes: &7 " + plugin.getEconomy().format(tax) + " due in " + Utils.formatSeconds(secondsDue)));
		player.sendMessage(plugin.colorize("   &dFlags: &7 " + RegionUtils.getFlagsString(city.getRegion())));
		return true;
	}
}
