package net.pl3x.pl3xcities.commands.city;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import net.pl3x.pl3xcities.Pl3xCities;
import net.pl3x.pl3xcities.cities.City;
import net.pl3x.pl3xcities.commands.BaseCommand;
import net.pl3x.pl3xcities.commands.PlayerCommand;
import net.pl3x.pl3xcities.events.city.CityOfficerAddEvent;
import net.pl3x.pl3xcities.events.city.CityOfficerRemoveEvent;
import net.pl3x.pl3xcities.util.RegionUtils;

import org.bukkit.command.Command;
import org.bukkit.entity.Player;

import com.sk89q.worldguard.domains.DefaultDomain;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

/**
 * Control the list of officers for the city.
 * 
 */
public class OfficerCommand extends BaseCommand {
	public OfficerCommand(Pl3xCities plugin) {
		super(plugin, "officer", "Manage the city officers list", null, null);
		registerSubcommand(new ListCommand(plugin));
		registerSubcommand(new AddCommand(plugin));
		registerSubcommand(new RemoveCommand(plugin));
	}

	public static class ListCommand extends PlayerCommand {
		public ListCommand(Pl3xCities plugin) {
			super(plugin, "list", "List city officers", "pl3xcities.command.city.officer.list", new String[] { "&dView the officers of the city: ", "   &e/&7city officer list" });
		}

		@Override
		public List<String> onTabComplete(Player player, Command command, String label, LinkedList<String> args) {
			return new ArrayList<String>();
		}

		@Override
		public boolean onCommand(Player player, Command command, String label, LinkedList<String> args) throws Exception {
			// Get city
			City city = RegionUtils.getCity(player.getLocation());

			// Finished.
			player.sendMessage(plugin.colorize("&dCity Officers:"));
			player.sendMessage(plugin.colorize(RegionUtils.listMembers(city.getRegion())));
			return true;
		}
	}

	public static class AddCommand extends PlayerCommand {
		public AddCommand(Pl3xCities plugin) {
			super(plugin, "add", "Add a city officer", "pl3xcities.command.city.officer.add", new String[] { "&dAdd an officer to the city:", "   &e/&7city officer add <name>" });
		}

		@Override
		public List<String> onTabComplete(Player player, Command command, String label, LinkedList<String> args) {
			return new ArrayList<String>();
		}

		@Override
		public boolean onCommand(Player player, Command command, String label, LinkedList<String> args) throws Exception {
			// Check for officer name
			if (args.isEmpty())
				throw new Exception("&4You must specify a player's name to add!");

			// Get city
			City city = RegionUtils.getCity(player.getLocation());

			// Check ownership
			if (!player.getName().equalsIgnoreCase(city.getOwner()))
				throw new Exception("&4You do not own this city.");

			// Get officer name
			String name = args.pop().trim();

			// Get the officers list
			ProtectedRegion region = city.getRegion();
			DefaultDomain dd = region.getMembers();

			// Check if officer is already on list
			if (dd.contains(name))
				throw new Exception("That name is already on the officers list!");

			// Call the event and check for cancellation
			CityOfficerAddEvent event = new CityOfficerAddEvent(city, player, name);
			plugin.getServer().getPluginManager().callEvent(event);
			if (event.isCancelled())
				return true;
			name = event.getOfficerName();

			// Add name to the list
			dd.addPlayer(name);
			region.setMembers(dd);

			// Save to WorldGuard
			RegionUtils.saveRegion(region);

			// Finished.
			player.sendMessage(plugin.colorize("&dAdded city officer."));
			return true;
		}
	}

	public static class RemoveCommand extends PlayerCommand {
		public RemoveCommand(Pl3xCities plugin) {
			super(plugin, "remove", "Remove a city officer", "pl3xcities.command.city.officer.remove", new String[] { "&dRemove an officer from the city:", "   &e/&7city officer remove <name>" });
		}

		@Override
		public List<String> onTabComplete(Player player, Command command, String label, LinkedList<String> args) {
			List<String> results = new ArrayList<String>();
			if (args.size() == 1) {
				try {
					City city = RegionUtils.getCity(player.getLocation());
					for (String name : city.getRegion().getMembers().getPlayers()) {
						if (name.startsWith(args.peek()))
							results.add(name);
					}
				} catch (Exception ignored) {
				}
			}
			return results;
		}

		@Override
		public boolean onCommand(Player player, Command command, String label, LinkedList<String> args) throws Exception {
			// Check for officer name
			if (args.isEmpty())
				throw new Exception("&4You must specify a player's name to remove!");

			// Get city
			City city = RegionUtils.getCity(player.getLocation());

			// Check ownership
			if (!player.getName().equalsIgnoreCase(city.getOwner()))
				throw new Exception("&4You do not own this city.");

			// Get officer name
			String name = args.pop();

			// Get the officers list
			ProtectedRegion region = city.getRegion();
			DefaultDomain dd = region.getMembers();

			// Check if officer is already on list
			if (!dd.contains(name))
				throw new Exception("That name is not on the officers list!");

			// Call the event and check for cancellation
			CityOfficerRemoveEvent event = new CityOfficerRemoveEvent(city, player, name);
			plugin.getServer().getPluginManager().callEvent(event);
			if (event.isCancelled())
				return true;
			name = event.getOfficerName();

			// Remove from officers list
			dd.removePlayer(name);

			// Save to WorldGuard
			RegionUtils.saveRegion(region);

			// Finished.
			player.sendMessage(plugin.colorize("&dRemoved city officer."));
			return true;
		}
	}
}
