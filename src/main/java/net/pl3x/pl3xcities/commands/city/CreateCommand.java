package net.pl3x.pl3xcities.commands.city;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import net.milkbowl.vault.economy.EconomyResponse;
import net.pl3x.pl3xcities.Pl3xCities;
import net.pl3x.pl3xcities.cities.City;
import net.pl3x.pl3xcities.commands.PlayerCommand;
import net.pl3x.pl3xcities.events.city.CityCreateEvent;
import net.pl3x.pl3xcities.flag.BankFlag;
import net.pl3x.pl3xcities.flag.TypeFlag;
import net.pl3x.pl3xcities.flag.WorldFlag;
import net.pl3x.pl3xcities.util.RegionUtils;
import net.pl3x.pl3xcities.util.Utils;

import org.bukkit.command.Command;
import org.bukkit.entity.Player;

import com.sk89q.worldedit.bukkit.selections.Selection;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

/**
 * Create a new city from the user's current WorldEdit selection with a spawn point where the user is standing.
 * 
 */
public class CreateCommand extends PlayerCommand {
	public CreateCommand(Pl3xCities plugin) {
		super(plugin, "create", "Create a new city", "pl3xcities.command.city.create", new String[] { "&dCreate a new city from your current WorldEdit selection:", "   &e/&7city create <name>" });
	}

	@Override
	public List<String> onTabComplete(Player player, Command command, String label, LinkedList<String> args) {
		return new ArrayList<String>();
	}

	@Override
	public boolean onCommand(Player player, Command command, String label, LinkedList<String> args) throws Exception {
		// Check for city name
		if (args.isEmpty())
			throw new Exception("&4You must specify a city name!");

		// Check how many cities the player already has
		int count = 0;
		for (City city : plugin.getCityManager().getAllCities()) {
			if (city.getOwner().equalsIgnoreCase(player.getName()))
				count++;
		}
		int limit = Utils.getLimit(player, "pl3xcities.city.limit");
		if (count >= limit && limit >= 0)
			throw new Exception("&4You have reached your city limit!");

		// Check city name
		String name = args.pop();
		if (plugin.getCityManager().isCityCreated(name))
			throw new Exception("&4City name already in use! Please choose another.");
		if (Utils.isInvalidName(name))
			throw new Exception("&4City names must contain valid characters only!");

		// Get WorldEdit Selection
		Selection selection = RegionUtils.resizeSelection(plugin.getWorldEdit().getSelection(player));
		if (selection == null)
			throw new Exception("&4You have not selected a valid region.");

		// Check the size requirements of the selection
		int maxHeight = selection.getWorld().getMaxHeight();
		int regionArea = selection.getArea() / maxHeight;
		int minCity = plugin.getConfig().getInt("minimum-city-area", 10000);
		if (regionArea < minCity)
			throw new Exception("&4Minimum area for a city is " + minCity + ". (" + regionArea + ")");
		int maxCity = plugin.getConfig().getInt("maximum-city-area", 1000000);
		if (regionArea > maxCity)
			throw new Exception("&4Maximum area for a city is " + maxCity + ". (" + regionArea + ")");

		// Check to see if player is standing inside of city. (for setting spawn)
		if (!selection.contains(player.getLocation()))
			throw new Exception("&4You must be standing in the city while creating it!");

		// Check for overlapping regions
		ApplicableRegionSet rSet = RegionUtils.getApplicableRegionSet(selection);
		if (rSet.size() > 0) {
			// Check if regions contain a city
			if (RegionUtils.containsCity(rSet))
				throw new Exception("&4Your current selection overlaps an existing city!");
			throw new Exception("&4Your current selection overlaps an existing region!");
		}

		// Check funds
		double costPerBlock = plugin.getConfig().getDouble("cost-per-city-block", 10.0);
		double costTotal = (regionArea * costPerBlock);
		double balance = plugin.getEconomy().getBalance(player.getName());
		if (costTotal > balance)
			throw new Exception("&4You cannot afford to create this city! (cost: " + plugin.getEconomy().format(costTotal) + ")");

		// Create a new region for new city
		ProtectedRegion region = RegionUtils.createRegion(selection, name.toLowerCase());
		region.setFlag(WorldFlag.flag, player.getWorld().getName());
		region.setFlag(TypeFlag.flag, TypeFlag.RegionType.CITY.toString());
		region.setFlag(BankFlag.flag, 0.0);

		// Create city
		City city = new City(name, region);

		// Call the event and check for cancellation
		CityCreateEvent event = new CityCreateEvent(city, player);
		plugin.getServer().getPluginManager().callEvent(event);
		if (event.isCancelled())
			return true;

		// Charge the account
		EconomyResponse response = plugin.getEconomy().withdrawPlayer(player.getName(), costTotal);
		if (!response.transactionSuccess())
			throw new Exception("&4An error has occurred trying to charge your account! " + response.errorMessage);

		// Save the region to worldguard and set price flag.
		try {
			RegionUtils.createNewRegion(region, player, null);
		} catch (Exception e) {
			throw e;
		}

		// Update player's selection now that the command has succeeded.
		if (plugin.getConfig().getBoolean("auto-update-player-selection", false))
			plugin.getWorldEdit().setSelection(player, selection);

		// Populate the city's data
		city.setOwner(player.getName());
		city.setSpawn(player.getLocation());
		city.setPrice(costTotal);
		int now = (int) (System.currentTimeMillis() / 1000);
		city.setPaymentDue(now + plugin.getConfig().getInt("tax-check-interval", 8400));

		// Add city to manager
		plugin.getCityManager().addCity(city);

		// Finished.
		player.sendMessage(plugin.colorize("&dYou have successfully claimed your city!"));
		player.sendMessage(plugin.colorize("&d" + plugin.getEconomy().format(costTotal) + " has been deducted from your account."));
		return true;
	}
}
