package net.pl3x.pl3xcities.commands.city;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import net.pl3x.pl3xcities.Pl3xCities;
import net.pl3x.pl3xcities.RegionVisualization;
import net.pl3x.pl3xcities.cities.City;
import net.pl3x.pl3xcities.cities.Plot;
import net.pl3x.pl3xcities.commands.PlayerCommand;
import net.pl3x.pl3xcities.util.RegionUtils;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.entity.Player;

/**
 * Show an outline of the city/plots to the user
 * 
 */
public class VisualizeCommmand extends PlayerCommand {
	public VisualizeCommmand(Pl3xCities plugin) {
		super(plugin, "visualize", "Visualize the city and plot borders", "pl3xcities.command.city.visualize", new String[] { "&dVisually see the city's border (optionally see plots too):", "   &e/&7city visualize [-plots]" });
	}

	@Override
	public List<String> onTabComplete(Player player, Command command, String label, LinkedList<String> args) {
		List<String> results = new ArrayList<String>();
		if (args.size() == 1) {
			if ("-plots".startsWith(args.peek().toLowerCase()))
				results.add("-plots");
		}
		return results;
	}

	@Override
	public boolean onCommand(Player player, Command command, String label, LinkedList<String> args) throws Exception {
		// Show plots too?
		boolean showPlots = args.remove("-plots");

		// Get the city
		City city;
		if (args.isEmpty())
			city = RegionUtils.getCity(player.getLocation());
		else {
			String name = args.pop().trim();
			if (!plugin.getCityManager().isCityCreated(name))
				throw new Exception("&4Failed to find city by that name!");
			city = plugin.getCityManager().getCity(name);
		}
		if (city == null)
			throw new Exception("&4Failed to find city!");

		// Show city outline
		new RegionVisualization(plugin, player, city.getRegion(), 100, Material.GOLD_BLOCK, (byte) 0).start();

		if (!showPlots)
			return true;

		// Show plots' outline
		List<Plot> plots = city.getPlots();
		for (int i = 0; i < plots.size(); i++) {
			RegionVisualization visualization = new RegionVisualization(plugin, player, plots.get(i).getRegion(), 100, Material.IRON_BLOCK, (byte) 0);
			Bukkit.getScheduler().runTaskLater(plugin, new RegionVisualization.Start(visualization), i);
		}

		// Finished.
		return true;
	}
}
