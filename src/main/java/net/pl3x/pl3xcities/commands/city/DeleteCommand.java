package net.pl3x.pl3xcities.commands.city;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import net.pl3x.pl3xcities.Pl3xCities;
import net.pl3x.pl3xcities.cities.City;
import net.pl3x.pl3xcities.commands.PlayerCommand;
import net.pl3x.pl3xcities.events.city.CityDeleteEvent;
import net.pl3x.pl3xcities.util.RegionUtils;

import org.bukkit.command.Command;
import org.bukkit.entity.Player;

/**
 * Delete a city.
 * 
 */
public class DeleteCommand extends PlayerCommand {
	public DeleteCommand(Pl3xCities plugin) {
		super(plugin, "delete", "Delete a city", "pl3xcities.command.city.delete", new String[] { "&dDelete city from existance:", "   &e/&7city delete <name>", });
	}

	@Override
	public List<String> onTabComplete(Player player, Command command, String label, LinkedList<String> args) {
		List<String> results = new ArrayList<String>();
		if (args.size() == 1) {
			for (City c : plugin.getCityManager().getAllCities()) {
				if (c.getName().toLowerCase().startsWith(args.peek()))
					results.add(c.getName());
			}
		}
		return results;
	}

	@Override
	public boolean onCommand(Player player, Command command, String label, LinkedList<String> args) throws Exception {
		// Check for city name
		if (args.isEmpty())
			throw new Exception("&4You must specify the city name!");

		// Get city
		String name = args.pop();
		if (!plugin.getCityManager().isCityCreated(name))
			throw new Exception("&4Failed to find city by that name!");
		City city = plugin.getCityManager().getCity(name);
		if (city != RegionUtils.getCity(player.getLocation()))
			throw new Exception("&4You must be standing in the city to delete it!");

		// Check ownership of city
		if (!player.getName().equalsIgnoreCase(city.getOwner()))
			throw new Exception("&4You do not own this city.");

		// Call the event and check for cancellation
		CityDeleteEvent event = new CityDeleteEvent(city, player);
		plugin.getServer().getPluginManager().callEvent(event);
		if (event.isCancelled())
			return true;

		// Delete city region (WorldGuard will delete all child regions automagically)
		RegionUtils.deleteRegion(city.getRegion());

		// Delete city from memory.
		plugin.getCityManager().removeCity(city);

		// Finished.
		player.sendMessage(plugin.colorize("&dYou have successfully deleted the city."));
		return true;
	}
}
