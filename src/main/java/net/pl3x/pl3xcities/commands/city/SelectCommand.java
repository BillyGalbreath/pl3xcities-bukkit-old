package net.pl3x.pl3xcities.commands.city;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import net.pl3x.pl3xcities.Pl3xCities;
import net.pl3x.pl3xcities.cities.City;
import net.pl3x.pl3xcities.commands.PlayerCommand;
import net.pl3x.pl3xcities.events.city.CitySelectEvent;
import net.pl3x.pl3xcities.util.RegionUtils;

import org.bukkit.command.Command;
import org.bukkit.entity.Player;

/**
 * create a worldedit selection matching the given city and assign it to the player.
 * 
 */
public class SelectCommand extends PlayerCommand {
	public SelectCommand(Pl3xCities plugin) {
		super(plugin, "select", "Select a city with the WorldEdit wand", "pl3xcities.command.city.select", new String[] { "&dSelect the city's region with the WorldEdit wand:", "   &e/&7city select" });
	}

	@Override
	public List<String> onTabComplete(Player player, Command command, String label, LinkedList<String> args) {
		return new ArrayList<String>();
	}

	@Override
	public boolean onCommand(Player player, Command command, String label, LinkedList<String> args) throws Exception {
		// Get city
		City city;
		if (args.isEmpty())
			city = RegionUtils.getCity(player.getLocation());
		else {
			String name = args.pop().trim();
			if (!plugin.getCityManager().isCityCreated(name))
				throw new Exception("&4Failed to find city by that name!");
			city = plugin.getCityManager().getCity(name);
		}
		if (city == null)
			throw new Exception("&4Failed to find city!");

		// Call the event and check for cancellation
		CitySelectEvent event = new CitySelectEvent(city, player);
		plugin.getServer().getPluginManager().callEvent(event);
		if (event.isCancelled())
			return true;

		// Update the player selection
		plugin.getWorldEdit().setSelection(player, RegionUtils.getSelection(city.getRegion(), city.getWorld()));

		// Finished.
		player.sendMessage(plugin.colorize("&dYou now have the entire city selected."));
		return true;
	}
}
