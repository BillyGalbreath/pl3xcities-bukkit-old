package net.pl3x.pl3xcities.commands.city;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import net.pl3x.pl3xcities.Pl3xCities;
import net.pl3x.pl3xcities.cities.City;
import net.pl3x.pl3xcities.commands.PlayerCommand;
import net.pl3x.pl3xcities.events.city.CitySpawnEvent;
import net.pl3x.pl3xcities.util.RegionUtils;
import net.pl3x.pl3xcities.util.Utils;

import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.entity.Player;

/**
 * Teleport the user to the city's spawn.
 * 
 */
public class SpawnCommand extends PlayerCommand {
	public SpawnCommand(Pl3xCities plugin) {
		super(plugin, "spawn", "Teleport to the city spawn", "pl3xcities.command.city.spawn", new String[] { "&dTeleport to the city spawn point:", "   &e/&7city spawn [city]" });
	}

	@Override
	public List<String> onTabComplete(Player player, Command command, String label, LinkedList<String> args) {
		List<String> results = new ArrayList<String>();
		if (args.size() == 1) {
			for (City c : plugin.getCityManager().getAllCities()) {
				if (c.getName().toLowerCase().startsWith(args.peek()))
					results.add(c.getName());
			}
		}
		return results;
	}

	@Override
	public boolean onCommand(Player player, Command command, String label, LinkedList<String> args) throws Exception {
		// Get city
		City city;
		if (args.isEmpty())
			city = RegionUtils.getCity(player.getLocation());
		else {
			String name = args.pop().trim();
			if (!plugin.getCityManager().isCityCreated(name))
				throw new Exception("&4Failed to find city by that name!");
			city = plugin.getCityManager().getCity(name);
		}
		if (city == null)
			throw new Exception("&4Failed to find city!");

		// Call event and check for cancellation
		CitySpawnEvent event = new CitySpawnEvent(city, player, Utils.getSpawn(city));
		plugin.getServer().getPluginManager().callEvent(event);
		if (event.isCancelled())
			return true;

		// Teleport the player
		player.sendMessage(plugin.colorize("&dTeleporting you to &7" + city.getName() + "&d."));
		if (plugin.getConfig().getBoolean("play-sounds", true)) {
			player.getWorld().playSound(player.getLocation(), Sound.ENDERMAN_TELEPORT, 1f, 1f);
			city.getWorld().playSound(event.getSpawnLocation(), Sound.ENDERMAN_TELEPORT, 1f, 1f);
		}
		player.teleport(event.getSpawnLocation());

		// Finished.
		return true;
	}
}
