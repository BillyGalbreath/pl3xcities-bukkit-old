package net.pl3x.pl3xcities.commands.city;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import net.milkbowl.vault.economy.EconomyResponse;
import net.pl3x.pl3xcities.Pl3xCities;
import net.pl3x.pl3xcities.cities.City;
import net.pl3x.pl3xcities.commands.BaseCommand;
import net.pl3x.pl3xcities.commands.PlayerCommand;
import net.pl3x.pl3xcities.util.RegionUtils;

import org.bukkit.command.Command;
import org.bukkit.entity.Player;

public class BankCommand extends BaseCommand {
	public BankCommand(Pl3xCities plugin) {
		super(plugin, "bank", "Manage the city bank account", null, null);
		registerSubcommand(new BalanceCommand(plugin));
		registerSubcommand(new DepositCommand(plugin));
		registerSubcommand(new WithdrawCommand(plugin));
	}

	public static class BalanceCommand extends PlayerCommand {
		public BalanceCommand(Pl3xCities plugin) {
			super(plugin, "balance", "View the balance of the city bank account", "pl3xcities.command.city.bank.balance", new String[] { "&dView the balance of the city bank account: ", "   &e/&7city bank balance" });
		}

		@Override
		public List<String> onTabComplete(Player player, Command command, String label, LinkedList<String> args) {
			return new ArrayList<String>();
		}

		@Override
		public boolean onCommand(Player player, Command command, String label, LinkedList<String> args) throws Exception {
			// Get city
			City city = RegionUtils.getCity(player.getLocation());

			// Check user rights
			// TODO allow more than just the owner (officers maybe? city owner decides)
			if (!city.getOwner().equalsIgnoreCase(player.getName()))
				throw new Exception("&4Only the city owner can check the bank balance!");

			// Finished.
			player.sendMessage(plugin.colorize("&dBalance: " + plugin.getEconomy().format(city.getBankBalance())));
			return true;
		}
	}

	public static class DepositCommand extends PlayerCommand {
		public DepositCommand(Pl3xCities plugin) {
			super(plugin, "deposit", "Deposit money into the city bank account", "pl3xcities.command.city.bank.deposit", new String[] { "&dDeposit money into the city bank account: ", "   &e/&7city bank deposit <amount>" });
		}

		@Override
		public List<String> onTabComplete(Player player, Command command, String label, LinkedList<String> args) {
			return new ArrayList<String>();
		}

		@Override
		public boolean onCommand(Player player, Command command, String label, LinkedList<String> args) throws Exception {
			// Get city
			City city = RegionUtils.getCity(player.getLocation());

			// Get the amount
			double amount = 0;
			try {
				amount = Double.valueOf(args.pop());
			} catch (Exception e) {
				throw new Exception("&4That is not a valid amount!");
			}
			if (amount <= 0)
				throw new Exception("Must specify a positive amount to deposit!");

			// Deposit the money
			city.addToBank(amount);

			// Remove from player's account
			EconomyResponse response = plugin.getEconomy().withdrawPlayer(player.getName(), amount);
			if (!response.transactionSuccess()) {
				// Take money back out of city bank so its not exploited
				city.addToBank(-amount);
				throw new Exception("&4An error has occurred trying to access your account! " + response.errorMessage);
			}

			// Finished.
			player.sendMessage(plugin.colorize("&dYou have deposited &7" + plugin.getEconomy().format(amount) + " &dinto the city bank."));
			return true;
		}
	}

	public static class WithdrawCommand extends PlayerCommand {
		public WithdrawCommand(Pl3xCities plugin) {
			super(plugin, "withdraw", "Withdraw money from the city bank account", "pl3xcities.command.city.bank.withdraw", new String[] { "&dWithdraw money from the city bank account: ", "   &e/&7city bank withdraw <amount>" });
		}

		@Override
		public List<String> onTabComplete(Player player, Command command, String label, LinkedList<String> args) {
			return new ArrayList<String>();
		}

		@Override
		public boolean onCommand(Player player, Command command, String label, LinkedList<String> args) throws Exception {
			// Get city
			City city = RegionUtils.getCity(player.getLocation());

			// Check for city owner - ONLY the owner can withdraw. Period.
			if (!city.getOwner().equalsIgnoreCase(player.getName()))
				throw new Exception("&4Only the city owner may withdraw from the bank account!");

			// Get the amount
			double amount = 0;
			try {
				amount = Double.valueOf(args.pop());
			} catch (Exception e) {
				throw new Exception("&4That is not a valid amount!");
			}
			if (amount <= 0)
				throw new Exception("Must specify a positive amount to withdraw!");

			// Withdraw the money
			city.addToBank(-amount);

			// Add to player's account
			EconomyResponse response = plugin.getEconomy().depositPlayer(player.getName(), amount);
			if (!response.transactionSuccess()) {
				// Put money back into city bank so its not lost
				city.addToBank(amount);
				throw new Exception("&4An error has occurred trying to access your account! " + response.errorMessage);
			}

			// Finished.
			player.sendMessage(plugin.colorize("&dYou have withdrawn &7" + plugin.getEconomy().format(amount) + " &dfrom the city bank."));
			return true;
		}
	}
}
