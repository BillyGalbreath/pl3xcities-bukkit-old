package net.pl3x.pl3xcities.commands.city;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import net.milkbowl.vault.economy.EconomyResponse;
import net.pl3x.pl3xcities.Pl3xCities;
import net.pl3x.pl3xcities.cities.City;
import net.pl3x.pl3xcities.commands.PlayerCommand;
import net.pl3x.pl3xcities.events.city.CityBuyEvent;
import net.pl3x.pl3xcities.util.RegionUtils;
import net.pl3x.pl3xcities.util.Utils;

import org.bukkit.command.Command;
import org.bukkit.entity.Player;

public class BuyCommand extends PlayerCommand {
	public BuyCommand(Pl3xCities plugin) {
		super(plugin, "buy", "Buy a city", "pl3xcities.command.city.buy", new String[] { "&dPurchase a city that's up for sale:", "   &e/&7city buy" });
	}

	@Override
	public List<String> onTabComplete(Player player, Command command, String label, LinkedList<String> args) {
		return new ArrayList<String>();
	}

	@Override
	public boolean onCommand(Player player, Command command, String label, LinkedList<String> args) throws Exception {
		// Check if server admin has disabled this feature.
		if (!plugin.getConfig().getBoolean("city-resale", false))
			throw new Exception("&4City selling has been disabled!");

		// Get city
		City city = RegionUtils.getCity(player.getLocation());
		if (city == null)
			throw new Exception("&4You are not standing in a city!");

		// Check if city is for sale
		boolean isForSale = city.isBuyable();
		if (!isForSale)
			throw new Exception("&4This city is not for sale!");

		// Check is buyer already owns the city
		if (player.getName().equalsIgnoreCase(city.getOwner()))
			throw new Exception("&4You already own this city!");

		// Check the players city limit
		int count = 0;
		int limit = Utils.getLimit(player, "pl3xcities.city.limit");
		for (City c : plugin.getCityManager().getAllCities()) {
			if (c.getOwner().equals(player.getName()))
				count++;
		}
		if (count >= limit && limit >= 0)
			throw new Exception("&4You have reached your limit of owned cities!");

		// Get the price of the city
		double price = city.getPrice();

		// Check for sufficient funds.
		double balance = plugin.getEconomy().getBalance(player.getName());
		if (price > balance)
			throw new Exception("&4You cannot afford to buy this city! (cost: " + plugin.getEconomy().format(price) + ")");

		// Call the event and check for cancellation
		CityBuyEvent event = new CityBuyEvent(city, player);
		plugin.getServer().getPluginManager().callEvent(event);
		if (event.isCancelled())
			return true;

		// Check funds
		EconomyResponse response = plugin.getEconomy().withdrawPlayer(player.getName(), price);
		if (!response.transactionSuccess())
			throw new Exception("&4An error has occurred trying to charge your account! " + response.errorMessage);

		// Update buyable flag and new owner
		city.setBuyable(false);
		city.setOwner(player.getName());

		// Finished.
		player.sendMessage(plugin.colorize("&dYou have purchased the city for " + plugin.getEconomy().format(price)));
		return true;
	}
}
