package net.pl3x.pl3xcities;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import net.milkbowl.vault.economy.Economy;
import net.pl3x.pl3xcities.cities.City;
import net.pl3x.pl3xcities.cities.CityManager;
import net.pl3x.pl3xcities.cities.Plot;
import net.pl3x.pl3xcities.cities.PlotManager;
import net.pl3x.pl3xcities.commands.CmdPl3xCities;
import net.pl3x.pl3xcities.commands.CmdPl3xCity;
import net.pl3x.pl3xcities.commands.CmdPl3xPlot;
import net.pl3x.pl3xcities.flag.BankFlag;
import net.pl3x.pl3xcities.flag.DisabledFlag;
import net.pl3x.pl3xcities.flag.LiquidProtectionFlag;
import net.pl3x.pl3xcities.flag.PaymentDueFlag;
import net.pl3x.pl3xcities.flag.PistonProtectionFlag;
import net.pl3x.pl3xcities.flag.RentPriceFlag;
import net.pl3x.pl3xcities.flag.RentableFlag;
import net.pl3x.pl3xcities.flag.RentalPeriodFlag;
import net.pl3x.pl3xcities.flag.RentedFlag;
import net.pl3x.pl3xcities.flag.RenterFlag;
import net.pl3x.pl3xcities.flag.TypeFlag;
import net.pl3x.pl3xcities.flag.TypeFlag.RegionType;
import net.pl3x.pl3xcities.flag.WorldFlag;
import net.pl3x.pl3xcities.listeners.LiquidListener;
import net.pl3x.pl3xcities.listeners.PistonListener;
import net.pl3x.pl3xcities.listeners.PlayerListener;
import net.pl3x.pl3xcities.task.TaskPaymentProcessor;
import net.pl3x.pl3xcities.util.Updater;
import net.pl3x.pl3xcities.util.Updater.UpdateResult;
import net.pl3x.pl3xcities.util.Updater.UpdateType;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitTask;
import org.mcstats.Metrics;

import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.GlobalRegionManager;
import com.sk89q.worldguard.protection.flags.DefaultFlag;
import com.sk89q.worldguard.protection.flags.Flag;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public class Pl3xCities extends JavaPlugin {
	private Economy economy;
	private CityManager cityManager;
	private BukkitTask paymentProcessor;
	private long lastUpdateCheck = 0;
	private boolean hasUpdate = false;
	private PlotManager plotManager;

	public void onEnable() {
		// copy config.yml to disk on first run
		saveDefaultConfig();

		// Check for dependency plugins
		List<String> failed = new ArrayList<String>();
		if (!Bukkit.getPluginManager().isPluginEnabled("WorldEdit"))
			failed.add("WorldEdit");
		if (!Bukkit.getPluginManager().isPluginEnabled("WorldGuard"))
			failed.add("WorldGuard");
		if (!Bukkit.getPluginManager().isPluginEnabled("Vault"))
			failed.add("Vault");
		if (!setupEconomy())
			failed.add("Any Economy Plugin");
		if (!failed.isEmpty()) {
			log("&4[ERROR] The following dependency are REQUIRED but missing!");
			log("&4[ERROR] " + failed.toString());
			log("&4[ERROR] These plugins are REQUIRED dependencies!");
			log("&4[ERROR] " + getName() + " is disabling itself!");
			Bukkit.getPluginManager().disablePlugin(this);
			return;
		}

		// Check for updates (unless server admin has disabled)
		if (getConfig().getBoolean("check-for-updates", true))
			checkForUpdate();

		// Create the city manager
		cityManager = new CityManager(this);
		plotManager = new PlotManager(this);

		// Register Listeners
		Bukkit.getPluginManager().registerEvents(new PlayerListener(this), this);
		Bukkit.getPluginManager().registerEvents(new LiquidListener(), this);
		Bukkit.getPluginManager().registerEvents(new PistonListener(), this);

		// Inject custom flags
		injectHax();

		// Register commands
		getCommand("pl3xcities").setExecutor(new CmdPl3xCities(this));
		getCommand("pl3xcity").setExecutor(new CmdPl3xCity(this));
		getCommand("pl3xplot").setExecutor(new CmdPl3xPlot(this));

		// Load city/plot data from regions
		loadCities();

		// Start the repeating tasks
		loadTasks();

		// Load up metrics
		try {
			Metrics metrics = new Metrics(this);
			metrics.start();
		} catch (IOException e) {
			log("&4Failed to start Metrics: &e" + e.getMessage());
		}

		// Finished.
		log(getName() + " v" + getDescription().getVersion() + " by BillyGalbreath enabled!");
	}

	public void onDisable() {
		if (paymentProcessor != null)
			paymentProcessor.cancel();
		log(getName() + " Disabled.");
	}

	/**
	 * Billy's bad-ass logger with COLOR (can be disabled in config.yml for plain logs)
	 * 
	 * @param string - The string to log
	 */
	public void log(String string) {
		if (getConfig().getBoolean("color-logs", true)) {
			getServer().getConsoleSender().sendMessage(colorize("&3[&d" + getName() + "&3]&r " + string));
		} else
			Bukkit.getLogger().log(Level.INFO, "[" + getName() + "] " + string.replaceAll("(?)\u00a7([a-f0-9k-or])", ""));
	}

	/**
	 * Debug logs only messages if debug-mode is enabled.
	 * 
	 * @param string - The string to log.
	 */
	public void debug(String string) {
		if (getConfig().getBoolean("debug-mode", false))
			log(string);
	}

	/**
	 * Old-school color code formatter. Replaces all color and style codes with the minecraft color code characters.
	 * 
	 * @param string - The string to colorize
	 * @return
	 */
	public String colorize(String string) {
		if (string == null || string == "")
			return "";
		return ChatColor.translateAlternateColorCodes('&', string);
	}

	/**
	 * Old-school color code formatter. Replaces all color and style codes with the minecraft color code characters.
	 * 
	 * @param message - The strings to colorize
	 * @return
	 */
	public String[] colorize(String[] message) {
		String[] lines = new String[message.length];
		for (int i = 0; i < lines.length; i++) {
			lines[i] = ChatColor.translateAlternateColorCodes('&', message[i]);
		}
		return lines;
	}

	/**
	 * Initialize the economy via Vault.
	 * 
	 * @return True if an economy plugin was found
	 */
	private boolean setupEconomy() {
		RegisteredServiceProvider<Economy> economyProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class);
		if (economyProvider != null)
			economy = economyProvider.getProvider();
		return (economy != null);
	}

	/**
	 * Get the economy
	 * 
	 * @return Economy
	 */
	public Economy getEconomy() {
		return economy;
	}

	/**
	 * Get the ever handy-dandy reference to the WorldGuardPlugin
	 * 
	 * @return The reference to WorldGuardPlugin
	 */
	public WorldGuardPlugin getWorldGuard() {
		Plugin plugin = getServer().getPluginManager().getPlugin("WorldGuard");
		if (plugin == null || !(plugin instanceof WorldGuardPlugin))
			return null;
		return (WorldGuardPlugin) plugin;
	}

	/**
	 * Get the ever handy-dandy reference to the WorldEditPlugin
	 * 
	 * @return The reference to WorldEditPlugin
	 */
	public WorldEditPlugin getWorldEdit() {
		Plugin plugin = getServer().getPluginManager().getPlugin("WorldEdit");
		if (plugin == null || !(plugin instanceof WorldEditPlugin))
			return null;
		return (WorldEditPlugin) plugin;
	}

	/**
	 * Get the city manager
	 * 
	 * @return CityManager
	 */
	public CityManager getCityManager() {
		return cityManager;
	}

	/**
	 * All the magical work to load city/plots from their yml files. Will not duplicate data on /reset
	 */
	public void loadCities() {
		List<City> allCities = new ArrayList<City>();
		List<Plot> allPlots = new ArrayList<Plot>();
		for (World world : Bukkit.getWorlds()) {
			RegionManager mgr = getWorldGuard().getGlobalRegionManager().get(world);
			Map<String, ProtectedRegion> regions = mgr.getRegions();
			for (ProtectedRegion region : regions.values()) {
				if (TypeFlag.getType(region).equals(RegionType.PLOT)) {
					String name = region.getId();
					int id = -1;
					try {
						id = Integer.valueOf(name.split("_plot")[1]);
					} catch (Exception e) {
						continue; // cant parse id number
					}
					Plot plot = new Plot(id, region);
					allPlots.add(plot);
					debug("PLOT FOUND: " + region.getId());
					continue; // plot added to list
				}
				if (TypeFlag.getType(region).equals(RegionType.CITY)) {
					allCities.add(new City(region.getId(), region));
					debug("CITY FOUND: " + region.getId());
					continue;
				}
				debug("REGION FOUND: " + region.getId() + " [Not a city or a plot. Skipping] " + TypeFlag.getType(region).toString());
			}
		}
		for (City city : allCities) {
			getCityManager().addCity(city);
			debug("Loaded City: " + city.getName());
			for (Plot plot : allPlots) {
				if (city.getRegion() == plot.getRegion().getParent()) {
					city.addPlot(plot);
					plotManager.addPlot(plot);
					debug("Loaded Plot: " + plot.getRegion().getId());
				}
			}
		}
	}

	public void loadTasks() {
		if (paymentProcessor != null)
			paymentProcessor.cancel();

		if (getConfig().getBoolean("plot-taxes") || getConfig().getBoolean("plot-rentals") || getConfig().getBoolean("city-taxes")) {
			long delay = getConfig().getLong("payment-processing-interval", 3);
			paymentProcessor = Bukkit.getScheduler().runTaskTimer(this, new TaskPaymentProcessor(this), delay, delay);
		}
	}

	/**
	 * Get the time since last update was checked
	 * 
	 * @return Unix timestamp (epoch)
	 */
	public long getLastUpdateCheck() {
		return lastUpdateCheck;
	}

	/**
	 * Check if the plugin already found an update. This does not actually perform the check.
	 * 
	 * @return True is update was found
	 */
	public boolean hasUpdate() {
		return hasUpdate;
	}

	/**
	 * Check if there is an update for the plugin. This queries the Bukkit servers for any updates.
	 * 
	 * @return True if update was found
	 */
	public boolean checkForUpdate() {
		lastUpdateCheck = System.currentTimeMillis();
		Updater updater = new Updater(this, 68169, this.getFile(), UpdateType.NO_DOWNLOAD, true);
		if (updater.getResult() == UpdateResult.UPDATE_AVAILABLE) {
			notifyOpsUpdateAvailable(updater.getLatestName());
			hasUpdate = true;
			lastUpdateCheck = -1;
		}
		return hasUpdate;
	}

	/**
	 * Notify console and ALL online Ops that an update was found.
	 * 
	 * @param version - The version string of the update (i.e., 'Pl3xCities v1.0')
	 */
	public void notifyOpsUpdateAvailable(String version) {
		log("&3***********************************************************************************");
		log("&3* [&6UPDATE&3] &dUpdated version available for Pl3xCities plugin!                       &3*");
		log("&3* [&6UPDATE&3] &dCurrent version: &4" + getName() + " v" + getDescription().getVersion() + "                                       &3*");
		log("&3* [&6UPDATE&3] &dUpdated version: &4" + version + "                                       &3*");
		log("&3* [&6UPDATE&3] &dPlease download from: http://dev.bukkit.org/bukkit-plugins/pl3xcities/ &3*");
		log("&3***********************************************************************************");
		for (Player player : Bukkit.getOnlinePlayers()) {
			if (!player.isOp())
				continue;
			player.sendMessage(colorize("&3[&7Pl3xCities&3] &dUpdated version available for Pl3xCities plugin! " + version));
			player.sendMessage(colorize("&3[&7Pl3xCities&3] &dPlease download from: http://dev.bukkit.org/bukkit-plugins/pl3xcities/"));
		}
	}

	public PlotManager getPlotManager() {
		return plotManager;
	}

	@SuppressWarnings("rawtypes")
	public void injectHax() {
		debug("Injecting custom flag data to WorldGuard...");
		try {
			Field field = DefaultFlag.class.getDeclaredField("flagsList");
			Field modifiersField = Field.class.getDeclaredField("modifiers");
			modifiersField.setAccessible(true);
			modifiersField.setInt(field, field.getModifiers() & 0xFFFFFFEF);
			field.setAccessible(true);
			List<Flag> elements = new ArrayList<Flag>(Arrays.asList(DefaultFlag.getFlags()));
			elements.add(BankFlag.flag);
			elements.add(DisabledFlag.flag);
			elements.add(LiquidProtectionFlag.flag);
			elements.add(PaymentDueFlag.flag);
			elements.add(PistonProtectionFlag.flag);
			elements.add(RentableFlag.flag);
			elements.add(RentalPeriodFlag.flag);
			elements.add(RentedFlag.flag);
			elements.add(RenterFlag.flag);
			elements.add(RentPriceFlag.flag);
			elements.add(TypeFlag.flag);
			elements.add(WorldFlag.flag);
			Flag[] list = new Flag[elements.size()];
			for (int i = 0; i < elements.size(); i++) {
				list[i] = elements.get(i);
			}
			field.set(null, list);
			Field grm = WorldGuardPlugin.class.getDeclaredField("globalRegionManager");
			grm.setAccessible(true);
			GlobalRegionManager globalRegionManager = (GlobalRegionManager) grm.get(Bukkit.getPluginManager().getPlugin("WorldGuard"));
			globalRegionManager.preload();
		} catch (Exception e) {
			e.printStackTrace();
		}
		debug("done.");
	}
}
