package net.pl3x.pl3xcities.flag;

import java.util.Set;

import net.pl3x.pl3xcities.util.Utils;

import com.sk89q.worldguard.protection.flags.SetFlag;
import com.sk89q.worldguard.protection.flags.StringFlag;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public class DisabledFlag extends SetFlag<String> {
	public static DisabledFlag flag = new DisabledFlag();

	public DisabledFlag() {
		super("disabled", new StringFlag(null));
		Utils.addIllegalFlag("disabled");
	}

	/**
	 * Check if a flag has been disabled by the city owner
	 * 
	 * @param region - The ProtectedRegion
	 * @return True if the flag is disabled from use
	 */
	public static boolean isFlagDisabled(ProtectedRegion region, String chkName) {
		Set<String> flagNames = region.getFlag(flag);
		if (flagNames == null || !flagNames.contains(chkName))
			return false;
		return true;
	}

	/**
	 * Get a Set of the disabled flag names for a region
	 * 
	 * @param region - The ProtectedRegion
	 * @return Set of flag names that are disabled
	 */
	public static Set<String> getDisabledFlags(ProtectedRegion region) {
		return region.getFlag(flag);
	}
}
