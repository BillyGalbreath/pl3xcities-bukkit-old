package net.pl3x.pl3xcities.flag;

import net.pl3x.pl3xcities.util.Utils;

import com.sk89q.worldguard.protection.flags.BooleanFlag;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public class RentableFlag extends BooleanFlag {
	public static RentableFlag flag = new RentableFlag();

	public RentableFlag() {
		super("rentable");
		Utils.addIllegalFlag("rentable");
	}

	/**
	 * Check if the plot can be rented out
	 * 
	 * @param set - The ProtectedRegion
	 * @return True if flag is true. False if flag is false or not set
	 */
	public static boolean isRentable(ProtectedRegion region) {
		Boolean bool = region.getFlag(flag);
		return bool == null ? false : bool;
	}
}
