package net.pl3x.pl3xcities.flag;

import net.pl3x.pl3xcities.util.Utils;

import org.bukkit.Bukkit;
import org.bukkit.World;

import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.flags.StringFlag;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public class WorldFlag extends StringFlag {
	public static WorldFlag flag = new WorldFlag();

	public WorldFlag() {
		super("world");
		Utils.addIllegalFlag("world");
	}

	/**
	 * Get the value of the flag
	 * 
	 * @param set - The ProtectedRegion
	 * @return The Bukkit World the region belongs to or null if no world found
	 */
	public static World getWorld(ProtectedRegion region) {
		return parseName(region.getFlag(flag));
	}

	/**
	 * Get the value of the flag
	 * 
	 * @param set - The ApplicableRegionSet
	 * @return The Bukkit World the region belongs to or null if no world found
	 */
	public static World getWorld(ApplicableRegionSet set) {
		return parseName(set.getFlag(flag));
	}

	/**
	 * Parse the name of a world into a Bukkit World object
	 * 
	 * @param name - The name of the world to parse
	 * @return A Bukkit World or null if no world found
	 */
	private static World parseName(String name) {
		if (name == null || name == "")
			return null;
		return Bukkit.getWorld(name);
	}
}
