package net.pl3x.pl3xcities.flag;

import net.pl3x.pl3xcities.util.Utils;

import com.sk89q.worldguard.protection.flags.StringFlag;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public class TypeFlag extends StringFlag {
	public static TypeFlag flag = new TypeFlag();

	public TypeFlag() {
		super("type");
		Utils.addIllegalFlag("type");
	}

	/**
	 * Get the value of the flag
	 * 
	 * @param set - The ProtectedRegion
	 * @return The RegionType of the region
	 */
	public static RegionType getType(ProtectedRegion region) {
		String type = region.getFlag(flag);
		if (type == null)
			return RegionType.UNKNOWN;
		if (type.equalsIgnoreCase("city"))
			return RegionType.CITY;
		if (type.equalsIgnoreCase("plot"))
			return RegionType.PLOT;
		return RegionType.UNKNOWN;
	}

	/**
	 * Enum representing the types of regions
	 */
	public static enum RegionType {
		CITY,
		PLOT,
		UNKNOWN;
	}
}
