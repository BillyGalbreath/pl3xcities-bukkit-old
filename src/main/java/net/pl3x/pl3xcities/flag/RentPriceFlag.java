package net.pl3x.pl3xcities.flag;

import net.pl3x.pl3xcities.util.Utils;

import com.sk89q.worldguard.protection.flags.DoubleFlag;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public class RentPriceFlag extends DoubleFlag {
	public static RentPriceFlag flag = new RentPriceFlag();

	public RentPriceFlag() {
		super("rent-price");
		Utils.addIllegalFlag("rent-price");
	}

	/**
	 * Get the rent price
	 * 
	 * @param region - The ProtectedRegion
	 * @return The price
	 */
	public static double getPrice(ProtectedRegion region) {
		return region.getFlag(flag);
	}
}
