package net.pl3x.pl3xcities.flag;

import net.pl3x.pl3xcities.util.Utils;

import com.sk89q.worldguard.protection.flags.StringFlag;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public class RenterFlag extends StringFlag {
	public static RenterFlag flag = new RenterFlag();

	public RenterFlag() {
		super("renter");
		Utils.addIllegalFlag("renter");
	}

	/**
	 * Get the renter of a plot
	 * 
	 * @param set - The ProtectedRegion
	 * @return The name of the renter
	 */
	public static String getRenter(ProtectedRegion region) {
		return region.getFlag(flag);
	}
}
