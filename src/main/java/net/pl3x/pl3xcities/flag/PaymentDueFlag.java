package net.pl3x.pl3xcities.flag;

import net.pl3x.pl3xcities.util.Utils;

import com.sk89q.worldguard.protection.flags.IntegerFlag;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public class PaymentDueFlag extends IntegerFlag {
	public static PaymentDueFlag flag = new PaymentDueFlag();

	public PaymentDueFlag() {
		super("payment-due");
		Utils.addIllegalFlag("payment-due");
	}

	/**
	 * Get the timestamp when next payment is due.
	 * 
	 * @param region - The ProtectedRegion
	 * @return The timestamp in seconds since epoch
	 */
	public static int getPaymentDue(ProtectedRegion region) {
		Integer paymentDue = region.getFlag(flag);
		return (paymentDue == null) ? 0 : paymentDue;
	}
}
