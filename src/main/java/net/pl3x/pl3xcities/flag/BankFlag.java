package net.pl3x.pl3xcities.flag;

import net.pl3x.pl3xcities.util.Utils;

import com.sk89q.worldguard.protection.flags.DoubleFlag;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public class BankFlag extends DoubleFlag {
	public static BankFlag flag = new BankFlag();

	public BankFlag() {
		super("bank");
		Utils.addIllegalFlag("bank");
	}

	/**
	 * Get the balance of the region's bank
	 * 
	 * @param set - The ProtectedRegion
	 * @return The balance
	 */
	public static double getBalance(ProtectedRegion region) {
		return region.getFlag(flag);
	}
}
