package net.pl3x.pl3xcities.flag;

import net.pl3x.pl3xcities.util.Utils;

import com.sk89q.worldguard.protection.flags.BooleanFlag;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public class RentedFlag extends BooleanFlag {
	public static RentedFlag flag = new RentedFlag();

	public RentedFlag() {
		super("rented");
		Utils.addIllegalFlag("rented");
	}

	/**
	 * Check if the plot currently rented out
	 * 
	 * @param set - The ProtectedRegion
	 * @return True if flag is true. False if flag is false or not set
	 */
	public static boolean isRented(ProtectedRegion region) {
		Boolean bool = region.getFlag(flag);
		return bool == null ? false : bool;
	}
}
