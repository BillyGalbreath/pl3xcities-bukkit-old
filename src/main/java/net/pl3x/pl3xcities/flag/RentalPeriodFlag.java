package net.pl3x.pl3xcities.flag;

import net.pl3x.pl3xcities.util.Utils;

import com.sk89q.worldguard.protection.flags.IntegerFlag;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public class RentalPeriodFlag extends IntegerFlag {
	public static RentalPeriodFlag flag = new RentalPeriodFlag();

	public RentalPeriodFlag() {
		super("rental-period");
		Utils.addIllegalFlag("rental-period");
	}

	/**
	 * Get the rental period in seconds.
	 * 
	 * @param region - The ProtectedRegion
	 * @return The timestamp in seconds since epoch
	 */
	public static int getRentalPeriod(ProtectedRegion region) {
		return region.getFlag(flag);
	}
}
