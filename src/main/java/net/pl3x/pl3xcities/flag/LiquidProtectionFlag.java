package net.pl3x.pl3xcities.flag;

import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.flags.BooleanFlag;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public class LiquidProtectionFlag extends BooleanFlag {
	public static LiquidProtectionFlag flag = new LiquidProtectionFlag();

	public LiquidProtectionFlag() {
		super("liquid-protection");
		// Do not add to illegal list!
	}

	/**
	 * Get the value of the flag
	 * 
	 * @param region - The ProtectedRegion being checked
	 * @return True if flag is true. False if flag is false or not set
	 */
	public static boolean getValue(ProtectedRegion region) {
		Boolean bool = region.getFlag(flag);
		return bool == null ? false : bool;
	}

	/**
	 * Get the value of the flag
	 * 
	 * @param set - The ApplicableregionSet
	 * @return True if flag is true. False if flag is false or not set
	 */
	public static boolean getValue(ApplicableRegionSet set) {
		Boolean bool = set.getFlag(flag);
		return bool == null ? false : bool;
	}
}
