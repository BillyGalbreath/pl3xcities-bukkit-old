package net.pl3x.pl3xcities.cities;

import net.pl3x.pl3xcities.flag.PaymentDueFlag;
import net.pl3x.pl3xcities.flag.RentPriceFlag;
import net.pl3x.pl3xcities.flag.RentableFlag;
import net.pl3x.pl3xcities.flag.RentalPeriodFlag;
import net.pl3x.pl3xcities.flag.RenterFlag;
import net.pl3x.pl3xcities.flag.WorldFlag;
import net.pl3x.pl3xcities.util.RegionUtils;

import org.bukkit.World;

import com.sk89q.worldguard.domains.DefaultDomain;
import com.sk89q.worldguard.protection.flags.BooleanFlag;
import com.sk89q.worldguard.protection.flags.DefaultFlag;
import com.sk89q.worldguard.protection.flags.Flag;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public class Plot {
	private City city;
	private int id;
	private ProtectedRegion region;

	public Plot(int id, ProtectedRegion region) {
		this.id = id;
		this.region = region;
	}

	/**
	 * Get the plot ID number
	 * 
	 * @return int - The plot ID number
	 */
	public int getId() {
		return id;
	}

	/**
	 * Get the City's name that this plot belongs to.
	 * 
	 * @return String - The parent City name
	 */
	public String getCityName() {
		return getRegion().getParent().getId();
	}

	/**
	 * Get the owner of the plot Mayor is owner if no one has purchase the plot.
	 * 
	 * @return String - The owner's name
	 */
	public String getOwner() {
		Object[] owners = getRegion().getOwners().getPlayers().toArray();
		if (owners.length == 0)
			return null;
		return owners[0].toString();
	}

	/**
	 * Set the owner of the plot. Mayor is owner if no one has purchased the plot.
	 * 
	 * @param owner - The owner's name
	 */
	public void setOwner(String owner) {
		DefaultDomain dd = new DefaultDomain();
		dd.addPlayer(owner);
		getRegion().setOwners(dd);
	}

	/**
	 * Get the region of this plot
	 * 
	 * @return The ProtectedRegion
	 */
	public ProtectedRegion getRegion() {
		return region;
	}

	/**
	 * Set the region of this plot
	 * 
	 * @param region - The ProtectedRegion
	 */
	public void setRegion(ProtectedRegion region) {
		this.region = region;
	}

	/**
	 * Get the world of this plot
	 * 
	 * @return The Bukkit World
	 */
	public World getWorld() {
		return WorldFlag.getWorld(getRegion());
	}

	/**
	 * Set the world this plot is in
	 * 
	 * @param world - The Bukkit World
	 * @throws Exception
	 */
	public void setWorld(World world) throws Exception {
		getRegion().setFlag((WorldFlag) WorldFlag.flag, world.getName());
		RegionUtils.saveRegion(getRegion());
	}

	/**
	 * Get the price of this plot
	 * 
	 * @return The price
	 */
	public double getPrice() {
		double price = -1;
		price = (Double) getRegion().getFlag(DefaultFlag.PRICE);
		return price;
	}

	/**
	 * Set the price of this plot
	 * 
	 * @param price - The price
	 * @throws Exception
	 */
	public void setPrice(double price) throws Exception {
		getRegion().setFlag(DefaultFlag.PRICE, price);
		RegionUtils.saveRegion(getRegion());
	}

	/**
	 * Check if this plot is buyable
	 * 
	 * @return True if plot is buyable
	 * @throws Exception
	 */
	public boolean isBuyable() throws Exception {
		Flag<?> flag = DefaultFlag.BUYABLE;
		boolean isForSale = false;
		try {
			isForSale = (Boolean) getRegion().getFlag(flag);
		} catch (Exception ignore) {
			isForSale = false;
		}
		return isForSale;
	}

	/**
	 * Set whether this plot is buyable or not
	 * 
	 * @param state - The buyable state
	 * @throws Exception
	 */
	public void setBuyable(boolean state) throws Exception {
		getRegion().setFlag((BooleanFlag) DefaultFlag.BUYABLE, state);
		RegionUtils.saveRegion(getRegion());
	}

	/**
	 * Check if this plot is rentable
	 * 
	 * @return True if plot is rentable
	 */
	public boolean isRentable() {
		return RentableFlag.isRentable(getRegion());
	}

	/**
	 * Set whether this plot is rentable or not
	 * 
	 * @param state - The rentable state
	 * @throws Exception
	 */
	public void setRentable(boolean state) throws Exception {
		getRegion().setFlag((RentableFlag) RentableFlag.flag, state);
		RegionUtils.saveRegion(getRegion());
	}

	/**
	 * Get the renter's name
	 * 
	 * @return The renter's name or null
	 */
	public String getRenter() {
		return RenterFlag.getRenter(getRegion());
	}

	/**
	 * Set the renter of this plot
	 * 
	 * @param renter - The renter's name
	 * @throws Exception
	 */
	public void setRenter(String renter) throws Exception {
		getRegion().setFlag((RenterFlag) RenterFlag.flag, renter);
		RegionUtils.saveRegion(getRegion());
	}

	/**
	 * Get the rent price of the plot
	 * 
	 * @return The rent price
	 */
	public double getRentPrice() {
		return RentPriceFlag.getPrice(getRegion());
	}

	/**
	 * Get the rent/tax due timestamp in seconds since epoch
	 * 
	 * @return the timestamp (in seconds since epoch) when next rent is due
	 */
	public int getPaymentDue() {
		return PaymentDueFlag.getPaymentDue(getRegion());
	}

	/**
	 * Set the rent price of the plot
	 * 
	 * @param rentPrice - The rent price
	 * @throws Exception
	 */
	public void setRentPrice(double rentPrice) throws Exception {
		getRegion().setFlag((RentPriceFlag) RentPriceFlag.flag, rentPrice);
		RegionUtils.saveRegion(getRegion());
	}

	/**
	 * Set the timestamp (in seconds since epoch) when next rent/tax payment is due
	 * 
	 * @param due
	 * @throws Exception
	 */
	public void setPaymentDue(int due) throws Exception {
		getRegion().setFlag((PaymentDueFlag) DefaultFlag.fuzzyMatchFlag("payment-due"), due);
		RegionUtils.saveRegion(getRegion());
	}

	/**
	 * get the rental period for this plot.
	 * 
	 * @return the time in seconds this plot's rent is charged
	 */
	public int getRentalPeriod() {
		return RentalPeriodFlag.getRentalPeriod(getRegion());
	}

	/**
	 * set the rental period for this plot.
	 * 
	 * @param period the time in seconds this plot's rent is charged
	 */
	public void setRentalPeriod(int period) throws Exception {
		getRegion().setFlag((RentalPeriodFlag) DefaultFlag.fuzzyMatchFlag("rental-period"), period);
		RegionUtils.saveRegion(getRegion());
	}

	/**
	 * Check if the plot is currently rented.
	 * 
	 * @return true if a player is currently renting this plot, false otherwise.
	 */
	public boolean isRented() {
		return !(this.getRenter() == null || this.getRenter().equals(""));
	}

	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}
}
