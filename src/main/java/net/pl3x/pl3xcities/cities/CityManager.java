package net.pl3x.pl3xcities.cities;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.PriorityQueue;

import net.milkbowl.vault.economy.EconomyResponse;
import net.pl3x.pl3xcities.Pl3xCities;
import net.pl3x.pl3xcities.util.RegionUtils;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import com.sk89q.worldedit.bukkit.selections.Selection;
import com.sk89q.worldguard.domains.DefaultDomain;

public class CityManager {
	private Pl3xCities plugin;
	private HashMap<String, City> cities = new HashMap<String, City>();
	private City cityProcessing = null;
	private Iterator<Plot> cityPlotIter = null;
	private double cityTaxesCollected = 0;
	private PriorityQueue<City> paymentQueue = new PriorityQueue<City>(10, new Comparator<City>() {
		@Override
		public int compare(City city1, City city2) {
			return city1.getPaymentDue() - city2.getPaymentDue();
		}
	});

	public CityManager(Pl3xCities plugin) {
		this.plugin = plugin;
	}

	/**
	 * Get City object by name
	 * 
	 * @param name Name of City to get
	 * @return City
	 */
	public City getCity(String name) {
		return cities.get(name.toLowerCase());
	}

	/**
	 * Get a list of all known cities in all worlds
	 * 
	 * @return List of all City objects
	 */
	public Collection<City> getAllCities() {
		return cities.values();
	}

	/**
	 * Add city to the list of known cities
	 * 
	 * @param city - The City
	 */
	public void addCity(City city) {
		if (cities.containsValue(city))
			return;
		cities.put(city.getName().toLowerCase(), city);
		paymentQueue.add(city);
	}

	/**
	 * Remove city form the list of known cities
	 * 
	 * @param city - The City
	 */
	public void removeCity(City city) {
		if (!cities.containsValue(city))
			return;
		cities.remove(city.getName().toLowerCase());
		paymentQueue.remove(city);
		for (Plot plot : city.getPlots()) {
			plugin.getPlotManager().removePlot(plot);
		}
	}

	/**
	 * Check if a known city exists with specified name
	 * 
	 * @param name - The name to check
	 * @return True if a city has this name
	 */
	public boolean isCityCreated(String name) {
		return cities.get(name.toLowerCase()) != null;
	}

	/**
	 * Process city taxes.
	 */
	public void processTaxes() {
		int now = (int) (System.currentTimeMillis() / 1000);
		if (cityProcessing == null || !isCityCreated(cityProcessing.getName())) {
			cityProcessing = paymentQueue.peek();
			if (cityProcessing == null)
				return;
			if (cityProcessing.getPaymentDue() > now)
				cityProcessing = null;
			else
				cityPlotIter = new ArrayList<Plot>(cityProcessing.getPlots()).iterator();
		}
		if (cityProcessing != null) {
			boolean plotTaxes = plugin.getConfig().getBoolean("plot-taxes", true);
			int taxPeriod = plugin.getConfig().getInt("tax-collection-interval", 8400);
			if (plotTaxes && cityPlotIter.hasNext()) {
				// process a plot
				Plot plot = cityPlotIter.next();
				if (plot.getOwner() != null && !plot.getOwner().equals(plot.getCity().getOwner())) {
					// Calculate plot tax
					Selection selection = RegionUtils.getSelection(plot.getRegion(), plot.getWorld());
					double plotArea = selection.getArea() / selection.getHeight();
					double tax = (plugin.getConfig().getDouble("cost-per-plot-block-buy", 1.0) * (plugin.getConfig().getDouble("plot-tax-percent", 1.0) / 100)) * plotArea;
					double taxTotal = 0;
					int paidTo = cityProcessing.getPaymentDue();
					while (now < paidTo) {
						taxTotal += tax;
						paidTo += taxPeriod;
					}
					EconomyResponse wdResp = plugin.getEconomy().withdrawPlayer(plot.getOwner(), taxTotal);
					OfflinePlayer player = plugin.getServer().getOfflinePlayer(plot.getOwner());
					if (wdResp.transactionSuccess()) {
						if (plugin.getConfig().getBoolean("notify-tax-collection", true)) {
							if (player.isOnline())
								player.getPlayer().sendMessage(plugin.colorize("&dYou have paid your taxes (" + plugin.getEconomy().format(taxTotal) + ") for your plot in " + plot.getCityName()));
						}
						cityTaxesCollected += taxTotal;
						try {
							cityProcessing.addToBank(taxTotal);
						} catch (Exception e) {
							e.printStackTrace();
						}
					} else {
						if (player.isOnline())
							player.getPlayer().sendMessage("&4You have been evicted from your plot in " + plot.getCity().getName() + "for not paying your taxes!");
						plot.setOwner(plot.getCity().getOwner());
						plot.getRegion().setMembers(new DefaultDomain());
						try {
							plot.setPaymentDue(Integer.MAX_VALUE);
						} catch (Exception ex) {
							ex.printStackTrace();
						}
					}
				}
			} else {
				// Process the city itself.
				Player owner = Bukkit.getPlayerExact(cityProcessing.getOwner());
				if (owner != null && owner.isOnline() && cityTaxesCollected > 0)
					owner.sendMessage(plugin.colorize("&d" + cityProcessing.getName() + " has collected " + plugin.getEconomy().format(cityTaxesCollected) + " in plot taxes."));

				if (plugin.getConfig().getBoolean("city-taxes", true)) {
					double taxTotal = 0;
					Selection selection = RegionUtils.getSelection(cityProcessing.getRegion(), cityProcessing.getWorld());
					double cityArea = selection.getArea() / selection.getHeight();
					double tax = (plugin.getConfig().getDouble("cost-per-city-block", 10.0) * (plugin.getConfig().getDouble("city-tax-percent", 1.0) / 100)) * cityArea;
					int paidTo = cityProcessing.getPaymentDue();
					while (now >= paidTo) {
						paidTo += taxPeriod;
						taxTotal += tax;
					}

					try {
						cityProcessing.addToBank(-taxTotal);
					} catch (Exception ex) {
						ex.printStackTrace();
					}
					if (owner != null && owner.isOnline())
						owner.sendMessage(plugin.colorize("&dYou have paid " + plugin.getEconomy().format(taxTotal) + " in city taxes (for " + cityProcessing.getName() + ")"));

					String account = plugin.getConfig().getString("server-tax-account", null);
					if (account != null && !account.equals("")) {
						if (!plugin.getEconomy().depositPlayer(account, taxTotal).transactionSuccess()) {
							plugin.log(plugin.colorize("&4There was an error depositing " + plugin.getEconomy().format(taxTotal) + " in city taxes for " + cityProcessing.getName() + " into server account: " + account));
						}
					}

					if (cityProcessing.getBankBalance() < plugin.getConfig().getDouble("city-minimum-balance", -10000)) {
						plugin.getServer().broadcastMessage("&f" + cityProcessing.getName() + " &d has fallen!");
						removeCity(cityProcessing);
					}
				}
				try {
					cityProcessing.setPaymentDue(now + taxPeriod);
				} catch (Exception e) {
					e.printStackTrace();
				}
				paymentQueue.remove(cityProcessing);
				if (isCityCreated(cityProcessing.getName()))
					paymentQueue.add(cityProcessing);
				// prepare for next city.
				cityProcessing = null;
				cityPlotIter = null;
				cityTaxesCollected = 0;
			}
		}
	}
}
