package net.pl3x.pl3xcities.cities;

import java.util.Comparator;
import java.util.PriorityQueue;

import net.milkbowl.vault.economy.EconomyResponse;
import net.pl3x.pl3xcities.Pl3xCities;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;

import com.sk89q.worldguard.domains.DefaultDomain;

public class PlotManager {
	Pl3xCities plugin;

	private PriorityQueue<Plot> paymentQueue = new PriorityQueue<Plot>(10, new Comparator<Plot>() {
		@Override
		public int compare(Plot o1, Plot o2) {
			return o1.getPaymentDue() - o2.getPaymentDue();
		}
	});

	public PlotManager(Pl3xCities plugin) {
		this.plugin = plugin;
	}

	public void addPlot(Plot plot) {
		paymentQueue.remove(plot);
		paymentQueue.add(plot);
	}

	public void removePlot(Plot plot) {
		paymentQueue.remove(plot);
	}

	public void refreshPlot(Plot plot) {
		addPlot(plot);
	}

	public void processRent() {
		int now = (int) (System.currentTimeMillis() / 1000);
		Plot plot = paymentQueue.peek();
		if (!plot.isRented()) {
			try {
				plot.setPaymentDue(Integer.MAX_VALUE);
			} catch (Exception e) {
				e.printStackTrace();
			}
			paymentQueue.remove(plot);
			return;
		}
		double rentPrice = plot.getRentPrice();
		double totalRent = 0;
		int period = plot.getRentalPeriod();
		OfflinePlayer renter = Bukkit.getOfflinePlayer(plot.getRenter());
		OfflinePlayer owner = Bukkit.getOfflinePlayer(plot.getOwner());
		while (plot.getPaymentDue() < now) {
			totalRent += rentPrice;
			try {
				plot.setPaymentDue(plot.getPaymentDue() + period);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		EconomyResponse response = plugin.getEconomy().withdrawPlayer(plot.getRenter(), totalRent);

		if (response.transactionSuccess()) {
			if (plugin.getConfig().getBoolean("notify-paid-rent", true)) {
				if (renter.isOnline())
					renter.getPlayer().sendMessage(plugin.colorize("&dYou have paid your rent (" + plugin.getEconomy().format(totalRent) + ") for your plot in " + plot.getCity().getName()));
			}
			EconomyResponse depositResponse = plugin.getEconomy().depositPlayer(owner.getName(), totalRent);
			if (depositResponse.transactionSuccess()) {
				if (plugin.getConfig().getBoolean("notify-paid-rent", true) && owner.isOnline())
					owner.getPlayer().sendMessage(plugin.colorize("&dYou have collected " + plugin.getEconomy().format(totalRent) + " in rent from " + renter.getName() + " for your plot in " + plot.getCity().getName()));
			} else {
				if (owner.isOnline())
					owner.getPlayer().sendMessage(plugin.colorize("&4There was an error depositing the rent (" + plugin.getEconomy().format(totalRent) + ") from " + renter.getName() + " for your plot in " + plot.getCity().getName()));
				plugin.log(plugin.colorize("&4There was an error depositing the rent (" + plugin.getEconomy().format(totalRent) + ") into " + owner.getName() + "'s account for plot " + plot.getId() + " in " + plot.getCity().getName()));
			}
		} else {
			if (renter.isOnline())
				renter.getPlayer().sendMessage(plugin.colorize("&4You have been evicted from your plot in " + plot.getCity().getName() + "for not paying your rent!"));
			if (owner.isOnline())
				owner.getPlayer().sendMessage(plugin.colorize("&d" + renter.getName() + " was evicted from their plot in " + plot.getCity().getName() + " for not paying rent!"));
			try {
				plot.setRenter(null);
				plot.getRegion().setMembers(new DefaultDomain());
				plot.setPaymentDue(Integer.MAX_VALUE);
				paymentQueue.remove(plot);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		paymentQueue.remove(plot);
		paymentQueue.add(plot);
	}
}
