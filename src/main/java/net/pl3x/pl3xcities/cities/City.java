package net.pl3x.pl3xcities.cities;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import net.pl3x.pl3xcities.flag.BankFlag;
import net.pl3x.pl3xcities.flag.PaymentDueFlag;
import net.pl3x.pl3xcities.flag.WorldFlag;
import net.pl3x.pl3xcities.util.RegionUtils;
import net.pl3x.pl3xcities.util.Utils;

import org.bukkit.Location;
import org.bukkit.World;

import com.sk89q.worldguard.domains.DefaultDomain;
import com.sk89q.worldguard.protection.flags.BooleanFlag;
import com.sk89q.worldguard.protection.flags.DefaultFlag;
import com.sk89q.worldguard.protection.flags.DoubleFlag;
import com.sk89q.worldguard.protection.flags.Flag;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public class City {
	private String name;
	private ProtectedRegion region;
	private List<Plot> plots = new ArrayList<Plot>();

	public City(String name, ProtectedRegion region) {
		this.name = name;
		this.region = region;
	}

	/**
	 * Get the name of this City
	 * 
	 * @return City name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Change the city name
	 * 
	 * @param newName - The city's name
	 */
	public void setName(String newName) {
		name = newName;
	}

	/**
	 * Get the city region
	 * 
	 * @return ProtectedRegion
	 */
	public ProtectedRegion getRegion() {
		return region;
	}

	/**
	 * Set the city region
	 * 
	 * @param region - The ProtectedRegion
	 */
	public void setRegion(ProtectedRegion region) {
		this.region = region;
	}

	/**
	 * Get a plot in this city.
	 * 
	 * @param id - The id number of the plot to get
	 * @return The Plot
	 */
	public Plot getPlot(int id) {
		for (Plot plot : plots) {
			if (plot.getId() == id)
				return plot;
		}
		return null;
	}

	/**
	 * Get a list all the plots in this city.
	 * 
	 * @return List of this city's plots
	 */
	public List<Plot> getPlots() {
		return plots;
	}

	/**
	 * Add a plot to this city.
	 * 
	 * @param plot - The Plot to add
	 */
	public void addPlot(Plot plot) {
		plots.add(plot);
	}

	/**
	 * Remove a plot from this city.
	 * 
	 * @param plot
	 */
	public void removePlot(Plot plot) {
		if (plots.contains(plot))
			plots.remove(plot);
	}

	/**
	 * Get the next available id for plots. (Will reuse old ids no longer in use)
	 * 
	 * @return Next available id number
	 */
	public int getNextPlotId() {
		for (int i = 0; i <= getPlots().size(); i++) {
			if (getPlot(i) == null)
				return i;
		}
		return getPlots().size();
	}

	/**
	 * Get the name of the owner (mayor)
	 * 
	 * @return The owner's name
	 */
	public String getOwner() {
		Object[] owners = getRegion().getOwners().getPlayers().toArray();
		if (owners.length == 0)
			return null;
		return owners[0].toString();
	}

	/**
	 * Set the owner (mayor) of this City.
	 * 
	 * @param owner - The new owner's name
	 */
	public void setOwner(String owner) {
		DefaultDomain dd = new DefaultDomain();
		dd.addPlayer(owner);
		getRegion().setOwners(dd);
	}

	/**
	 * Get the world this city is in
	 * 
	 * @return Bukkit World
	 */
	public World getWorld() {
		return WorldFlag.getWorld(getRegion());
	}

	/**
	 * Set the world this city is in
	 * 
	 * @param world - The Bukkit World
	 * @throws Exception
	 */
	public void setWorld(World world) throws Exception {
		getRegion().setFlag(WorldFlag.flag, world.getName());
		RegionUtils.saveRegion(getRegion());
	}

	/**
	 * Get the location of the city's spawn
	 * 
	 * @return Bukkit Location
	 */
	public Location getSpawn() {
		return Utils.getSpawn(this);
	}

	/**
	 * Set the city's spawn location
	 * 
	 * @param spawn - The Bukkit Location
	 * @throws Exception
	 */
	public void setSpawn(Location spawn) throws Exception {
		getRegion().setFlag(DefaultFlag.SPAWN_LOC, RegionUtils.getLocation(spawn));
		RegionUtils.saveRegion(getRegion());
	}

	/**
	 * Get the city's price
	 * 
	 * @return The price
	 * @throws Exception
	 */
	public double getPrice() throws Exception {
		double price = -1;
		try {
			price = (Double) getRegion().getFlag(DefaultFlag.PRICE);
		} catch (Exception e) {
			throw new Exception("&4Unable to determine city price!");
		}
		return price;
	}

	/**
	 * Set the price of the city
	 * 
	 * @param price - The price
	 * @throws Exception
	 */
	public void setPrice(double price) throws Exception {
		getRegion().setFlag((DoubleFlag) DefaultFlag.PRICE, price);
		RegionUtils.saveRegion(getRegion());
	}

	/**
	 * Check if this city is buyable
	 * 
	 * @return True if city is buyable
	 * @throws Exception
	 */
	public boolean isBuyable() throws Exception {
		Flag<?> flag = DefaultFlag.BUYABLE;
		boolean isForSale = false;
		try {
			isForSale = (Boolean) getRegion().getFlag(flag);
		} catch (Exception ignore) {
			isForSale = false;
		}
		return isForSale;
	}

	/**
	 * Set whether this city is buyable or not
	 * 
	 * @param state - The buyable state
	 * @throws Exception
	 */
	public void setBuyable(boolean state) throws Exception {
		getRegion().setFlag((BooleanFlag) DefaultFlag.BUYABLE, state);
		RegionUtils.saveRegion(getRegion());
	}

	/**
	 * Get the city's bank balance
	 * 
	 * @return The balance
	 */
	public double getBankBalance() {
		Double balance = (Double) getRegion().getFlag(BankFlag.flag);
		if (balance == null) {
			balance = 0.0;
			getRegion().setFlag(BankFlag.flag, balance);
		}
		return balance;
	}

	/**
	 * Add to the city's bank account. Negative amount will subtract.
	 * 
	 * @param amount - The amount
	 * @throws Exception
	 */
	public void addToBank(double amount) throws Exception {
		getRegion().setFlag(BankFlag.flag, getBankBalance() + amount);
		RegionUtils.saveRegion(getRegion());
	}

	/**
	 * Set the timestamp (in seconds since epoch) when next tax payment is due
	 * 
	 * @param due
	 * @throws Exception
	 */
	public void setPaymentDue(int due) throws Exception {
		getRegion().setFlag((PaymentDueFlag) DefaultFlag.fuzzyMatchFlag("payment-due"), due);
		RegionUtils.saveRegion(getRegion());
	}

	/**
	 * Get the rent/tax due timestamp in seconds since epoch
	 * 
	 * @return the timestamp (in seconds since epoch) when next tax payment is due
	 */
	public int getPaymentDue() {
		return PaymentDueFlag.getPaymentDue(getRegion());
	}

	public Set<String> getOfficers() {
		return getRegion().getMembers().getPlayers();
	}
}
